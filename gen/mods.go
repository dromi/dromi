// Copyright (c) 2015, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package gen

import (
	"github.com/goulash/stat/dist"
	"gitlab.com/dromi/dromi/mdl"
	"gitlab.com/dromi/dromi/script"
	"gitlab.com/dromi/dromi/simt"
)

func init() {
	script.CreateFunc("pod|mark", NewMarker)
	script.CreateFunc("pod|mux-random", NewRandomMultiplexer)
	script.CreateFunc("pod|mux-robin", NewRoundRobinMultiplexer)
	script.CreateFunc("pod|mux-slice", func(xs []int64, ps ...Podder) Podder {
		ys := make([]int, len(xs))
		for i, x := range xs {
			ys[i] = int(x)
		}
		return NewAlternatingMultiplexer(ys, ps...)
	})
}

// Marker {{{1

func NewMarker(mark uint8, p Podder) Podder {
	return &marker{p, mark}
}

type marker struct {
	p Podder
	m uint8
}

func (p *marker) Pod(t simt.Time) mdl.Pod {
	pod := p.p.Pod(t)
	pod.Marks |= p.m
	return pod
}

// Multiplexer {{{1

type MultiplexerFunc func() int

func NewMultiplexer(mf MultiplexerFunc, ps ...Podder) Podder {
	return &multiplexer{mf, ps}
}

func NewRandomMultiplexer(d dist.Discrete, ps ...Podder) Podder {
	return &multiplexer{
		mf:   func() int { return int(d.Int63()) },
		pods: ps,
	}
}

func NewRoundRobinMultiplexer(ps ...Podder) Podder {
	idx, n := -1, len(ps)
	return &multiplexer{
		mf: func() int {
			idx = (idx + 1) % n
			return idx
		},
		pods: ps,
	}
}

func NewAlternatingMultiplexer(xs []int, ps ...Podder) Podder {
	idx, n := -1, len(ps)
	return &multiplexer{
		mf: func() int {
			idx = (idx + 1) % n
			return xs[idx]
		},
		pods: ps,
	}
}

type multiplexer struct {
	mf   MultiplexerFunc
	pods []Podder
}

func (p *multiplexer) Pod(t simt.Time) mdl.Pod {
	return p.pods[p.mf()].Pod(t)
}

// Copyright (c) 2015, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package gen

import (
	"github.com/goulash/stat/dist"
	"gitlab.com/dromi/dromi/mdl"
	"gitlab.com/dromi/dromi/script"
	"gitlab.com/dromi/dromi/simt"
)

func init() {
	script.CreateFunc("pod.distributed", NewDistributed)
}

func NewDistributed(runtime, deadline simt.Distribution, priority dist.Discrete, minc, maxc, mem, net dist.Continuous) *Distributed {
	null := &dist.Null{}
	if runtime == nil {
		runtime = func() simt.Duration { return 0 }
	}
	if deadline == nil {
		deadline = func() simt.Duration { return 0 }
	}
	if priority == nil {
		priority = null
	}
	if minc == nil {
		minc = null
	}
	if maxc == nil {
		panic("max cores cannot be nil")
	}
	if mem == nil {
		panic("mem cannot be nil")
	}
	if net == nil {
		net = null
	}
	return &Distributed{runtime, deadline, priority, minc, maxc, mem, net}
}

// Distributed is a very simple event generator, where every
// aspect is simply given by a distribution.
type Distributed struct {
	// Runtime is how much time on a 1GHz 1 core CPU this process
	// needs. We assume for now that speedup is proportional to cores.
	Runtime simt.Distribution

	// Deadline is the time after which it doesn't matter if the
	// process becomes obsolete. The birth time needs to be added.
	Deadline simt.Distribution

	// Priority must be distributed between 0 and 11.
	Priority dist.Discrete

	// Describe the required resources.
	MinCores dist.Continuous
	MaxCores dist.Continuous
	Memory   dist.Continuous
	Network  dist.Continuous
}

func (p *Distributed) Pod(t simt.Time) mdl.Pod {
	pod := mdl.Pod{
		Deadline: t + simt.Time(p.Deadline()),
		Runtime:  p.Runtime(),
		Priority: int8(p.Priority.Int63()),
		MinCores: float32(p.MinCores.Float64()),
		MaxCores: float32(p.MaxCores.Float64()),
		Memory:   uint32(p.Memory.Float64()),
		Network:  uint32(p.Network.Float64()),
	}
	if pod.MaxCores < pod.MinCores {
		pod.MaxCores = pod.MinCores
	}
	return pod
}

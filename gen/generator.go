// Copyright (c) 2015, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

// Package gen provides job generators for dromi.
package gen

import (
	"gitlab.com/dromi/dromi"
	"gitlab.com/dromi/dromi/event"
	"gitlab.com/dromi/dromi/mdl"
	"gitlab.com/dromi/dromi/script"
	"gitlab.com/dromi/dromi/simt"
)

func init() {
	script.CreateFunc("gen.podder", New)
}

// Podder describes any type that can create Pods.
// The time passed the function is the time of birth.
type Podder interface {
	Pod(simt.Time) mdl.Pod
}

// Generator takes a Process and a Podder and creates a Generator,
// that satisfies the interface requirements of Master.
type Generator struct {
	Process simt.Process // Determines the time of the next event
	Podder  Podder       // Creates pods for the next event
	Time    simt.Time    // Starting time
	ID      int64        // Starting job ID (not included)
}

// New creates a new Generator.
func New(o Podder, p simt.Process) *Generator {
	return &Generator{
		Process: p,
		Podder:  o,
	}
}

// Next returns a new birth event.
func (g *Generator) Next() *event.Event {
	g.ID++
	g.Time = g.Process.Next(g.Time)
	return &event.Event{
		Time: g.Time,
		Type: event.Arrival,
		Job: &mdl.Job{
			ID:       g.ID,
			Arrival:  g.Time,
			Pod:      g.Podder.Pod(g.Time),
			Sched:    make([]simt.Time, 0, 2),
			SchedDur: make([]simt.Duration, 0, 2),
		},
	}
}

func (g *Generator) Finalize() error {
	g.Process = nil
	g.Podder = nil
	return nil
}

var _ = dromi.Generator(&Generator{})

// Copyright (c) 2015, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package script

import (
	"errors"
	"fmt"
	"math/rand"
	"reflect"
	"strings"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/goulash/pre"
	"github.com/goulash/stat/dist"
	"github.com/goulash/twikutil"
	"gopkg.in/twik.v1"
	"gopkg.in/twik.v1/ast"
)

func New() *twikutil.Executer {
	e := twikutil.New(LibraryLoader)
	e.PreProcessor = pre.New()
	e.Scope().Create("let", letFn)
	return e
}

func Exec(file string) (*twik.Scope, error) {
	e := New()
	return e.Exec(file)
}

func LibraryUsage() []string {
	fset := twik.NewFileSet()
	s := twik.NewScope(fset)

	fm := libLoader(s, func(dst, src twikutil.FuncMap) { dst.Import(src) })
	return fm.FormatList()
}

func LibraryLoader(s *twik.Scope) twikutil.FuncMap {
	return libLoader(s, func(dst, src twikutil.FuncMap) {
		for k, v := range src {
			fs := strings.SplitN(k, ":", 2)
			if len(fs) != 2 {
				panic("key value should have format <pkg>:<func>")
			}
			dst[fs[1]] = v
		}
	})
}

func libLoader(s *twik.Scope, fn func(dst, src twikutil.FuncMap)) twikutil.FuncMap {
	dst := make(twikutil.FuncMap)
	fn(dst, stdlang(s))
	fn(dst, stdlib)
	fn(dst, dromilib)
	return dst
}

func stdlang(s *twik.Scope) twikutil.FuncMap {
	return twikutil.FuncMap{
		"lang:id":      func(v interface{}) interface{} { return v },
		"lang:let$":    letStarFn(s),
		"lang:set$":    setStarFn(s),
		"lang:get":     get,
		"lang:list":    list,
		"lang:list#":   listLen,
		"lang:list<":   listAppend,
		"lang:list+":   listAdd,
		"lang:list&":   listRep,
		"lang:slice":   slice,
		"lang:switch":  switchFn,
		"lang:case":    caseFn,
		"lang:default": defaultFn,
		"lang:range":   rangeFn,
	}
}

var stdlib = twikutil.FuncMap{
	"std:debugf":        log.Debugf,
	"std:infof":         log.Infof,
	"std:warningf":      log.Warningf,
	"std:printf":        fmt.Printf,
	"std:sprintf":       fmt.Sprintf,
	"std:assert":        assert,
	"std:rand.new":      rand.New,
	"std:rand.int64":    func(r *rand.Rand) int64 { return r.Int63() },
	"std:rand.float64":  func(r *rand.Rand) float64 { return r.Float64() },
	"std:rand.src":      rand.NewSource,
	"std:rand.factory":  randFactory,
	"std:str.split":     strings.Split,
	"std:str.join":      strings.Join,
	"std:str.prefix?":   strings.HasPrefix,
	"std:str.suffix?":   strings.HasSuffix,
	"std:str.contains?": strings.Contains,
	"std:time.now":      time.Now,
	"std:time.unix":     func(t time.Time) int64 { return t.Unix() },

	"std:dist|hp":     dist.Highpass,
	"std:dist|lp":     dist.Lowpass,
	"std:dist|mp":     dist.Midpass,
	"std:dist|round":  dist.Round,
	"std:dist|floor":  dist.Floor,
	"std:dist|ceil":   dist.Ceil,
	"std:dist.exp":    dist.NewExponential,
	"std:dist.hexp":   dist.NewHyperExponential,
	"std:dist.norm":   dist.NewNormal,
	"std:dist.lnorm":  dist.NewLogNormal,
	"std:dist.pois":   dist.NewPoisson,
	"std:dist.stairs": dist.NewStairs,
	"std:dist.unif":   dist.NewUniform,
	"std:dist.unifd":  dist.NewUniformDiscrete,
}

// dromilib {{{1

var dromilib = twikutil.FuncMap{}

func dromiName(s string) string {
	return "dromi:" + s
}

func CreateFunc(name string, fn interface{}) {
	_, ok := dromilib[dromiName(name)]
	if ok {
		panic("function already exists in dromilib")
	}
	OverwriteFunc(name, fn)
}

func OverwriteFunc(name string, fn interface{}) {
	t := reflect.TypeOf(fn)
	if t.Kind() != reflect.Func {
		panic("fn is not a function")
	}
	dromilib[dromiName(name)] = fn
}

// stdlang definitions {{{1

// randFactory uses s to create new seeded sources.
// This is a good way to get random, yet repeatable results.
func randFactory(s rand.Source) func([]interface{}) (interface{}, error) {
	rnd := rand.New(s)
	return twikutil.Func("rand.gen", func() rand.Source {
		return rand.NewSource(rnd.Int63())
	})
}

func assert(b bool) {
	if !b {
		panic("assertion failed")
	}
}

func letFn(scope *twik.Scope, args []ast.Node) (value interface{}, err error) {
	if len(args) == 0 || len(args) > 2 {
		return nil, errors.New("let takes one or two arguments")
	}
	symbol, ok := args[0].(*ast.Symbol)
	if !ok {
		return nil, errors.New("let takes a symbol as first argument")
	}
	if len(args) == 1 {
		value = nil
	} else {
		value, err = scope.Eval(args[1])
		if err != nil {
			return nil, err
		}
	}
	return nil, scope.Create(symbol.Name, value)
}

func letStarFn(s *twik.Scope) func(string, interface{}) error {
	return func(name string, v interface{}) error {
		return s.Create(name, v)
	}
}

func setStarFn(s *twik.Scope) func(string, interface{}) error {
	return func(name string, v interface{}) error {
		return s.Set(name, v)
	}
}

func get(iv int64, v interface{}) (interface{}, error) {
	// TODO:
	i := int(iv)
	vt := reflect.TypeOf(v)
	switch vt.Kind() {
	case reflect.Slice, reflect.String, reflect.Array:
	default:
		return nil, twikutil.NewTypeError(v, []string{"[]{}"})
	}

	vv := reflect.ValueOf(v)
	if vv.Len() <= i {
		return nil, errors.New("index out of range")
	}
	return vv.Index(i).Interface(), nil
}

func list(args ...interface{}) []interface{} { return args }

func listLen(list []interface{}) int64 { return int64(len(list)) }

func listAdd(list []interface{}, lists ...[]interface{}) []interface{} {
	for _, l := range lists {
		list = append(list, l...)
	}
	return list
}

func listAppend(list []interface{}, args ...interface{}) []interface{} {
	return append(list, args...)
}

func listRep(v interface{}, n int64) []interface{} {
	list := make([]interface{}, int(n))
	for i := range list {
		list[i] = v
	}
	return list
}

func slice(args []interface{}) (interface{}, error) {
	n := len(args)
	if n == 0 {
		return nil, errors.New("cannot create empty slice")
	}

	t := reflect.TypeOf(args[0])
	slice := reflect.MakeSlice(reflect.SliceOf(t), 0, n)
	for _, v := range args {
		vt := reflect.TypeOf(v)
		if vt != t {
			return nil, twikutil.NewTypeError(v, []string{t.String()})
		}
		slice = reflect.Append(slice, reflect.ValueOf(v))
	}
	return slice.Interface(), nil
}

func rangeFn(a, z int64) []int64 {
	var d, n int64
	if a < z {
		d = 1
		n = z - a
	} else {
		d = -1
		n = a - z
	}

	xs := make([]int64, n)
	for i := range xs {
		xs[i] = a
		a += d
	}
	return xs
}

type caseType struct {
	Default bool
	Match   interface{}
	Result  interface{}
}

func caseFn(match, result interface{}) *caseType {
	return &caseType{
		Match:  match,
		Result: result,
	}
}

func defaultFn(result interface{}) *caseType {
	return &caseType{
		Default: true,
		Result:  result,
	}
}

func switchFn(v interface{}, cases ...*caseType) (interface{}, error) {
	for i, c := range cases {
		if c.Default {
			// If c is the default, it needs to be the last case.
			if i != len(cases)-1 {
				return nil, errors.New("default case must be last in switch statement")
			}
			return c.Result, nil
		}
		if v == c.Match {
			return c.Result, nil
		}
	}
	return nil, errors.New("case not found; switch must be exhaustive")
}

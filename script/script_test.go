// Copyright (c) 2015, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package script_test

import (
	"os"
	"testing"

	"gitlab.com/dromi/dromi/script"
)

func TestRunability(z *testing.T) {
	stdout := os.Stdout
	os.Stdout = nil
	defer func() { os.Stdout = stdout }()

	for _, path := range []string{
		"testdata/test_ok_01.ds",
		"testdata/test_ok_02.ds",
		"testdata/test_ok_03.ds",
	} {
		_, err := script.Exec(path)
		if err != nil {
			z.Error(err)
		}
	}
}

func TestFailure1(z *testing.T) {
	_, err := script.Exec("testdata/test_fail_01.ds")
	if err == nil {
		z.Error("expected error")
	} else if err.Error() != "testdata/test_fail_01.ds:7:2: undefined symbol: unknownFunction" {
		z.Error("wrong error:", err)
	}
}

func TestFailure2(z *testing.T) {
	_, err := script.Exec("testdata/test_fail_02.ds")
	if err == nil {
		z.Error("expected error")
	} else if err.Error() != "testdata/test_fail_02.ds:2:2: undefined symbol: unknownFunction" {
		z.Error("wrong error:", err)
	}
}

// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package dromi

import (
	"errors"
	"fmt"
	"math/rand"

	log "github.com/Sirupsen/logrus"
	"gitlab.com/dromi/dromi/event"
	"gitlab.com/dromi/dromi/internal"
	"gitlab.com/dromi/dromi/internal/qa"
	"gitlab.com/dromi/dromi/mdl"
	"gitlab.com/dromi/dromi/script"
	"gitlab.com/dromi/dromi/simt"
)

func init() {
	script.CreateFunc("cl|shuffle", ClusterShuffle)
	script.CreateFunc("cl|rsubset", func(s rand.Source, n int64, c Cluster) Cluster {
		return ClusterRandomSubset(s, int(n), c)
	})
}

// Cluster interface and functions {{{1

// ErrBreak is what the function passed to Iterate can return to break
// out of the loop without Iterate returning an error.
var ErrBreak = errors.New("signal iterator to stop")

// A Cluster is essentially a set of machines.
type Cluster interface {
	// Init initializes the cluster with the simulator.
	//
	// The function exitFn is passed departing events;
	// the function Depart should call it.
	Initialize(exitFn func(e *event.Event)) error

	// Finalize cleans up as many resources as possible in the cluster.
	//
	// Finalize is called when the simulation is complete, although there
	// may still be pointers to the cluster and to the machines, they
	// will not be used.
	Finalize() error

	// Layer returns how many times the machines were wrapped.
	Layer() int

	// Len returns the number of machines in the cluster.
	Len() int

	// Time returns the maximum machine time among the cluster.
	//
	// Not every machine is guaranteed to have the same time,
	// however all machines should be less-or-equal to this time.
	Time() simt.Time

	// Machines returns the cluster underlying slice of machines.
	// This can be used to prevent an extra function call, which
	// greatly increases scheduling speed.
	Machines() []Machine

	// Get returns the i-th machine in the cluster.
	Get(i int) Machine

	// Set sets the i-th machine in the cluster to m.
	//
	// Warning: setting the machine for a particular cluster will only change
	// what the cluster sees. Many other parts of the simulation (such as
	// observers) may already have pointers to the machines.
	Set(i int, m Machine)

	// Iterate iterates through all machines in the cluster, in ascending index
	// order.
	//
	// The function fn is passed the index and the machine.
	// Calling m.Index() should also return the same as i.
	// If fn returns an error, iteration is halted and the error is returned,
	// unless the error is ErrBreak, in which case nil is returned.
	Iterate(fn func(i int, m Machine) error) error

	// Slice returns a slice between [a,b). The Machines are all wrapped in
	// *MachineIndexer, so that calls to Index refer to the returned cluster.
	Slice(a, b int) Cluster

	// Subset returns a new cluster with the machines at the indexes in xs
	// in the new cluster. The Machines are all wrapped in *MachineIndexer,
	// so that calls to Index refer to the returned cluster.
	Subset(xs []int) Cluster

	// Labels are used to tag certain parts of the cluster, that can be
	// used for observation and scheduling purposes.
	//
	// Note: Currently, labels are a product of the creation process, and
	// cannot be added by the user. This will change in a future release.
	Labels() map[string]Cluster

	// Assign is passed an event with e.Type == event.Assignment, and e.Data
	// should be the machine to which it is assigned.  The machine should be
	// from the cluster. If the scheduler split the cluster, then it should
	// unwrap the Machine (by casting to *MachineIndexer and getting the
	// underlying Machine).
	//
	// Pre-conditions
	//
	//  ∙ Cluster is not finished.
	//  ∙ Event.Type ≡ event.Assignment
	//  ∙ Event.Time ≥ Cluster.Time
	//  ∙ Event.Data type is Machine interface
	//  ∙ Assigned Machine:
	//      * actually exists in the cluster
	//      * has Time() ≤ (Event.Time ≡ Job.SchedEnd)
	//      * supports event
	//
	// Post-conditions
	//
	//  ∙ Cluster.Time ≡ Event.Time
	Assign(e *event.Event)

	// Depart is called by Machine after an event is completed.
	// Depart should call exitFn.
	//
	// Post-conditions
	//
	//  ∙ Event.Time ≡ Job.Departure
	//  ∙ Event.Type ≡ event.Departure
	//  ∙ Job properties:
	//      * Job.Arrival > Job.Sched[0]
	//      * len(Job.Sched) > 0
	//      * len(Job.Sched) ≡ len(Job.SchedDur)
	//      * Job.Sched[i] + Job.SchedDur[i] ≤ Job.Sched[i+1]
	//      * Job.Sched[$] + Job.SchedDur[$] ≡ Job.SchedEnd
	//      * Job.SchedEnd > 0
	//      * Job.Departure > 0
	//      * Job.Departure > Job.SchedEnd
	//
	// Note: This function may be removed in the future.
	Depart(e *event.Event)

	// Update updates every machine in the cluster to time t.
	//
	// If the simulation is using the LazyCluster optimization, this function
	// will never be called by the simulation, only by the cluster itself.
	//
	// Pre-conditions
	//
	//  ∙ Cluster is not finished.
	//
	// Post-conditions
	//
	//  ∙ Cluster.Time ≡ t
	Update(t simt.Time)

	// Finish calls Finish for every machine in the cluster.
	//
	// Pre-conditions
	//
	//  ∙ Cluster is not finished.
	//
	// Post-conditions
	//
	//  ∙ previous Cluster.Time ≤ returned time
	//  ∙ Cluster.Time ≡ returned time
	Finish() simt.Time
}

func ClusterRandomSubset(s rand.Source, n int, c Cluster) Cluster {
	xs := internal.List0N(c.Len())
	internal.ShuffleFirstN(s, n, xs)
	return c.Subset(xs)
}

func ClusterShuffle(s rand.Source, c Cluster) Cluster {
	indices := internal.List0N(c.Len())
	internal.Shuffle(s, indices)
	return c.Subset(indices)
}

// ClusterQA {{{2

// ClusterQA provides quality assurance for Cluster instances.
type ClusterQA struct {
	t        simt.Time
	finished bool

	Cluster
}

func init() { script.CreateFunc("qa|cluster", NewClusterQA) }

func NewClusterQA(c Cluster) Cluster {
	if _, ok := c.(*ClusterQA); ok {
		log.Warn("Ignoring cluster QA nesting")
		return c
	}
	return &ClusterQA{
		t:       c.Time(),
		Cluster: c,
	}
}

// Init passes the original cluster a modified exitFn for quality assurance.
func (c *ClusterQA) Initialize(exitFn func(e *event.Event)) error {
	const from = "Cluster.Depart"
	return c.Cluster.Initialize(func(e *event.Event) {
		t := e.Time
		if t != e.Job.Departure {
			qa.Panic(from, "departure time is not event time",
				qa.Expr("Event.Time ≡ Job.Departure: %v ≡ %v", t, e.Job.Departure),
				qa.Data("e", e),
			)
		}
		if e.Type != event.Departure {
			qa.Panic(from, "event type is not departure",
				qa.Data("e.Type", e.Type),
				qa.Data("e", e),
			)
		}
		if err := jobDepartureConsistent(e.Job); err != nil {
			qa.Panic(from, "job is not departure consistent",
				qa.Data("error", err),
				qa.Data("e", e),
			)
		}
		exitFn(e)
	})
}

// jobDepartureConsistent returns nil if j is internally consistent at departure,
// otherwise it returns an error.
//
//  ∙ Job properties:
//      * Job.Arrival > Job.Sched[0]
//      * len(Job.Sched) > 0
//      * len(Job.Sched) ≡ len(Job.SchedDur)
//      * Job.Sched[i] + Job.SchedDur[i] ≤ Job.Sched[i+1]
//      * Job.Sched[$] + Job.SchedDur[$] ≡ Job.SchedEnd
//      * Job.SchedEnd > 0
//      * Job.Departure > 0
//      * Job.Departure > Job.SchedEnd
func jobDepartureConsistent(j *mdl.Job) error {
	if j.Departure <= 0 {
		return fmt.Errorf("job departure time %v <= 0", j.Departure)
	}
	if j.SchedEnd <= 0 {
		return fmt.Errorf("job scheduling end %v not positive", j.SchedEnd)
	}
	if j.Departure <= j.SchedEnd {
		return fmt.Errorf("job departure time %v must be after scheduling end %v", j.Departure, j.SchedEnd)
	}
	n := len(j.Sched)
	if n <= 0 {
		return fmt.Errorf("job scheduling attempts %d <= 0", n)
	}
	if n != len(j.SchedDur) {
		return fmt.Errorf("job sched duration %d and ends %d inconsistent in lengths", n, len(j.SchedDur))
	}
	for i, st := range j.Sched[1:] {
		// Note: I am assuming that only one scheduling can go on at a time.
		if j.Sched[i]+simt.Time(j.SchedDur[i]) > st {
			return fmt.Errorf("job scheduling times don't line up")
		}
	}
	if j.Sched[n-1]+simt.Time(j.SchedDur[n-1]) != j.SchedEnd {
		return fmt.Errorf("job last scheduling time + duration don't add up to scheduling end")
	}
	if j.Arrival > j.Sched[0] {
		return fmt.Errorf("job arrival %v is after scheduling start %v", j.Arrival, j.Sched[0])
	}
	return nil
}

// Slice wraps the returned Cluster in a new ClusterQA.
func (c *ClusterQA) Slice(a, b int) Cluster {
	return NewClusterQA(c.Cluster.Slice(a, b))
}

// Subset wraps the returned Cluster in a new ClusterQA.
func (c *ClusterQA) Subset(xs []int) Cluster {
	return NewClusterQA(c.Cluster.Subset(xs))
}

// Assign performs quality assurance on the Assign function of a Cluster.
func (c *ClusterQA) Assign(e *event.Event) {
	const from = "Cluster.Assign"

	// Pre-conditions
	//
	//  ∙ Cluster is not finished.
	if c.finished {
		qa.Panic(from, "cluster already finished", qa.Data("e", e))
	}
	//  ∙ Event.Type ≡ event.Assignment
	if e.Type != event.Assignment {
		qa.Panic(from, "event type must be assignment",
			qa.Expr("e.Type ≡ event.Assignment: %s ≡ Assignment", e.Type),
			qa.Data("e", e),
		)
	}
	//  ∙ Event.Time ≡ Event.Job.SchedEnd
	if e.Time != e.Job.SchedEnd {
		qa.Panic(from, "event time should equal job scheduling end",
			qa.Expr("e.Time ≡ e.Job.SchedEnd: %v ≡ %v", e.Time, e.Job.SchedEnd),
			qa.Data("e", e),
		)
	}
	//  ∙ Event.Time ≥ Cluster.Time
	if e.Time < c.t {
		qa.Panic(from, "event time should be after cluster time",
			qa.Expr("e.Time ≥ Cluster.Time: %v ≥ %v", e.Time, c.t),
			qa.Data("e", e),
		)
	}
	//  ∙ Event.Data type is Machine interface
	m, ok := e.Data.(Machine)
	if !ok {
		qa.Panic(from, "Event.Data must be of type Machine",
			qa.Data("e.Data", e.Data),
			qa.Data("e", e),
		)
	}
	//  ∙ Assigned Machine:
	//      * actually exists in the cluster
	//      * has Time() ≤ (Event.Time ≡ Job.SchedEnd)
	//      * supports event
	layer := c.Layer()
	id := m.IndexAt(layer)
	if id >= c.Len() {
		qa.Panic(from, "machine id past cluster length",
			qa.Expr("Machine.Index() < Cluster.Len(): %d < %d", id, c.Len()),
			qa.Data("e", e),
		)
	}
	mu := m.UnwrapTo(layer)
	if c.Get(id) != mu {
		qa.Panic(from, "machine is not the same as the one existing in cluster",
			qa.Data("mu", mu),
			qa.Data("c.Get(id)", c.Get(id)),
		)
	}
	if mu.Time() > e.Time {
		qa.Panic(from, "machine time is after event time",
			qa.Expr("Machine.Time() ≤ Event.Time: %v ≤ %v", mu.Time(), e.Time),
			qa.Data("e", e),
		)
	}
	if !MachineSupports(mu, e) {
		qa.Panic(from, "machine does not support job",
			qa.Data("e", e),
			qa.Data("machine", mu),
			qa.Data("jobq", mu.Jobs()),
		)
	}

	// After passing e, we cannot use it anymore.
	c.t = e.Time
	c.Cluster.Assign(e)

	// Unchecked post-conditions
	//
	//  ∙ Cluster.Time ≡ Event.Time
}

func (c *ClusterQA) Update(t simt.Time) {
	const from = "Cluster.Update"
	if c.finished {
		qa.Panic(from, "cluster already finished", qa.Data("t", t), qa.Data("c.Time()", c.t))
	}

	c.Cluster.Update(t)
	ct := c.Cluster.Time()
	if ct != t {
		qa.Panic(from, "cluster time must equal updated time", qa.Expr("Cluster.Time() ≡ t: %v ≡ t", ct, t))
	}
	c.t = t
}

func (c *ClusterQA) Finish() simt.Time {
	const from = "Cluster.Finish"
	if c.finished {
		qa.Panic(from, "cluster already finished", qa.Data("c.Time()", c.t))
	}

	ft := c.Cluster.Finish()
	if ft < c.t {
		qa.Panic(from, "finish time in the past", qa.Expr("finishTime ≥ previous Cluster.Time: %v ≥ %v", ft, c.t))
	}
	if c.Cluster.Time() != ft {
		qa.Panic(from, "cluster time inconsistent with finish time",
			qa.Expr("finishTime ≡ current Cluster.Time: %v ≡ %v", ft, c.Cluster.Time()),
		)
	}
	c.t = ft
	c.finished = true
	return ft
}

// Machine interface, types, and functions {{{1

type Machine interface {
	Initialize(c Cluster, idx int) error

	// Finalize may be called after the simulation is complete to clean up
	// resources.
	Finalize() error

	Layer() int

	String() string
	// Index returns the index of the machine in the cluster.
	Index() int
	IndexAt(layer int) int

	// Unwrap returns the wrapped machine, or nil if it is already at layer 0.
	Unwrap() Machine

	// UnwrapTo unwraps as many layers as necessary to get to the specified layer.
	// If layer is greater than the current layer, then it panics.
	UnwrapTo(layer int) Machine

	// Hardware returns a pointer to the base hardware of the machine.
	// A pointer is returned for performance reasons, it should not be modified.
	Hardware() *mdl.Hardware
	// Available returns a pointer to the available hardware of the machine.
	// A pointer is returned for performance reasons, it should not be modified.
	Available() *mdl.Hardware
	// Utilization returns the current hardware utilization.
	Utilization() *mdl.HardwareUtilization
	// NJobs returns the number of jobs currently running on the machine.
	NJobs() int
	// Jobs returns a pointer to the internal heap of events.
	// A pointer is returned for performance reasons, it should not be modified.
	Jobs() []*event.Event

	RegisterObserver(o MachineObserver)

	Insert(e *event.Event)

	Time() simt.Time
	Update(t simt.Time)
	Finish() simt.Time
}

type MachineFunc func() Machine

func MachineSupports(m Machine, e *event.Event) bool {
	m.Update(e.Time)
	return m.Available().Supports(&e.Job.Pod)
}

func MachineIsEmpty(m Machine) bool {
	return m.NJobs() == 0
}

func MachineUtilization(m Machine) float64 {
	if m.NJobs() == 0 {
		return 0.0
	} else {
		return 1.0
	}
}

// MachineObserver and MachineObservers {{{2

type MachineObserver interface {
	Insert(e *event.Event)
	Remove(e *event.Event)
	Update(t simt.Time)
}

type MachineObservers []MachineObserver

func (obs MachineObservers) Insert(e *event.Event) {
	for _, o := range obs {
		o.Insert(e)
	}
}

func (obs MachineObservers) Remove(e *event.Event) {
	for _, o := range obs {
		o.Remove(e)
	}
}

// }}}

// MachineIndexer {{{1

// MachineIndexer makes it possible to partition clusters,
// retaining access to the original index, and having a current partition index.
type MachineIndexer struct {
	Machine

	layer int32
	idx   int32
}

func NewMachineIndexer(m Machine, i int) *MachineIndexer {
	return &MachineIndexer{
		Machine: m,
		layer:   int32(m.Layer() + 1),
		idx:     int32(i),
	}
}

func (m MachineIndexer) String() string        { return fmt.Sprintf("Machine{%d/%d}", m.layer, m.idx) }
func (m MachineIndexer) Layer() int            { return int(m.layer) }
func (m MachineIndexer) Index() int            { return int(m.idx) }
func (m MachineIndexer) IndexAt(layer int) int { return m.UnwrapTo(layer).Index() }
func (m MachineIndexer) Unwrap() Machine       { return m.Machine }
func (m MachineIndexer) UnwrapTo(layer int) Machine {
	// If layer is greater than m.layer, we will keep decrementing
	// the following i until a panic occurs. This is a good thing,
	// because it means we don't have to explicitely check if layer
	// is ok.
	var a Machine = m
	for i := int(m.layer); i != layer; i-- {
		a = a.Unwrap()
	}
	return a
}

// Compile-time interface compliancy check.
var _ = Machine(new(MachineIndexer))

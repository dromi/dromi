// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package event

import (
	"container/heap"

	"gitlab.com/dromi/dromi/script"
	"gitlab.com/dromi/dromi/simt"
)

type Queue interface {
	// Len returns the number of events currently in the queue.
	Len() int

	// Head returns the next event that will be popped.
	Head() *Event

	// Push inserts the event into the queue and returns true on success.
	//
	// If the queue is capped, false is returned when the limit is reached,
	// that is the queue size is equal to the cap.
	Push(*Event) bool

	// Pop returns the next event (according to the queuing strategy),
	// or nil if there is no next event.
	Pop() *Event

	// View returns a slice of events
	View() []*Event

	// Reset should reset the queue to a zero state, such that the memory
	// can be reclaimed by the garbage collector.
	Reset()
}

func init() {
	script.CreateFunc("eq|cap", func(q Queue, cap int64) *CappedQueue { return NewCappedQueue(q, int(cap)) })
	script.CreateFunc("eq.linkedlist", NewLinkedList)
	script.CreateFunc("eq.arraylist", func(n int64) *ArrayList { return NewArrayList(int(n)) })
	script.CreateFunc("eq.stack", func(n int64) *Stack { return NewStack(int(n)) })
	script.CreateFunc("eq.heap/event", func(n int64) *Heap { return NewHeap(ByEvent, int(n)) })
	script.CreateFunc("eq.heap/time", func(n int64) *Heap { return NewHeap(ByTime, int(n)) })
	script.CreateFunc("eq.heap/runtime", func(n int64) *Heap { return NewHeap(ByRuntime, int(n)) })
	script.CreateFunc("eq.heap/priority", func(n int64) *Heap { return NewHeap(ByPriority, int(n)) })
	script.CreateFunc("eq.heap/deadline", func(n int64) *Heap { return NewHeap(ByDeadline, int(n)) })
}

// CappedQueue {{{1

type CappedQueue struct {
	Queue     // underlying queue implementation
	cap   int // cap
}

func NewCappedQueue(q Queue, cap int) *CappedQueue {
	if cap < 0 {
		panic("cap cannot be negative")
	}
	return &CappedQueue{q, cap}
}

func (q *CappedQueue) Cap() int {
	return q.cap
}

func (q *CappedQueue) Push(e *Event) bool {
	if q.Len() == q.cap {
		return false
	}
	return q.Queue.Push(e)
}

// LinkedList {{{1

type LinkedList struct {
	head *link
	tail *link
	sz   int
}

func NewLinkedList() *LinkedList {
	return &LinkedList{}
}

func (q *LinkedList) Len() int {
	return q.sz
}

func (q *LinkedList) Head() *Event {
	if q.sz == 0 {
		return nil
	}
	return q.head.e
}

func (q *LinkedList) View() []*Event {
	es := make([]*Event, q.sz)
	lnk := q.head
	for i := range es {
		es[i] = lnk.e
		lnk = lnk.next
	}
	return es
}

func (q *LinkedList) Push(e *Event) bool {
	lnk := &link{e, nil}
	if q.sz == 0 {
		q.head = lnk
	} else {
		q.tail.next = lnk
	}
	q.tail = lnk
	q.sz++
	return true
}

func (q *LinkedList) Pop() *Event {
	if q.sz == 0 {
		return nil
	} else if q.sz == 1 {
		// This is not really necessary, but it can free up some space.
		q.tail = nil
	}
	lnk := q.head
	q.head = lnk.next
	q.sz--
	return lnk.e
}

func (q *LinkedList) Reset() {
	q.head, q.tail = nil, nil
	q.sz = 0
}

type link struct {
	e    *Event
	next *link
}

var _ = Queue(new(LinkedList))

// ArrayList {{{1

type ArrayList struct {
	es []*Event
	sz int
	a  int // index to the next to be popped event
	z  int // index to where the next event can be saved
}

func NewArrayList(n int) *ArrayList {
	return &ArrayList{
		es: make([]*Event, n),
	}
}

func (q *ArrayList) Len() int {
	return q.sz
}

func (q *ArrayList) Head() *Event {
	if q.sz == 0 {
		return nil
	}
	return q.es[q.a]
}

func (q *ArrayList) View() []*Event {
	if q.a <= q.z {
		// [...a+++++z....]
		return q.es[q.a:q.z]
	} else {
		// [++z.......a+++]
		es := make([]*Event, q.sz)
		q.copyTo(es)
		return es
	}
}

func (q *ArrayList) copyTo(dst []*Event) {
	a, n := q.a, len(q.es)
	for i := range dst[0:q.sz] {
		dst[i] = q.es[a%n]
		a++
	}
}

func (q *ArrayList) Push(e *Event) bool {
	if q.sz == len(q.es) {
		// We do not have enough space, in which case
		//  q.sz == len(q.es) and
		//  q.a == q.z.
		// We then have to create a new backing queue. We can just extend
		// the current one if q.a is not too far away from 0. Otherwise
		// we have to create a new one and copy everything over.
		// For now: no premature optimization.
		var cap int
		if q.sz == 0 {
			cap = 1
		} else {
			cap = q.sz << 1
		}

		es := make([]*Event, cap)
		q.copyTo(es)
		q.es = es
		q.a, q.z = 0, q.sz
	}

	q.es[q.z] = e
	q.z = (q.z + 1) % len(q.es)
	q.sz++
	return true
}

func (q *ArrayList) Pop() *Event {
	if q.sz == 0 {
		return nil
	}

	e := q.es[q.a]
	q.a = (q.a + 1) % len(q.es)
	q.sz--
	return e
}

func (q *ArrayList) Reset() {
	q.es = make([]*Event, 0)
	q.sz = 0
	q.a, q.z = 0, 0
}

var _ = Queue(new(ArrayList))

// Stack {{{1

type Stack struct {
	es []*Event
}

func NewStack(n int) *Stack {
	return &Stack{
		es: make([]*Event, 0, n),
	}
}

func (s *Stack) Len() int {
	return len(s.es)
}

func (s *Stack) Head() *Event {
	if len(s.es) == 0 {
		return nil
	}
	return s.es[len(s.es)-1]
}

func (s *Stack) View() []*Event {
	return s.es
}

func (s *Stack) Push(e *Event) bool {
	s.es = append(s.es, e)
	return true
}

func (s *Stack) Pop() *Event {
	if len(s.es) == 0 {
		return nil
	}
	m := len(s.es) - 1
	e := s.es[m]
	s.es = s.es[0:m]
	return e
}

func (s *Stack) Reset() {
	s.es = make([]*Event, 0)
}

var _ = Queue(new(Stack))

// Heap {{{1

type LessFunc func(a, b *Event) bool

func ByEvent(a, b *Event) bool    { return a.Time < b.Time || (a.Time == b.Time && a.Type < b.Type) }
func ByTime(a, b *Event) bool     { return a.Time < b.Time }
func ByRuntime(a, b *Event) bool  { return a.Job.Pod.Runtime < b.Job.Pod.Runtime }
func ByPriority(a, b *Event) bool { return a.Job.Pod.Priority < b.Job.Pod.Priority }
func ByDeadline(a, b *Event) bool { return a.Job.Pod.Deadline < b.Job.Pod.Deadline }

type Heap struct {
	es events
}

func NewHeap(lessFn LessFunc, n int) *Heap {
	es := events{
		all: make([]*Event, 0, n),
		fn:  lessFn,
	}
	heap.Init(&es)
	return &Heap{es}
}

func (h *Heap) Len() int {
	return len(h.es.all)
}

func (h *Heap) Head() *Event {
	if h.Len() == 0 {
		return nil
	}
	return h.es.all[0]
}

func (h *Heap) View() []*Event {
	return h.es.all
}

func (h *Heap) Push(e *Event) bool {
	heap.Push(&h.es, e)
	return true
}

func (h *Heap) Pop() *Event {
	return heap.Pop(&h.es).(*Event)
}

func (h *Heap) Reset() {
	h.es.all = make([]*Event, 0)
}

// Special non-queue methods, in case events are changed.

func (h *Heap) Init()     { heap.Init(&h.es) }
func (h *Heap) Fix(i int) { heap.Fix(&h.es, i) }

type events struct {
	all []*Event
	fn  LessFunc
}

func (es events) Len() int {
	return len(es.all)
}

func (es events) Less(i, j int) bool {
	return es.fn(es.all[i], es.all[j])
}

func (es events) Swap(i, j int) {
	es.all[i], es.all[j] = es.all[j], es.all[i]
	es.all[i].index = i
	es.all[j].index = j
}

func (es *events) Push(x interface{}) {
	n := len(es.all)
	e := x.(*Event)
	e.index = n
	es.all = append(es.all, e)
}

func (es *events) Pop() interface{} {
	m := len(es.all) - 1
	e := es.all[m]
	e.index = -1
	es.all = es.all[0:m]
	return e
}

var _ = Queue(new(Heap))

// EventHeap {{{1

// EventHeap is a special heap where the lowest event time has the highest priority.
// This heap is used for machine and global simulation. As such, on implementation
// level it may be modified for improved performance.
//
// It also contains an extra function NextTime.
type EventHeap struct {
	Heap
}

func NewEventHeap(n int) *EventHeap {
	return &EventHeap{
		Heap: *NewHeap(ByEvent, n),
	}
}

func (h *EventHeap) NextTime() simt.Time {
	if h.Len() == 0 {
		return simt.Time(simt.Max)
	}
	return h.Head().Time
}

// CountingEventHeap {{{1

// CountingEventHeap is an EventHeap that keeps track of what kind of event types
// the heap contains. After freeing the heap, the count remains, so it should not
// be used after freeing.
type CountingEventHeap struct {
	EventHeap
	count map[Type]int
}

func NewCountingEventHeap(n int) *CountingEventHeap {
	return &CountingEventHeap{
		EventHeap: *NewEventHeap(n),
		count:     make(map[Type]int),
	}
}

func (h *CountingEventHeap) Push(e *Event) bool {
	h.count[e.Type]++
	return h.Heap.Push(e)
}

// Popping an empty CountingEventHeap will panic.
func (h *CountingEventHeap) Pop() *Event {
	e := h.Heap.Pop()
	// In our use of CountingEventHeap, the queue will never be empty,
	// so we don't need to check if e is nil or not.
	h.count[e.Type]--
	return e
}

func (h *CountingEventHeap) Count() map[Type]int {
	return h.count
}

// Copyright (c) 2015, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package event

var observerTypes = []Type{
	Arrival,
	Departure,
	Eviction,
	Rejection,
	Submission,
	SchedulingFailure,
	SchedulingConflict,
	SchedulingError,
	Assignment,
	AssignmentError,
}

func msig(t Type) int {
	var i int
	for t != 0 {
		i++
		t = t >> 1
	}
	return i
}

type Observers [][]func(*Event)

func NewObservers() [][]func(*Event) {
	return make([][]func(*Event), msig(lastType)-1)
}

func (m Observers) Add(t Type, f func(e *Event)) (assigned bool) {
	for _, et := range observerTypes {
		if t&et != 0 {
			assigned = true
			i := msig(et)
			m[i] = append(m[i], f)
		}
	}
	return
}

func (m Observers) Observe(e *Event) {
	for _, o := range m[msig(e.Type)] {
		o(e)
	}
}

func (m Observers) ObserveType(t Type, e *Event) {
	for _, o := range m[msig(t)] {
		o(e)
	}
}

// Copyright (c) 2015, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package event

import (
	"fmt"
	"strings"

	"gitlab.com/dromi/dromi/mdl"
	"gitlab.com/dromi/dromi/script"
	"gitlab.com/dromi/dromi/simt"
)

func init() {
	script.CreateFunc("event", ParseType)
}

type Type int

// This is an ordered list of event types. The order determines precedence
// when the time is the same.
const (
	Unknown Type = 0
	Error   Type = 1 << (iota - 1)

	Departure
	Eviction
	Rejection

	Assignment
	AssignmentError

	Submission
	SchedulingFailure
	SchedulingConflict
	SchedulingError

	Arrival

	Observation
	Reset

	lastType
)

var strTypes = map[string]Type{
	"error":              Error,
	"arrival":            Arrival,
	"departure":          Departure,
	"eviction":           Eviction,
	"rejection":          Rejection,
	"schedulingfailure":  SchedulingFailure,
	"schedulingconflict": SchedulingConflict,
	"schedulingerror":    SchedulingError,
	"assignment":         Assignment,
	"assignmenterror":    AssignmentError,
	"observation":        Observation,
	"reset":              Reset,
}

var typeStrs = map[Type]string{
	Unknown:            "Unknown",
	Error:              "Error",
	Arrival:            "Arrival",
	Departure:          "Departure",
	Eviction:           "Eviction",
	Rejection:          "Rejection",
	Assignment:         "Assignment",
	AssignmentError:    "AssignmentError",
	Submission:         "Submission",
	SchedulingFailure:  "SchedulingFailure",
	SchedulingConflict: "SchedulingConflict",
	SchedulingError:    "SchedulingError",
	Observation:        "Observation",
	Reset:              "Reset",
}

func (t Type) String() string {
	// TODO: Need to improve this so it handles types put together (just like reading)
	if s, ok := typeStrs[t]; ok {
		return s
	}
	return "Unknown"
}

func ParseType(s string) (Type, error) {
	if s == "" {
		return Unknown, nil
	}

	var t Type
	fs := strings.Split(strings.ToLower(s), "|")
	for _, f := range fs {
		v, ok := strTypes[f]
		if !ok {
			return 0, fmt.Errorf("unknown event type %q", f)
		}
		t = t | v
	}
	return t, nil
}

type Event struct {
	Time simt.Time
	Type Type

	Job *mdl.Job

	// Data contains extra information, that may vary depending on the event type:
	//
	//  Arrival: nil
	//  Submission: nil
	//  Assignment: dromi.Machine
	//  Departure: dromi.Machine
	//  AssignmentError: error
	//  SchedulingError: error
	//  Observation: func(dromi.State)
	Data interface{}

	index int
}

func (e Event) String() string {
	return fmt.Sprintf("Event{%v %s %s (%v)}", e.Time, e.Type, e.Job, e.Data)
}

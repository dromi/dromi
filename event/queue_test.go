// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package event

import (
	"fmt"
	"math/rand"
	"sort"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/dromi/dromi/simt"
)

// Test Queues {{{

func TestLinkedList(z *testing.T) {
	testQueue(z, func() Queue { return NewLinkedList() })
}

func TestArrayList(z *testing.T) {
	assert := assert.New(z)

	assert.Panics(func() { NewArrayList(-1) })
	testQueue(z, func() Queue { return NewArrayList(0) })
	testQueue(z, func() Queue { return NewArrayList(1) })
	testQueue(z, func() Queue { return NewArrayList(2) })
	testQueue(z, func() Queue { return NewArrayList(4) })
	testQueue(z, func() Queue { return NewArrayList(64) })
	testQueue(z, func() Queue { return NewArrayList(128) })
	testQueue(z, func() Queue { return NewArrayList(1024) })
	testQueue(z, func() Queue { return NewArrayList(1000) })
	testQueue(z, func() Queue { return NewArrayList(1000) })
}

func testQueue(z *testing.T, fn func() Queue) {
	assert := assert.New(z)

	tests := []qsTest{
		qsTest{[]int{-1, -1, -1}, []int{}},
		qsTest{[]int{}, []int{}},
		qsTest{[]int{1, 2, 3, -1}, []int{2, 3}},
		qsTest{[]int{1, 2, 3, 4, 5}, []int{1, 2, 3, 4, 5}},
		qsTest{[]int{1, 2, 3, 4, -1, 1, -1, -1}, []int{4, 1}},
		qsTest{[]int{1, -1, 1, -1, -1, 1, -1, 1, -1}, []int{}},
		qsTest{[]int{1, 2, 3, 4, 5, 6, 7, 8, 9}, []int{1, 2, 3, 4, 5, 6, 7, 8, 9}},
	}

	q := fn()
	assert.Equal(0, q.Len())
	assert.Equal(q.View(), []*Event{})
	assert.Nil(q.Head())
	testQueueInsert(z, q, 1000)

	for _, t := range tests {
		q = fn()
		t.Test(z, q)

		q.Reset()
		assert.Equal(0, q.Len())
		assert.Equal(q.View(), []*Event{})
		assert.Nil(q.Head())
	}

}

func testQueueInsert(z *testing.T, q Queue, n int) {
	assert := assert.New(z)
	for i := 1; i < n; i++ {
		q.Push(&Event{Data: i})
	}
	for i := 1; i < n; i++ {
		e := q.Pop()
		assert.NotNil(e)
		assert.Equal(i, e.Data.(int))
	}
	assert.Nil(q.Pop())
}

type qsTest struct {
	Ops []int
	Out []int
}

func (t qsTest) Test(z *testing.T, q Queue) {
	assert := assert.New(z)

	var sz, oi int
	defer func() {
		x := recover()
		if x != nil {
			fmt.Println("-- PANIC --")
			fmt.Println("  Ops:", t.Ops)
			fmt.Println("  oi:", oi)
			fmt.Println("  sz:", sz)
			panic(x)
		}
	}()
	for i, op := range t.Ops {
		oi = i
		if op < 0 {
			h := q.Head()
			e := q.Pop()
			assert.Equal(h, e)
			if sz == 0 {
				assert.Nil(e)
			} else {
				sz--
			}
		} else {
			q.Push(&Event{Data: op})
			sz++
		}
		assert.Equal(sz, q.Len())
	}
	oi = -1 // signal end of operations

	// Check that the view makes sense
	es := q.View()
	assert.Equal(len(t.Out), len(es))
	assert.Equal(sz, len(es))
	for i, e := range es {
		assert.Equal(t.Out[i], e.Data.(int))
	}

	// Check that we pop everything
	for _, o := range t.Out {
		assert.Equal(sz, q.Len())
		e := q.Pop()
		assert.Equal(o, e.Data.(int))
		sz--
	}
	assert.Equal(0, q.Len())
	assert.Nil(q.Pop())
}

// }}}

// Test Stacks {{{

func TestStack(z *testing.T) {
	assert := assert.New(z)
	assert.Panics(func() { NewStack(-1) })
}

// }}}

// Test Heaps {{{

func TestPriorityHeap(z *testing.T) {}

func TestTimeHeap(z *testing.T) {}

func TestRuntimeHeap(z *testing.T) {}

const (
	testMaxDuration = 24 * simt.Hour
	testMaxListSize = 100
)

func TestEventHeap2(z *testing.T) {
	s := rand.NewSource(42)
	r := rand.New(s)

	testIterations := 10000
	if testing.Short() {
		testIterations = 100
	}

	for i := 0; i < testIterations; i++ {
		testAllEventHeap(z, r)
	}
}

func testAllEventHeap(z *testing.T, r *rand.Rand) {
	es := createList(r, rand.Intn(testMaxListSize))
	err := testEventHeap(es)
	if err != nil {
		z.Error(err)
	}
	sort.Sort(es)
	err = testEventHeap(es)
	if err != nil {
		z.Error(err)
	}
	sort.Sort(sort.Reverse(es))
	err = testEventHeap(es)
	if err != nil {
		z.Error(err)
	}
}

func testEventHeap(es events) error {
	q := NewHeap(ByEvent, 8)
	for _, e := range es.all {
		q.Push(e)
	}
	start := simt.Time(0)
	for q.Len() != 0 {
		e := q.Head()
		t := e.Time
		v := q.Pop()
		if v != e {
			return fmt.Errorf("expected Head() == Pop(); got %v != %v", e.Time, v.Time)
		}
		if e.Time != t {
			return fmt.Errorf("expected Earliest() == Pop().Time; got %v != %v", t, e.Time)
		}
		if t < start {
			return fmt.Errorf("expected monotonically increasing results; got %v > %v", start, t)
		}
	}
	return nil
}

func createList(r *rand.Rand, size int) events {
	es := events{
		all: make([]*Event, size),
		fn:  ByEvent,
	}
	for i := range es.all {
		t := simt.Time(rand.Int63n(int64(testMaxDuration)))
		es.all[i] = &Event{
			Time: t,
			Type: Arrival,
		}
	}
	return es
}

// }}}

// Benchmark Queues {{{

func BenchmarkLinkedListPush(b *testing.B) {
	q := NewLinkedList()
	for i := 0; i < b.N; i++ {
		q.Push(&Event{Data: i})
	}
}

func BenchmarkArrayListFastPush(b *testing.B) {
	q := NewArrayList(b.N)
	for i := 0; i < b.N; i++ {
		q.Push(&Event{Data: i})
	}
}

func BenchmarkArrayListPush(b *testing.B) {
	q := NewArrayList(0)
	for i := 0; i < b.N; i++ {
		q.Push(&Event{Data: i})
	}
}

// }}}

// Copyright (c) 2015, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package api

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDimensions(z *testing.T) {
	assert := assert.New(z)

	d := NewDimensions()
	assert.Equal(d.Total(), 0, "when there are no dimensions, total should be 0")
	d.Create("key1", "Key 1", []interface{}{0, 1, 2, 3, 4})
	assert.Equal(d.Total(), 5, "there are now five cases")
	for i := 0; i < 4; i++ {
		v, err := d.Value("key1")
		assert.Nil(err)
		assert.Equal(v, i, "should line up")
		assert.Equal(d.Next(), true, "4 times rotating ok")
		assert.NotEqual(d.Get("key1").Index(), 0, "should not be zero")
	}
	assert.Equal(d.Next(), false, "last time should reset")
	d.Create("key2", "Key 2", []interface{}{1.0, 10.0, 15.0, 20.0})
	assert.Equal(d.Total(), 20, "now we have 4×5 = 20 cases")
	for i := 0; i < 19; i++ {
		assert.Equal(d.Next(), true, "19 times rotating ok")
	}
	assert.Equal(d.Next(), false, "last time should reset")
	assert.Equal(d.Get("key1").Index(), 0, "should be zero")
	assert.Equal(d.Get("key2").Index(), 0, "should be zero")
}

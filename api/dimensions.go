// Copyright (c) 2015, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"github.com/goulash/csv"
)

// Dimension {{{

type Dimension struct {
	id     string
	name   string
	values []interface{}
	index  int
}

func (d Dimension) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf(`{"id":%q,"name":%q,"index":%d,"value":%#v}`, d.id, d.name, d.index, d.Value())), nil
}

func (d Dimension) Header() []string { return []string{"id", "name", "index", "value"} }
func (d Dimension) Record() []string {
	return []string{d.id, d.name, strconv.FormatInt(int64(d.index), 10), fmt.Sprintf("%v", d.Value())}
}
func (d Dimension) String() string     { return fmt.Sprintf("%s=%v", d.name, d.Value()) }
func (d Dimension) Clone() *Dimension  { return &Dimension{d.id, d.name, d.values, d.index} }
func (d Dimension) ID() string         { return d.id }
func (d Dimension) Name() string       { return d.name }
func (d Dimension) Value() interface{} { return d.values[d.index] }
func (d Dimension) Index() int         { return d.index }
func (d Dimension) Len() int           { return len(d.values) }

// }}}

// Point {{{

type Point []*Dimension

func (p Point) String() string {
	idx := p.Indices()
	sidx := make([]string, len(idx))
	for i, v := range idx {
		sidx[i] = strconv.FormatInt(int64(v), 10)
	}
	res := make([]string, len(idx))
	for i, v := range p {
		res[i] = v.String()
	}
	return fmt.Sprintf("(%s): %s", strings.Join(sidx, ","), strings.Join(res, ", "))
}

func (p Point) Len() int {
	return len(p)
}

func (p Point) Header() []string {
	header := make([]string, len(p))
	for i, d := range p {
		header[i] = d.id
	}
	return header
}

func (p Point) Record() []string {
	record := make([]string, len(p))
	for i, d := range p {
		record[i] = fmt.Sprintf("%v", d.Value())
	}
	return record
}

func (p Point) Indices() []int {
	ids := make([]int, len(p))
	for i, d := range p {
		ids[i] = d.index
	}
	return ids
}

// }}}

var (
	ErrBadDim    = errors.New("dimension does not exist")
	ErrDimEmpty  = errors.New("dimension is empty")
	ErrDimExists = errors.New("dimension already exists")
)

type Dimensions struct {
	order    []string
	dims     map[string]*Dimension
	readOnly bool
}

func NewDimensions() *Dimensions {
	return &Dimensions{
		order: []string{},
		dims:  map[string]*Dimension{},
	}
}

func (d Dimensions) Names() []string {
	dst := make([]string, len(d.order))
	copy(dst, d.order)
	return dst
}

func (d Dimensions) Len() int { return len(d.order) }

func (d *Dimensions) Configure(e *Dimensions, readOnly bool) {
	d.readOnly = readOnly
	for _, v := range e.order {
		d.order = append(d.order, v)
	}
	for k, v := range e.dims {
		d.dims[k] = v.Clone()
	}
}

func (d *Dimensions) Create(id, name string, xs []interface{}) error {
	if d.readOnly {
		// After finalizing with another dimensions configuration, we ignore further creations.
		return nil
	} else if d.Exists(id) {
		return ErrDimExists
	} else if len(xs) == 0 {
		return ErrDimEmpty
	}

	d.order = append(d.order, id)
	d.dims[id] = &Dimension{id, name, xs, 0}
	return nil
}

func (d Dimensions) Exists(id string) bool {
	_, ok := d.dims[id]
	return ok
}

func (d Dimensions) Get(id string) *Dimension {
	return d.dims[id]
}

func (d Dimensions) Value(id string) (interface{}, error) {
	if !d.Exists(id) {
		return nil, ErrBadDim
	}
	return d.dims[id].Value(), nil
}

func (d Dimensions) Current() Point {
	pt := make(Point, len(d.order))
	for i, k := range d.order {
		pt[i] = d.dims[k].Clone()
	}
	return pt
}

func (d *Dimensions) Next() (ok bool) {
	if d.readOnly {
		return false
	}

	n := len(d.order)
	for i := n - 1; i >= 0; i-- {
		id := d.order[i]

		k := d.dims[id]
		k.index = (k.index + 1) % k.Len()
		if k.index != 0 {
			ok = true
			break
		}
	}

	// If we don't break at all in the loop, then we have wrapped around
	// completely and therefore there are no more experiments.
	return
}

func (d Dimensions) Total() int {
	if len(d.order) == 0 {
		return 0
	}

	total := 1
	for _, v := range d.dims {
		total *= v.Len()
	}
	return total
}

func (d Dimensions) MarshalJSON() ([]byte, error) {
	dims := make([]dim, len(d.order))
	for i, x := range d.order {
		dims[i].ID = x
		dims[i].Name = d.dims[x].Name()
		dims[i].Size = d.dims[x].Len()
	}
	return json.Marshal(dims)
}

func (d Dimensions) MarshalCSV() ([]byte, error) {
	dims := make([]dim, len(d.order))
	for i, x := range d.order {
		dims[i].ID = x
		dims[i].Name = d.dims[x].Name()
		dims[i].Size = d.dims[x].Len()
	}
	return csv.Marshal(dims)
}

type dim struct {
	ID   string `json:"id"`
	Name string `json:"name"`
	Size int    `json:"size"`
}

func (d dim) Header() []string { return []string{"id", "name", "size"} }
func (d dim) Record() []string { return []string{d.ID, d.Name, strconv.FormatInt(int64(d.Size), 10)} }

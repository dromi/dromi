// Copyright (c) 2015, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package api

import (
	"errors"
	"fmt"
	"math/rand"
	"path/filepath"
	"reflect"
	"time"

	"github.com/Jeffail/tunny"
	log "github.com/Sirupsen/logrus"
	"github.com/goulash/errs"
	"github.com/goulash/twikutil"
	"github.com/goulash/twikutil/key"
	"github.com/spf13/pflag"
	"gitlab.com/dromi/dromi"
	"gitlab.com/dromi/dromi/script"
	"gitlab.com/dromi/dromi/simt"
	"gitlab.com/dromi/dromi/webutil"

	// The following imports are so all DromiScript functions are loaded.
	_ "gitlab.com/dromi/dromi"
	_ "gitlab.com/dromi/dromi/cl"
	_ "gitlab.com/dromi/dromi/event"
	_ "gitlab.com/dromi/dromi/gen"
	_ "gitlab.com/dromi/dromi/mdl"
	_ "gitlab.com/dromi/dromi/mux"
	_ "gitlab.com/dromi/dromi/obs"
	_ "gitlab.com/dromi/dromi/presets"
	_ "gitlab.com/dromi/dromi/presets/borg"
	_ "gitlab.com/dromi/dromi/presets/thesis"
	_ "gitlab.com/dromi/dromi/sched"
	_ "gitlab.com/dromi/dromi/simt"
)

// Default values
var (
	// DromiScript options
	RandomSeed         int64
	OverrideRndSource  bool  = true
	CorrelateRndSource bool  = false
	SpeedUpHint        bool  = false
	SpeedUpFactor      int64 = 2

	// Simulation options
	QualityAssurance bool = false

	// These kind of things should not be set from the command line
	Replications int64         = 1
	Lazy         bool          = true
	JobOverhead  time.Duration = 1 * time.Millisecond
	Duration     time.Duration = 25 * time.Hour
	Size         int64         = 100
	MaxRetries   int64         = 10
)

// SetFlags sets the relevant DromiScript flags.
func SetFlags(fs *pflag.FlagSet) {
	fs.Int64VarP(&RandomSeed, "seed", "s", RandomSeed, "random seed, if zero unix time is used")
	fs.BoolVar(&OverrideRndSource, "override", OverrideRndSource, "whether to override the random source")
	fs.BoolVar(&CorrelateRndSource, "correlate", CorrelateRndSource, "whether to try to correlate the random sources")
	fs.BoolVarP(&SpeedUpHint, "speedup", "f", SpeedUpHint, "whether to give speed-up simulation hint")
	fs.Int64Var(&SpeedUpFactor, "speedup-factor", SpeedUpFactor, "hint factor by which to speed-up simulation")

	fs.BoolVar(&QualityAssurance, "qa", QualityAssurance, "perform quality assurance on generator, scheduler, and cluster")

	fs.Int64VarP(&Replications, "replications", "r", Replications, "perform each experiment this many times")
	fs.DurationVarP(&Duration, "duration", "d", Duration, "simulation duration")
	fs.Int64VarP(&Size, "size", "z", Size, "default cluster size")
	fs.BoolVarP(&Lazy, "lazy", "l", Lazy, "cluster updates machines as-late-as-possible")
	fs.Int64Var(&MaxRetries, "max-retries", MaxRetries, "maximum scheduling retries before rejection")
	fs.DurationVar(&JobOverhead, "job-overhead", JobOverhead, "job runtime overhead")
}

// NewKeys returns the DromiScript keys.
func NewKeys() key.KeyMap {
	defaultRandSrc := func() rand.Source {
		var rs rand.Source
		if RandomSeed == 0 {
			rs = rand.NewSource(time.Now().Unix())
		} else {
			rs = rand.NewSource(RandomSeed)
		}
		return rs
	}

	var (
		clusterType   = reflect.TypeOf((*dromi.Cluster)(nil)).Elem()
		generatorType = reflect.TypeOf((*dromi.Generator)(nil)).Elem()
		schedulerType = reflect.TypeOf((*dromi.Scheduler)(nil)).Elem()
		simdurType    = reflect.TypeOf(simt.Millisecond)
		randsrcType   = reflect.TypeOf((*rand.Source)(nil)).Elem()
		slavesType    = key.TyperFunc("[]*dromi.Slave", func(v interface{}) (interface{}, error) {
			t := reflect.TypeOf(v)
			if t.Kind() != reflect.Slice {
				return nil, key.NewTypeError("", t, []*dromi.Slave{})
			}

			vv := reflect.ValueOf(v)
			n := vv.Len()
			xs := make([]*dromi.Slave, n)
			slave := reflect.TypeOf((*dromi.Slave)(nil))
			for i := range xs {
				sv := vv.Index(i).Interface()
				if s, ok := sv.(*dromi.Slave); ok {
					xs[i] = s
				} else {
					st := reflect.TypeOf(sv)
					return nil, key.NewTypeError(fmt.Sprintf("SLAVES[%d]", i), st, slave)
				}
			}
			return xs, nil
		})
	)

	var (
		rwq = key.Required | key.ReadWrite
		rq  = key.Required | key.Read
		wq  = key.Required | key.Write
		ro  = key.Read // optional read-only
	)

	keys := key.NewKeyMap()

	key.Must(keys.Create("CORRELATE", key.Bool, CorrelateRndSource, rwq, "try to correlate experiments (ie. use same randsrc for each experiment"))
	key.Must(keys.Create("AUTO_OVERRIDE", key.Bool, OverrideRndSource, rwq, "automatically override randsrc for experiments"))
	key.Must(keys.Create("SPEEDUP_FACTOR", key.Int64, SpeedUpFactor, wq, "speedup factor hint"))
	key.Must(keys.Create("SPEEDUP", key.Bool, SpeedUpHint, wq, "whether speedup is being used"))
	key.Must(keys.Create("SEED", key.Int64, RandomSeed, wq, "random seed"))
	key.Must(keys.Create("RANDSRC", randsrcType, defaultRandSrc(), rwq, "to recreate known situations"))

	key.Must(keys.Create("REPLICATIONS", key.Int64, Replications, rwq, "how often each experiment should be replicated"))
	key.Must(keys.Create("EXPERIMENT", key.Int64, int64(0), wq, "which experiment is currently being run"))
	key.Must(keys.Create("REPL", key.Int64, int64(0), wq, "which replication is currently being run"))
	key.Must(keys.Create("NAME", key.String, nil, ro, "name of the experiment"))
	key.Must(keys.Create("DESC", key.String, nil, ro, "description of the experiment"))
	key.Must(keys.Create("RSCRIPT", key.String, nil, ro, "path to R script for evaluating the experiment"))
	key.Must(keys.Create("RSCRIPT_ANYTIME", key.Bool, false, ro, "allow R script to run before experiment is done"))

	key.Must(keys.Create("QUALITY_ASSURANCE", key.Bool, QualityAssurance, rwq, "quality assurance on generator, scheduler, and cluster"))
	key.Must(keys.Create("CLUST_SIZE", key.Int64, Size, rwq, "pre-defined cluster size"))
	key.Must(keys.Create("CLUST_LAZY", key.Bool, Lazy, rwq, "whether cluster should be updated lazily"))
	key.Must(keys.Create("JOB_OVERHEAD", simdurType, simt.Duration(JobOverhead), rwq, "how long each job runs at least"))
	key.Must(keys.Create("SCHED_MAX_RETRIES", key.Int64, MaxRetries, rwq, "how often to reschedule a job before rejecting"))
	key.Must(keys.Create("SIMULATE_DURATION", simdurType, simt.Duration(Duration), rwq, "how long to run the simulation"))
	key.Must(keys.Create("RESET_TIME", simdurType, 24*simt.Hour, ro, "reset time, used for graphing purposes"))

	key.Must(keys.Create("CLUSTER", clusterType, nil, rq, "machine cluster"))
	key.Must(keys.Create("GENERATOR", generatorType, nil, rq, "generator"))
	key.Must(keys.Create("SCHEDULER", schedulerType, nil, rq, "scheduler"))
	key.Must(keys.Create("SLAVES", slavesType, nil, ro, "slaves"))

	return keys
}

// Simulation contains the result of parsing a DromiScript file.
type Simulation struct {
	// The following two keys are the only ones that MUST be set for a Simulation.
	// The rest can be filled in by calling Parse.

	// Executer can collectively take up a lot of memory. When you are done
	// using it, via parsing, you can set it to nil.
	Executer *twikutil.Executer
	Keys     key.KeyMap

	Path       string
	Dimensions *Dimensions

	Rscript        string
	RscriptAnytime bool
	Name           string
	Desc           string
	Replications   int
	Duration       simt.Duration

	Master     *dromi.Master
	Simulation *dromi.Simulation
	Parameters Point
}

// New creates a new Simulation, optionally parsing the path given.
func New(path string) (*Simulation, error) {
	s := &Simulation{
		Executer:   script.New(),
		Keys:       NewKeys(),
		Dimensions: NewDimensions(),
	}

	// Note: This creates a special variable function, which on first reading
	// will allow setting and reading DIMENSION, and can be replaced by another
	// function which only allows reads. This way all errors can be caught.
	s.Executer.Create("DIMENSION", func(id, name string, list interface{}) (interface{}, error) {
		var xs []interface{}
		switch t := list.(type) {
		case []interface{}:
			xs = t
		case []string:
			xs = make([]interface{}, len(t))
			for i, s := range t {
				xs[i] = s
			}
		default:
			v := reflect.ValueOf(list)
			if v.Kind() != reflect.Slice {
				return nil, errors.New("expecting list or slice")
			}

			xs = make([]interface{}, v.Len())
			for i := range xs {
				xs[i] = v.Index(i).Interface()
			}
		}

		if err := s.Dimensions.Create(id, name, xs); err != nil {
			return nil, err
		}
		return s.Dimensions.Value(id)
	})

	if path != "" {
		err := s.Parse(path)
		return s, err
	}
	return s, nil
}

// Parse a file and fill in the Simulation values.
func (s *Simulation) Parse(path string) error {
	return s.ParseWithHandler(path, errs.Quit)
}

// ErrFailed is returned when DromiScript is incomplete as a configuration.
var ErrFailed = errors.New("failed to acquire required variables")

// ParseWithHandler lets you deterimine how to handle the individual errors
// that occur during checking the DromiScript file.
func (s *Simulation) ParseWithHandler(path string, h errs.Handler) error {
	s.Path = path
	if err := s.Keys.ApplyOrNil(s.Executer); err != nil {
		panic(err)
	}
	if _, err := s.Executer.Exec(path); err != nil {
		return err
	}

	ok := true
	err := s.Keys.Acquire(s.Executer, func(err error) error {
		if err != nil {
			ok = false
		}
		return h(err)
	})
	if err != nil {
		return err
	} else if !ok {
		return ErrFailed
	}

	keys := s.Keys
	g := keys["GENERATOR"].Get().(dromi.Generator)
	sched := keys["SCHEDULER"].Get().(dromi.Scheduler)
	c := keys["CLUSTER"].Get().(dromi.Cluster)

	sim := dromi.NewSimulation(c, g, sched)
	sim.QualityAssurance = keys["QUALITY_ASSURANCE"].Get().(bool)
	sim.LazyCluster = keys["CLUST_LAZY"].Get().(bool)
	sim.MaxSchedRetries = int(keys["SCHED_MAX_RETRIES"].Get().(int64))
	sim.JobOverhead = keys["JOB_OVERHEAD"].Get().(simt.Duration)
	m := dromi.NewMaster(sim)

	sk := keys["SLAVES"]
	if !sk.Empty() {
		v := sk.Get().([]*dromi.Slave)
		m.RegisterSlave(v...)
	}

	s.Master = m
	s.Simulation = sim
	s.Parameters = s.Dimensions.Current()
	s.Replications = int(keys["REPLICATIONS"].Get().(int64))
	s.Name = keys["NAME"].GetOr("unknown").(string)
	s.Desc = keys["DESC"].GetOr("no description").(string)
	s.Duration = keys["SIMULATE_DURATION"].Get().(simt.Duration)
	s.Rscript = keys["RSCRIPT"].GetOr("").(string)
	s.RscriptAnytime = keys["RSCRIPT_ANYTIME"].GetOr(false).(bool)

	// Print a warnings if relevant
	if c.Len() == 0 {
		log.Warning("cluster is empty")
	}
	return nil
}

// RealPath returns the real path of the DromiScript being evaluated.
// This is used for finding the Rscript.
func (s *Simulation) RealPath() (string, error) {
	path, err := filepath.EvalSymlinks(s.Path)
	if err != nil {
		return "", err
	}
	return filepath.Abs(path)
}

// RealRscript finds the path to the Rscript referenced in the DromiScript.
func (s *Simulation) RealRscript() (string, error) {
	if filepath.IsAbs(s.Rscript) {
		return filepath.EvalSymlinks(s.Path)
	}

	path, err := s.RealPath()
	if err != nil {
		return "", err
	}
	dir := filepath.Dir(path)

	path = filepath.Join(dir, s.Rscript)
	return filepath.EvalSymlinks(path)
}

// Run runs a simulation.
func (s *Simulation) Run() time.Duration {
	start := time.Now()
	s.Master.Execute(dromi.ForDuration(s.Duration))
	return time.Now().Sub(start)
}

// Finalize frees memory from this simulation object.
func (s *Simulation) Finalize() error {
	s.Executer = nil
	s.Keys = nil
	s.Dimensions = nil
	s.Simulation = nil
	return s.Master.Finalize()
}

// An Overseer type runs all simulations, keeps a progress ETA,
// and notifies functions when complete.
type Overseer struct {
	Simulations []*Simulation
	notify      []func()

	eta webutil.SyncETA
	ch  chan struct{}
}

// NewOverseer returns a new Overseer using all as the group of Simulations.
func NewOverseer(all []*Simulation) *Overseer {
	return &Overseer{
		Simulations: all,
	}
}

// NotifyFunc adds function f to be called when all simulations are done.
func (o *Overseer) NotifyFunc(f func()) {
	o.notify = append(o.notify, f)
}

// RunAsync runs num simulations at-a-time.
func (o *Overseer) RunAsync(num int) error {
	// Create a pool of workers, so we can control how many simulations
	// can run at once. (See the --parallel flag for manipulating this.)
	n := len(o.Simulations)
	if num <= 0 {
		num = n
	}
	pool, err := tunny.CreatePoolGeneric(num).Open()
	if err != nil {
		return err
	}

	o.eta.Begin()
	o.eta.Add(n)
	for _, s := range o.Simulations {
		// Why so convoluted? Within a loop, closures work in very unexpected ways.
		// See https://github.com/golang/go/wiki/CommonMistakes for more on this.
		pool.SendWorkAsync(func(my *Simulation) func() {
			return func() {
				my.Run()
				o.eta.Done()
				err := my.Finalize() // free memory!
				if err != nil {
					log.Errorln("Error finalizing simulation:", err)
				}
			}
		}(s), nil)
	}

	go func() {
		for _, s := range o.Simulations {
			s.Master.Wait()
		}
		o.eta.End()

		for _, f := range o.notify {
			go f()
		}
	}()

	return nil
}

// Progress returns the ETA.
func (o *Overseer) Progress() webutil.ETA { return o.eta.ETA() }

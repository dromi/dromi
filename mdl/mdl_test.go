// Copyright (c) 2015, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package mdl

import (
	"testing"

	"gitlab.com/dromi/dromi/simt"
)

func TestPodRuntimeWith(z *testing.T) {
	type test struct {
		Runtime  simt.Duration
		Min      float32
		Max      float32
		Extra    float32
		Used     float32
		Expected simt.Duration
	}

	tests := []test{
		{1 * simt.Second, 1.0, 2.0, 0.0, 0.0, 1000 * simt.Millisecond},
		{1 * simt.Second, 1.0, 2.0, 1.0, 1.0, 500 * simt.Millisecond},
		{1 * simt.Second, 1.0, 2.0, 1.5, 1.0, 500 * simt.Millisecond},
		{1 * simt.Second, 1.0, 2.0, 100, 1.0, 500 * simt.Millisecond},
		{1 * simt.Second, 1.0, 2.0, -0.5, -0.5, 2000 * simt.Millisecond},
		{1 * simt.Second, 0.1, 2.0, 1.9, 1.9, 500 * simt.Millisecond},
		// Anything with Runtime 0 should return simt.Max
		{0 * simt.Second, 0.0, 2.0, 1.0, 0, simt.Max},
		{0 * simt.Second, 0.1, -2.0, 41.0, 0, simt.Max},
		{0 * simt.Second, 0.2, -2.0, -1.0, 0, simt.Max},
		{0 * simt.Second, 1.0, 345672.0, -1.0, 0, simt.Max},
		{0 * simt.Second, -1.0, 34.0, 0, 0, simt.Max},
	}

	for _, t := range tests {
		p := Pod{Runtime: t.Runtime, MinCores: t.Min, MaxCores: t.Max}
		d, x := p.RuntimeWith(t.Extra)
		if x != t.Used || d != t.Expected {
			z.Errorf("Got Pod{%v, #%.2f-%.2f}.RuntimeWith(%.2f) = (%v, %.2f), expecting (%v, %.2f)", t.Runtime, t.Min, t.Max, t.Extra, d, x, t.Expected, t.Used)
		}
	}
}

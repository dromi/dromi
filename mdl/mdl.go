// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

// Package mdl provides data structures and types for use
// in cluster-scheduler simulation.
package mdl

import (
	"bytes"
	"fmt"
	"strconv"

	"gitlab.com/dromi/dromi/script"
	"gitlab.com/dromi/dromi/simt"
)

func init() {
	script.CreateFunc("hw", func(cores float64, memory, network, graphics, limit int64) Hardware {
		return Hardware{float32(cores), uint32(memory), uint32(network), uint16(graphics), int16(limit)}
	})

	script.CreateFunc("net.kbps", func(v float64) int64 { return int64(v * float64(Kbps)) })
	script.CreateFunc("net.mbps", func(v float64) int64 { return int64(v * float64(Mbps)) })
	script.CreateFunc("net.gbps", func(v float64) int64 { return int64(v * float64(Gbps)) })
}

// Job {{{1

// Job contains statistics for a pod as well as the pod itself.
//
// The size of a Job is around ten 64-bit words, depending on how
// slices are stored, so we make all method receivers pointers.
type Job struct {
	// The ID is unique across all jobs.
	ID int64

	// Arrival is the time that the pod was submitted to the system.
	Arrival simt.Time
	// Sched is a list of all the scheduling attempts for the pod.  After
	// scheduling, it should have a size of at least 1, the last time is the
	// successful scheduling.
	Sched []simt.Time
	// SchedDur is a list of how long each scheduling attempt took.
	SchedDur []simt.Duration
	// SchedEnd is the end of the last scheduling attempt,
	// which if the job is assigned, is also the assignment time.
	SchedEnd simt.Time
	// Departure is the time that the pod was completed/killed.
	Departure simt.Time

	// Conflicts is the number of times an assignment conflicted with
	// another. A conflict is only counted once (in the later job).
	Conflicts int16

	// Pod contains the pod of a job.
	Pod Pod
}

// ArrivalTime returns the ceration time of the job or when it entered the
// system.  This is also the time that the job is normally initially submitted
// for scheduling to the scheduler.
func (j *Job) ArrivalTime() simt.Time { return j.Arrival }

// NumSchedulingAttempts returns the number of scheduling attempts.  A job may
// be scheduled multiple times, because the first scheduling attempts failed.
// After a scheduling fails, a ajob may be put back into the queue.  The last
// scheduling is either successful or the job is rejected at that point.
func (j *Job) NumSchedulingAttempts() int { return len(j.Sched) }

// SchedulingStarts returns the starting times of schedulings.
func (j *Job) SchedulingStarts() []simt.Time { return j.Sched }

// SchedulingDurations returns the scheduling durations. These entries
// correspond with those from SchedulingStarts.
func (j *Job) SchedulingDurations() []simt.Duration { return j.SchedDur }

// SchedulingEnd returns the time at which the scheduling processed is
// complete. If the job was not rejected, this is the time that it is assigned
// to a machine. If the job is rejected, then it is the same as DepartureTime.
func (j *Job) SchedulingEnd() simt.Time { return j.SchedEnd }

// AssignmentTime returns the assignment time, if it has occurred, otherwise 0.
func (j *Job) AssignmentTime() simt.Time {
	if j.IsAssigned() {
		return j.SchedEnd
	} else {
		return 0
	}
}

// IsAssigned returns whether the job was assigned or not.
func (j *Job) IsAssigned() bool { return j.SchedEnd != 0 && j.SchedEnd != j.Departure }

// NumConflicts returns the number of conflicts encountered while trying to
// schedule this job.
func (j *Job) NumConflicts() int { return int(j.Conflicts) }

// DepartureTime returns the departure time of this job from the system.
func (j *Job) DepartureTime() simt.Time { return j.Departure }

// IsRejected() returns whether the job was rejected (which should not be the
// case if IsAssigned returns true).
func (j *Job) IsRejected() bool { return j.Departure != 0 && j.Departure == j.SchedEnd }

// WaitingDuration is the total time that the job spent in a queue waiting.
// This does not include the time being actively scheduled (even if scheduling
// fails).
func (j *Job) WaitingDuration() simt.Duration {
	return j.AssignmentDuration() - j.SchedulingDuration()
}

func (j *Job) WaitingDurations() []simt.Duration {
	waiting := make([]simt.Duration, len(j.SchedDur))
	for i, d := range j.Sched {
		if i == 0 {
			waiting[0] = simt.Duration(d - j.Arrival)
		} else {
			waiting[i] = simt.Duration(d) - (simt.Duration(j.Sched[i-1]) + j.SchedDur[i-1])
		}
	}
	return waiting
}

// SchedulingDuration returns the sum of all scheduling durations, without
// any waiting time.
func (j *Job) SchedulingDuration() simt.Duration {
	var dur simt.Duration
	for _, d := range j.SchedDur {
		dur += d
	}
	return dur
}

// AssignmentDuration returns the time from arrival to assignment or
// rejection and thus includes any waiting time.
func (j *Job) AssignmentDuration() simt.Duration {
	// NOTE: This definition assumes that jobs cannot be evicted.
	return simt.Duration(j.SchedEnd - j.Arrival)
}

// RunningDuration returns the duration the job spends running in the cluster.
// If zero is returned, the job is not finished scheduling or has been rejected.
// If a negative duration is returned, then the job
func (j *Job) RunningDuration() simt.Duration {
	// NOTE: This definition assumes that jobs cannot be evicted.
	return simt.Duration(j.Departure - j.SchedEnd)
}

// AliveDuration returns the current duration that the job has been in the system.
func (j *Job) AliveDuration() simt.Duration {
	return simt.Duration(j.LatestEventTime() - j.Arrival)
}

func (j *Job) LatestEventTime() simt.Time {
	if j.Departure != 0 {
		return j.Departure
	} else if j.SchedEnd != 0 {
		return j.SchedEnd
	} else {
		return j.Arrival
	}
}

func (j *Job) Marks() uint8 {
	return j.Pod.Marks
}

func (j *Job) String() string {
	var buf bytes.Buffer
	for i, s := range j.Sched {
		buf.WriteString(s.String())
		buf.WriteString("->")
		buf.WriteString((s + simt.Time(j.SchedDur[i])).String())
		buf.WriteString(", ")
	}
	s := buf.String()
	if len(j.Sched) != 0 {
		s = s[:len(s)-2]
	}
	return fmt.Sprintf("Job{%d: *%v ς(%v) ⚡%v: %v}", j.ID, j.Arrival, s, j.Departure, j.Pod)
}

func (j *Job) SchedLastStart() simt.Time {
	return j.Sched[len(j.Sched)-1]
}

func (j *Job) AddSched(start simt.Time, dur simt.Duration) {
	j.Sched = append(j.Sched, start)
	j.SchedDur = append(j.SchedDur, dur)
	j.SchedEnd = start + simt.Time(dur)
}

// Pod {{{1

// Pod represents a grouping of tasks that is scheduled together.
//
// While there is an integer number of installed cores, pods
// can request a fraction of a core, which represents usage percentage
// available: 0.1 cores would be 10% of the running time of a core.
//
// The size of a Pod is five 64-bit words, so we use a pointer receiver
// on all the methods.
//
// Note: The concept of a pod comes from Kubernetes.
//
type Pod struct {
	// Deadline is the time after which the pod can be closed.
	// If Deadline is 0, then there is no deadline.
	Deadline simt.Time `json:"deadline"`
	// Runtime is the (estimated) time required if the pod uses 1 core.
	// If Runtime is 0, then it always ends at simt.Max (effectively never).
	Runtime simt.Duration `json:"runtime"`

	Marks    uint8   `json:"marks"`    // tags for filtering pods/jobs
	Priority int8    `json:"priority"` // priority of pod
	MinCores float32 `json:"minCores"` // required amount of cores
	MaxCores float32 `json:"maxCores"` // max cores we can take advantage of (<0 for infinity)
	Memory   uint32  `json:"memory"`   // Memory is the megabytes of memory
	Network  uint32  `json:"network"`  // Network is the kbps network required
}

func (p *Pod) SetMark(bits ...uint) {
	for _, b := range bits {
		if b > 7 {
			panic("invalid bit set")
		}
		p.Marks |= 1 << b
	}
}

func (p *Pod) String() string {
	return fmt.Sprintf("Pod{-%v <%v $%d #%.1f-%.1f +%dMB @%dKbps}", p.Runtime, p.Deadline, p.Priority, p.MinCores, p.MaxCores, p.Memory, p.Network)
}

func (p *Pod) Header() []string {
	return []string{"deadline", "runtime", "priority", "min_cores", "max_cores", "req_memory", "req_network"}
}

func (p *Pod) Record() []string {
	return []string{
		p.Deadline.String(),
		p.Runtime.String(),
		strconv.FormatInt(int64(p.Priority), 10),
		strconv.FormatFloat(float64(p.MinCores), 'f', -1, 32),
		strconv.FormatFloat(float64(p.MaxCores), 'f', -1, 32),
		strconv.FormatInt(int64(p.Memory), 10),
		strconv.FormatInt(int64(p.Network), 10),
	}
}

// RuntimeWith returns the runtime required if cores are given to the already
// reserved set of cores. The amount of cores used is returned as well.
//
// If cores is negative, you are responsible that it is no less than
// -p.Resources.Cores, otherwise the result is junk.
func (p *Pod) RuntimeWith(cores float32) (simt.Duration, float32) {
	if p.Runtime == 0 {
		return simt.Max, 0
	}

	cores += p.MinCores
	if cores > p.MaxCores {
		cores = p.MaxCores
	}
	dur := simt.Duration(float64(p.Runtime) / float64(cores))
	return dur, cores - p.MinCores
}

// Hardware {{{1

// Hardware represents the hardware a machine has available.
//
// Because this type may be duplicated often, it is important to keep the
// size down.
//
//  uint32 can represent up to 4294967296, or 4096 terabyte at a megabyte resolution.
//  uint16 can represent up to 65536, or 64 gigabyte at a megabyte resolution.
//  int16 can represent up to 32767.
//
// Currently, it uses 16 bytes memory, which is two 64-bit words.
type Hardware struct {
	// Cores is the number of CPU cores available.
	Cores float32 `json:"cores"`
	// RAM is the number of megabytes memory available.
	Memory uint32 `json:"memory"`
	// Network is the number of bits per second network transfer speed available.
	Network uint32 `json:"network"`
	// Graphics is the number of megabytes graphics memory available
	// Can be 0 if there is no graphics card in the system.
	Graphics uint16 `json:"graphics"`
	// Limit is a cap to the number of pods that can be run.
	// If Limit is negative, then there is no limit to the number of pods that
	// can be run.
	Limit int16 `json:"limit"`
}

// String returns the hardware as a string in the form
//
//            cores memory  network  graphics limit
//  Hardware (#3.0  +4500MB @100Kbps %1000MB  <500)
func (hw Hardware) String() string {
	return fmt.Sprintf("Hardware{#%.1f +%dMB @%dKbps %%%dMB ^%d}", hw.Cores, hw.Memory, hw.Network, hw.Graphics, hw.Limit)
}

func (hw Hardware) Header() []string {
	return []string{"cores", "memory", "network", "graphics", "limit"}
}

func (hw Hardware) Record() []string {
	return []string{
		strconv.FormatFloat(float64(hw.Cores), 'f', -1, 32),
		strconv.FormatInt(int64(hw.Memory), 10),
		strconv.FormatInt(int64(hw.Network), 10),
		strconv.FormatInt(int64(hw.Graphics), 10),
		strconv.FormatInt(int64(hw.Limit), 10),
	}
}

// Supports returns true if hw has more resources than r.
func (hw Hardware) Supports(p *Pod) bool {
	return hw.Cores >= p.MinCores && hw.Memory >= p.Memory && hw.Network >= p.Network
}

// Reduce reduces the number of resources in hw.
// Warning: No checks for validity are made.
func (hw *Hardware) Reduce(p *Pod) {
	hw.Cores -= p.MinCores
	hw.Memory -= p.Memory
	hw.Network -= p.Network
}

// Increase increases the number of resources in hw.
// Warning: No checks for validity are made.
func (hw *Hardware) Increase(p *Pod) {
	hw.Cores += p.MinCores
	hw.Memory += p.Memory
	hw.Network += p.Network
}

// HardwareUtilization {{{2

type HardwareUtilization struct {
	Cores    float64 `json:"cores"`
	Memory   float64 `json:"memory"`
	Network  float64 `json:"network"`
	Graphics float64 `json:"graphics"`
	Limit    float64 `json:"limit"`
}

func Fraction(base, avail *Hardware) *HardwareUtilization {
	return &HardwareUtilization{
		Cores:    1 - float64(avail.Cores)/float64(base.Cores),
		Memory:   1 - float64(avail.Memory)/float64(base.Memory),
		Network:  1 - float64(avail.Network)/float64(base.Network),
		Graphics: 1 - float64(avail.Graphics)/float64(base.Graphics),
		Limit:    1 - float64(avail.Limit)/float64(base.Limit),
	}
}

// Constants {{{1

// Network speed units.
const (
	Kbps = 1
	Mbps = 1024
	Gbps = 1024 * Mbps
)

// Speeds of various network devices.
const (
	Modem        = 56.7 * Kbps
	Bluetooh     = 723 * Kbps
	Ethernet10M  = 10 * Mbps
	Ethernet100M = 100 * Mbps
	Ethernet1G   = 1 * Gbps
	Ethernet10G  = 10 * Gbps
	WifiB        = 11 * Mbps
	WifiG        = 55 * Mbps
	WifiN        = 300 * Mbps
	GPRS         = 114 * Kbps
	EDGE         = 384 * Kbps
	UMTS         = 3.5 * Mbps
	OC256        = 13.27104 * Gbps
	OC384        = 19.90656 * Gbps
	OC768        = 39.81312 * Gbps
	OC1536       = 79.62624 * Gbps
	OC3072       = 159.25248 * Gbps
)

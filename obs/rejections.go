// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package obs

import (
	"gitlab.com/dromi/dromi"
	"gitlab.com/dromi/dromi/event"
	"gitlab.com/dromi/dromi/mdl"
	"gitlab.com/dromi/dromi/script"
	"gitlab.com/dromi/dromi/simt"
)

func init() {
	script.CreateFunc("obs.rejected-pods", Rejections)
}

var RejectionsEndpoint = "/rejections"

// Rejections should not be used by a Plotter or whatever saves the result
// of Serve.
func Rejections() *dromi.Slave {
	rejections := make([]rejection, 0)
	return &dromi.Slave{
		Name:     "rejections observer",
		Endpoint: RejectionsEndpoint,
		Serve:    func() interface{} { return rejections },
		Trigger:  event.Rejection,
		Observe: func(e *event.Event) {
			rejections = append(rejections, rejection{e.Time, &e.Job.Pod})
		},
	}
}

type rejection struct {
	Time simt.Time `json:"time"`
	Pod  *mdl.Pod  `json:"pod"`
}

func (r *rejection) Header() []string {
	ph := r.Pod.Header()
	h := make([]string, 1, len(ph)+1)
	h[0] = "time"
	h = append(h, ph...)
	return h
}

func (r *rejection) Record() []string {
	pr := r.Pod.Record()
	rr := make([]string, 1, len(pr)+1)
	rr[0] = r.Time.String()
	rr = append(rr, pr...)
	return rr
}

// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package obs

import (
	"bytes"
	"errors"
	"fmt"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/dromi/dromi"
	"gitlab.com/dromi/dromi/event"
	"gitlab.com/dromi/dromi/script"
)

func init() {
	script.CreateFunc("obs.sim-counters", Counters)
	script.CreateFunc("obs.sim-event-queue", Queue)
}

// Counters incl. counters {{{1

func Counters() *dromi.Slave {
	var sim *dromi.Simulation
	return &dromi.Slave{
		Initialize: func(s *dromi.Simulation) error {
			sim = s
			return nil
		},
		Finalize: func(self *dromi.Slave) error {
			cx := sim.Counters()
			self.Serve = func() interface{} {
				return newCounters(cx)
			}
			sim = nil
			return nil
		},
		Endpoint: "/counters",
		Serve: func() interface{} {
			return newCounters(sim.Counters())
		},
	}
}

func newCounters(cx *dromi.Counters) *counters {
	return &counters{
		cx.Time.String(),
		cx.Arrivals, cx.Departures, cx.Evictions, cx.Submissions, cx.Rejections,
		cx.SchedulingFailures, cx.SchedulingConflicts, cx.SchedulingErrors,
		cx.Assignments, cx.AssignmentErrors,
		cx.Observations, cx.Errors, cx.Events,
	}
}

// counters is basically a copy of dromi.Counters.
type counters struct {
	Time                string `json:"time"`
	Arrivals            int64  `json:"arrivals"`
	Departures          int64  `json:"departures"`
	Evictions           int64  `json:"evictions"`
	Submissions         int64  `json:"submissions"`
	Rejections          int64  `json:"rejections"`
	SchedulingFailures  int64  `json:"sheduling_failures"`
	SchedulingConflicts int64  `json:"scheduling_conflicts"`
	SchedulingErrors    int64  `json:"scheduling_errors"`
	Assignments         int64  `json:"assignments"`
	AssignmentErrors    int64  `json:"assignment_errors"`
	Observations        int64  `json:"observations"`
	Errors              int64  `json:"errors"`
	Events              int64  `json:"events"`
}

func (c counters) Header() []string {
	return []string{
		"time",
		"arrivals", "departures", "evictions", "submissions", "rejections",
		"scheduling_failures", "scheduling_conflicts", "scheduling_errors",
		"assignments", "assignment_errors",
		"observations", "errors", "events",
	}
}

func (c counters) Record() []string {
	return []string{
		c.Time,
		strconv.FormatInt(c.Arrivals, 10),
		strconv.FormatInt(c.Departures, 10),
		strconv.FormatInt(c.Evictions, 10),
		strconv.FormatInt(c.Submissions, 10),
		strconv.FormatInt(c.Rejections, 10),
		strconv.FormatInt(c.SchedulingFailures, 10),
		strconv.FormatInt(c.SchedulingConflicts, 10),
		strconv.FormatInt(c.SchedulingErrors, 10),
		strconv.FormatInt(c.Assignments, 10),
		strconv.FormatInt(c.AssignmentErrors, 10),
		strconv.FormatInt(c.Observations, 10),
		strconv.FormatInt(c.Errors, 10),
		strconv.FormatInt(c.Events, 10),
	}
}

// Queue incl. typeCounts and typeCount {{{1

// Queue observes the number of different types in the simulation event queue.
// Although normally, the event queue should be pretty empty.
func Queue() *dromi.Slave {
	var sim *dromi.Simulation
	return &dromi.Slave{
		Initialize: func(s *dromi.Simulation) error {
			sim = s
			return nil
		},
		Finalize: func(self *dromi.Slave) error {
			qx := sim.Queue().Count()
			self.Serve = func() interface{} {
				return newTypeCounts(qx)
			}
			sim = nil
			return nil
		},
		Endpoint: "/queue",
		Serve: func() interface{} {
			return newTypeCounts(sim.Queue().Count())
		},
	}
}

type typeCount struct {
	Type  event.Type
	Count int
}

func (t typeCount) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf(`{"type":%q,"count":%d}`, t.Type, t.Count)), nil
}

func (t typeCount) Header() []string { return []string{"type", "count"} }
func (t typeCount) Record() []string {
	return []string{t.Type.String(), strconv.FormatInt(int64(t.Count), 10)}
}

func newTypeCounts(qx map[event.Type]int) typeCounts {
	ts := make(typeCounts, 0, len(qx))
	for k, v := range qx {
		ts = append(ts, typeCount{k, v})
	}
	sort.Sort(ts)
	return ts
}

type typeCounts []typeCount

func (ts typeCounts) MarshalCSV() ([]byte, error) {
	var buf bytes.Buffer
	if len(ts) == 0 {
		return nil, errors.New("no data")
	}

	buf.WriteString(strings.Join(ts[0].Header(), ","))
	buf.WriteRune('\n')
	for _, x := range ts {
		buf.WriteString(strings.Join(x.Record(), ","))
		buf.WriteRune('\n')
	}
	return buf.Bytes(), nil
}

func (ts typeCounts) Len() int           { return len(ts) }
func (ts typeCounts) Less(i, j int) bool { return ts[i].Type.String() < ts[j].Type.String() }
func (ts typeCounts) Swap(i, j int)      { ts[i], ts[j] = ts[j], ts[i] }

// Copyright (c) 2015, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package obs

import (
	"strconv"

	"github.com/goulash/stat"
	"gitlab.com/dromi/dromi"
	"gitlab.com/dromi/dromi/event"
	"gitlab.com/dromi/dromi/internal"
	"gitlab.com/dromi/dromi/internal/json"
	"gitlab.com/dromi/dromi/mdl"
	"gitlab.com/dromi/dromi/script"
	"gitlab.com/dromi/dromi/simt"
)

func init() {
	script.CreateFunc("obs.machine-stats", MachineStat)
	script.CreateFunc("obs.cluster-stats", ClusterStat)
}

// ClusterStat {{{1

var ClusterStatEndpoint = "/cstat"

// ClusterStat returns the utilization of the entire cluster.
// It averages over MachineStat's results then.
//
// TODO: There has GOT to be a better way to do this...
func ClusterStat() *dromi.Slave {
	mu := MachineStat()
	return &dromi.Slave{
		Name:    "cluster utilization stats",
		Private: mu,
		// We don't need Initialize or Finalize methods because Private is initialized separately.
		Endpoint: ClusterStatEndpoint,
		Serve: func() interface{} {
			v := mu.Serve().([]*MachineStatistics)
			mkSeries := func(f func(*MachineStatistics) float64) stat.Series {
				s := make([]float64, len(v))
				for i, m := range v {
					s[i] = f(m)
				}
				return stat.Series(s)
			}
			var (
				any      = mkSeries(func(m *MachineStatistics) float64 { return m.Any })
				jobs     = mkSeries(func(m *MachineStatistics) float64 { return m.Jobs })
				cores    = mkSeries(func(m *MachineStatistics) float64 { return m.Cores })
				memory   = mkSeries(func(m *MachineStatistics) float64 { return m.Memory })
				network  = mkSeries(func(m *MachineStatistics) float64 { return m.Network })
				graphics = mkSeries(func(m *MachineStatistics) float64 { return m.Graphics })
				limit    = mkSeries(func(m *MachineStatistics) float64 { return m.Limit })
			)
			return &clusterStat{
				simt.Time(v[0].Last),

				json.Float64(any.Mean()),
				json.Float64(any.Median()),
				json.Float64(any.Min()),
				json.Float64(any.Max()),
				json.Float64(any.Var()),

				json.Float64(jobs.Mean()),
				json.Float64(jobs.Median()),
				json.Float64(jobs.Min()),
				json.Float64(jobs.Max()),
				json.Float64(jobs.Var()),

				json.Float64(cores.Mean()),
				json.Float64(cores.Median()),
				json.Float64(cores.Min()),
				json.Float64(cores.Max()),
				json.Float64(cores.Var()),

				json.Float64(memory.Mean()),
				json.Float64(memory.Median()),
				json.Float64(memory.Min()),
				json.Float64(memory.Max()),
				json.Float64(memory.Var()),

				json.Float64(network.Mean()),
				json.Float64(network.Median()),
				json.Float64(network.Min()),
				json.Float64(network.Max()),
				json.Float64(network.Var()),

				json.Float64(graphics.Mean()),
				json.Float64(graphics.Median()),
				json.Float64(graphics.Min()),
				json.Float64(graphics.Max()),
				json.Float64(graphics.Var()),

				json.Float64(limit.Mean()),
				json.Float64(limit.Median()),
				json.Float64(limit.Min()),
				json.Float64(limit.Max()),
				json.Float64(limit.Var()),
			}
		},
		Reset: mu.Reset,
	}
}

type clusterStat struct {
	Time simt.Time `json:"time"`

	AnyMean   json.Float64 `json:"any_mean"`
	AnyMedian json.Float64 `json:"any_median"`
	AnyMin    json.Float64 `json:"any_min"`
	AnyMax    json.Float64 `json:"any_max"`
	AnyVar    json.Float64 `json:"any_var"`

	JobsMean   json.Float64 `json:"jobs_mean"`
	JobsMedian json.Float64 `json:"jobs_median"`
	JobsMin    json.Float64 `json:"jobs_min"`
	JobsMax    json.Float64 `json:"jobs_max"`
	JobsVar    json.Float64 `json:"jobs_var"`

	CoresMean   json.Float64 `json:"cores_mean"`
	CoresMedian json.Float64 `json:"cores_median"`
	CoresMin    json.Float64 `json:"cores_min"`
	CoresMax    json.Float64 `json:"cores_max"`
	CoresVar    json.Float64 `json:"cores_var"`

	MemoryMean   json.Float64 `json:"memory_mean"`
	MemoryMedian json.Float64 `json:"memory_median"`
	MemoryMin    json.Float64 `json:"memory_min"`
	MemoryMax    json.Float64 `json:"memory_max"`
	MemoryVar    json.Float64 `json:"memory_var"`

	NetworkMean   json.Float64 `json:"network_mean"`
	NetworkMedian json.Float64 `json:"network_median"`
	NetworkMin    json.Float64 `json:"network_min"`
	NetworkMax    json.Float64 `json:"network_max"`
	NetworkVar    json.Float64 `json:"network_var"`

	GraphicsMean   json.Float64 `json:"graphics_mean"`
	GraphicsMedian json.Float64 `json:"graphics_median"`
	GraphicsMin    json.Float64 `json:"graphics_min"`
	GraphicsMax    json.Float64 `json:"graphics_max"`
	GraphicsVar    json.Float64 `json:"graphics_var"`

	LimitMean   json.Float64 `json:"limit_mean"`
	LimitMedian json.Float64 `json:"limit_median"`
	LimitMin    json.Float64 `json:"limit_min"`
	LimitMax    json.Float64 `json:"limit_max"`
	LimitVar    json.Float64 `json:"limit_var"`
}

func (cs *clusterStat) Header() []string {
	return []string{"time",
		"any_mean", "any_median", "any_min", "any_max", "any_var",
		"jobs_mean", "jobs_median", "jobs_min", "jobs_max", "jobs_var",
		"cores_mean", "cores_median", "cores_min", "cores_max", "cores_var",
		"memory_mean", "memory_median", "memory_min", "memory_max", "memory_var",
		"network_mean", "network_median", "network_min", "network_max", "network_var",
		"graphics_mean", "graphics_median", "graphics_min", "graphics_max", "graphics_var",
		"limit_mean", "limit_median", "limit_min", "limit_max", "limit_var",
	}
}

func (cs *clusterStat) Record() []string {
	ft := func(f json.Float64) string { return f.String() }
	return []string{
		cs.Time.String(),
		ft(cs.AnyMean), ft(cs.AnyMedian), ft(cs.AnyMin), ft(cs.AnyMax), ft(cs.AnyVar),
		ft(cs.JobsMean), ft(cs.JobsMedian), ft(cs.JobsMin), ft(cs.JobsMax), ft(cs.JobsVar),
		ft(cs.CoresMean), ft(cs.CoresMedian), ft(cs.CoresMin), ft(cs.CoresMax), ft(cs.CoresVar),
		ft(cs.MemoryMean), ft(cs.MemoryMedian), ft(cs.MemoryMin), ft(cs.MemoryMax), ft(cs.MemoryVar),
		ft(cs.NetworkMean), ft(cs.NetworkMedian), ft(cs.NetworkMin), ft(cs.NetworkMax), ft(cs.NetworkVar),
		ft(cs.GraphicsMean), ft(cs.GraphicsMedian), ft(cs.GraphicsMin), ft(cs.GraphicsMax), ft(cs.GraphicsVar),
		ft(cs.LimitMean), ft(cs.LimitMedian), ft(cs.LimitMin), ft(cs.LimitMax), ft(cs.LimitVar),
	}
}

// MachineStat {{{1

var MachineStatEndpoint = "/mstat"

// MachineStat observes the utilization of all machines in the cluster.
func MachineStat() *dromi.Slave {
	var (
		sim  *dromi.Simulation
		mobs []*MachineStatistics
	)
	return &dromi.Slave{
		Name: "machine utilization stats",
		Initialize: func(s *dromi.Simulation) error {
			sim = s
			mobs = make([]*MachineStatistics, s.Cluster.Len())
			s.Cluster.Iterate(func(i int, m dromi.Machine) error {
				o := NewMachineStatistics(m)
				m.RegisterObserver(o)
				mobs[i] = o
				return nil
			})
			return nil
		},
		Finalize: func(self *dromi.Slave) error {
			t := sim.Counters().Time
			for _, o := range mobs {
				o.Update(t)
				o.Finalize()
			}
			self.Serve = func() interface{} {
				return mobs
			}
			sim = nil
			return nil
		},
		Endpoint: MachineStatEndpoint,
		Serve: func() interface{} {
			t := sim.Counters().Time
			for _, o := range mobs {
				o.Update(t)
			}
			return mobs
		},
		Reset: func(t simt.Time) {
			for _, o := range mobs {
				o.Reset(t)
			}
		},
	}
}

// MachineStatistics {{{1

// MachineStatistics is the all-new encompassing utilization statistics
// gatherer for machines.
//
// NOTE: This type is not meant to be used directly.
type MachineStatistics struct {
	m     dromi.Machine
	midx  int
	av    mdl.Hardware
	Start float64
	Last  float64

	// Any stores whether the hardware is in use through any job.  This cannot
	// be determined by looking at base and available Hardware, because of
	// rounding errors (after adding, subtracting, multiplying, dividing,
	// Hardware values may not return to zero). It must be made by knowing how
	// many jobs are really running
	Any  float64
	Jobs float64

	// Hardware usage
	Cores    float64
	Memory   float64
	Network  float64
	Graphics float64
	Limit    float64
}

func NewMachineStatistics(m dromi.Machine) *MachineStatistics {
	o := &MachineStatistics{m: m}
	o.midx = m.Index()
	o.Reset(m.Time())
	return o
}

// Finalize removes references to the machine being tracked,
// as it is assumed that any modifications to this statistic
// will no longer be used: Reset, Remove, Update, Insert.
// The only functions that can be called at this point then
// are: Header, MachineID, MarshalJSON, Record.
func (o *MachineStatistics) Finalize() error {
	o.m = nil
	return nil
}

func (o *MachineStatistics) MarshalJSON() ([]byte, error) {
	var buf json.Buffer
	buf.Write(json.Object{
		{"id", o.midx},
		{"time", simt.Time(o.Last)},
		{"any", o.Any},
		{"jobs", o.Jobs},
		{"cores", o.Cores},
		{"memory", o.Memory},
		{"graphics", o.Graphics},
		{"limit", o.Limit},
	})
	return buf.Bytes(), nil
}

func (o *MachineStatistics) Header() []string {
	return []string{"id", "time", "any", "jobs", "cores", "memory", "network", "graphics", "limit"}
}

func (o *MachineStatistics) Record() []string {
	format := func(f float64) string { return strconv.FormatFloat(f, 'f', -1, 64) }
	return []string{
		strconv.FormatInt(int64(o.midx), 10),
		simt.Time(o.Last).String(),
		format(o.Any),
		format(o.Jobs),
		format(o.Cores),
		format(o.Memory),
		format(o.Network),
		format(o.Graphics),
		format(o.Limit),
	}
}

func (o *MachineStatistics) MachineID() int { return o.midx }

func (o *MachineStatistics) Reset(t simt.Time) {
	o.m.Update(t)
	o.av = *o.m.Available()
	tf := float64(t)

	o.Start, o.Last = tf, tf
	u, n := mdl.Fraction(o.m.Hardware(), &o.av), o.m.NJobs()

	if n == 0 {
		o.Any = 0.0
	} else {
		o.Any = 1.0
	}
	o.Jobs = float64(n)
	o.Cores = u.Cores
	o.Memory = u.Memory
	o.Network = u.Network
	o.Graphics = u.Graphics
	o.Limit = u.Limit
}

func (o *MachineStatistics) Update(t simt.Time) {
	internal.Assert(o.Last <= float64(t))
	tf := float64(t)
	o.m.Update(t)
	o.av = *o.m.Available()
	o.update(tf, o.m.NJobs())
	o.Last = tf
}

// Insert is called BEFORE an event is put into the machine,
// resources have not been increased on the machine.
func (o *MachineStatistics) Insert(e *event.Event) {
	tf := float64(e.Time) // should be e.Job.SchedEnd
	o.av = *o.m.Available()
	o.update(tf, o.m.NJobs())
	o.Last = tf
}

// Remove is called AFTER an event is returned,
// resources have already been reduced on the machine.
func (o *MachineStatistics) Remove(e *event.Event) {
	// We want to store what the utilization state was
	//      up until this point
	// so we don't update o.av and we compensate for the fact
	// that there is now one less job than there was.
	tf := float64(e.Time) // should be the same as e.Job.Departure
	o.update(tf, o.m.NJobs()+1)
	o.av = *o.m.Available()
	o.Last = tf
}

func (o *MachineStatistics) update(tf float64, n int) {
	u := mdl.Fraction(o.m.Hardware(), &o.av)
	prev, cur, total := (o.Last - o.Start), (tf - o.Last), (tf - o.Start)

	if n == 0 {
		o.Any = o.Any * prev / total
	} else {
		o.Any = (o.Any*prev + cur) / total
	}
	o.Jobs = (o.Jobs*prev + float64(n)*cur) / total
	o.Cores = (o.Cores*prev + u.Cores*cur) / total
	o.Memory = (o.Memory*prev + u.Memory*cur) / total
	o.Network = (o.Network*prev + u.Network*cur) / total
	o.Graphics = (o.Graphics*prev + u.Graphics*cur) / total
	o.Limit = (o.Limit*prev + u.Limit*cur) / total
}

var _ = dromi.MachineObserver(new(MachineStatistics))

// }}}

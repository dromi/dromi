// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package obs

import (
	"strconv"

	"github.com/goulash/csv"
	"gitlab.com/dromi/dromi"
	"gitlab.com/dromi/dromi/event"
	"gitlab.com/dromi/dromi/obs/accum"
	"gitlab.com/dromi/dromi/script"
	"gitlab.com/dromi/dromi/simt"
)

// WARNING: None of this really works, nor does it really make sense. You
// simply cannot always have all of the these values like this.
//
// I do not recommend using this observer!

func init() {
	script.CreateFunc("obs.schedq.depr", SchedulerQueue)
}

var SchedulerQueueEndpoint = "/schedq"

func SchedulerQueue() *dromi.Slave {
	var sq schedulerQueue
	return &dromi.Slave{
		Endpoint: SchedulerQueueEndpoint,
		Serve:    func() interface{} { return sq.Copy() },
		Reset:    sq.Reset,
		Trigger:  event.Submission | event.Assignment | event.SchedulingFailure,
		Observe:  sq.Observe,
	}
}

type schedulerQueue struct {
	Time simt.Time `json:"time"`

	// Length is not the queue length, but rather the number of jobs being
	// processed OR queued by the scheduler.
	length int
	Length accum.Run `json:"queue_length"`

	lastSubmitted simt.Time
	Submitted     int           `json:"submitted"`
	SubmittedIAT  accum.TimeRun `json:"submitted_iat"`

	lastAssigned simt.Time
	Assigned     int           `json:"assigned"`
	AssignedIAT  accum.TimeRun `json:"assigned_iat"`

	lastConflict simt.Time
	Conflict     int           `json:"conflict"`
	ConflictIAT  accum.TimeRun `json:"conflict_iat"`

	lastFailed simt.Time
	Failed     int           `json:"failed"`
	FailedIAT  accum.TimeRun `json:"failed_iat"`
}

// We can do this because run and accum.TimeRun can be shallow-copied.
func (sq schedulerQueue) Copy() *schedulerQueue { return &sq }

func (sq *schedulerQueue) Reset(t simt.Time) {
	sq.Time = t
	sq.Length = accum.Run{}

	sq.lastSubmitted = t
	sq.Submitted = 0
	sq.SubmittedIAT = accum.TimeRun{}

	sq.lastAssigned = t
	sq.Assigned = 0
	sq.AssignedIAT = accum.TimeRun{}

	sq.lastConflict = t
	sq.Conflict = 0
	sq.ConflictIAT = accum.TimeRun{}

	sq.lastFailed = t
	sq.Failed = 0
	sq.FailedIAT = accum.TimeRun{}
}

func (sq *schedulerQueue) Observe(e *event.Event) {
	sq.Length.AddN(int64(e.Time-sq.Time), float64(sq.length))
	sq.Time = e.Time

	switch e.Type {
	case event.Arrival, event.Submission:
		sq.Submitted++
		sq.SubmittedIAT.Add(float64(e.Time - sq.lastSubmitted))
		sq.lastSubmitted = e.Time
		sq.length++
	case event.Assignment:
		sq.Assigned++
		sq.AssignedIAT.Add(float64(e.Time - sq.lastAssigned))
		sq.lastAssigned = e.Time
		sq.length--
	case event.SchedulingConflict:
		sq.Conflict++
		sq.ConflictIAT.Add(float64(e.Time - sq.lastConflict))
		sq.lastConflict = e.Time
		fallthrough // because a conflict is a failure
	case event.SchedulingFailure, event.SchedulingError:
		sq.Failed++
		sq.FailedIAT.Add(float64(e.Time - sq.lastFailed))
		sq.lastFailed = e.Time
		sq.length--
	default:
		panic("not expecting this event: " + e.String())
	}
}

func (sq *schedulerQueue) Header() []string {
	return []string{
		"time",

		"queue_length_mean",
		"queue_length_var",
		"queue_length_std",
		"queue_length_min",
		"queue_length_max",

		"submitted",
		"submitted_iat_mean",
		"submitted_iat_std",
		"submitted_iat_min",
		"submitted_iat_max",

		"assigned",
		"assigned_iat_mean",
		"assigned_iat_std",
		"assigned_iat_min",
		"assigned_iat_max",

		"conflict",
		"conflict_iat_mean",
		"conflict_iat_std",
		"conflict_iat_min",
		"conflict_iat_max",

		"failed",
		"failed_iat_mean",
		"failed_iat_std",
		"failed_iat_min",
		"failed_iat_max",
	}
}

func (sq *schedulerQueue) Record() []string {
	strc := func(i int) string { return strconv.FormatInt(int64(i), 10) }
	nums := []string{
		sq.Time.String(),
		strc(sq.Submitted),
		strc(sq.Assigned),
		strc(sq.Conflict),
		strc(sq.Failed),
	}
	rs := []csv.Recorder{
		&sq.Length,
		&sq.SubmittedIAT,
		&sq.AssignedIAT,
		&sq.ConflictIAT,
		&sq.FailedIAT,
	}
	records := make([]string, 0, len(rs)*5+1)
	for i, r := range rs {
		records = append(records, nums[i])
		records = append(records, r.Record()...)
	}
	return records
}

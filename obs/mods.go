// Copyright (c) 2015, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package obs

import (
	"gitlab.com/dromi/dromi"
	"gitlab.com/dromi/dromi/event"
	"gitlab.com/dromi/dromi/script"
	"gitlab.com/dromi/dromi/simt"
)

func init() {
	script.CreateFunc("obs|ep", modEndpoint)
	script.CreateFunc("obs|exempt", setExempt)
	script.CreateFunc("obs|plotter", Plotter)
	script.CreateFunc("obs|plottert", PlotterT)
	script.CreateFunc("obs|plotterpt", PlotterPT)
	script.CreateFunc("obs|refresher", Refresher)
}

func modEndpoint(ep string, s *dromi.Slave) *dromi.Slave {
	s.Endpoint = ep
	return s
}

func setExempt(s *dromi.Slave) *dromi.Slave {
	s.Reset = nil
	return s
}

var PlotterEndpoint = ".plot"

func Plotter(p simt.Process, s *dromi.Slave) *dromi.Slave {
	values := make([]interface{}, 0)
	return &dromi.Slave{
		// Need to let s get registered.
		Private: s,

		Endpoint: s.Endpoint + PlotterEndpoint,
		Serve:    func() interface{} { return values },

		Trigger: event.Observation,
		Process: p,
		Observe: func(e *event.Event) {
			values = append(values, s.Serve())
		},
		Reset: func(t simt.Time) {
			// TODO: See if not appending the values leads to better results
			//values = append(values, s.Serve())
			if s.Reset != nil {
				s.Reset(t)
			}
		},
	}
}

func PlotterT(t event.Type, s *dromi.Slave) *dromi.Slave {
	p := Plotter(nil, s)
	p.Trigger = t
	return p
}

func PlotterPT(p simt.Process, t event.Type, s *dromi.Slave) *dromi.Slave {
	plotter := Plotter(p, s)
	plotter.Trigger |= t
	return plotter
}

var RefresherEndpoint = ".re"

// Refresher resets s when it is accessed via Serve. It does this by
// modifying the slave.
func Refresher(s *dromi.Slave) *dromi.Slave {
	if s.Reset == nil || s.Serve == nil {
		return s
	}

	var sim *dromi.Simulation
	s.AddInitializeFn(func(ds *dromi.Simulation) error { sim = ds; return nil })

	serve := s.Serve
	s.Serve = func() interface{} {
		v := serve()
		s.Reset(sim.Counters().Time)
		return v
	}

	if s.Endpoint == "" {
		return s
	}

	s.Endpoint = s.Endpoint + RefresherEndpoint
	return s
}

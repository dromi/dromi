// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package accum

import (
	"strconv"

	"github.com/goulash/stat"
	"gitlab.com/dromi/dromi/internal/json"
)

// Run wraps stat.Run so that we can add methods to it.
type Run struct {
	stat.Run
}

func (r Run) MarshalJSON() ([]byte, error) {
	var buf json.Buffer
	buf.StartObject()
	buf.WriteKey("mean").WriteFloat64(r.Mean())
	buf.WriteKey("var").WriteFloat64(r.Var())
	buf.WriteKey("std").WriteFloat64(r.Std())
	buf.WriteKey("min").WriteFloat64(r.Min())
	buf.WriteKey("max").WriteFloat64(r.Max())
	buf.EndObject()
	return buf.Bytes(), nil
}

func (r Run) Header() []string {
	return []string{"mean", "var", "std", "min", "max"}
}

func (r Run) Record() []string {
	return []string{
		strconv.FormatFloat(r.Mean(), 'f', -1, 64),
		strconv.FormatFloat(r.Var(), 'f', -1, 64),
		strconv.FormatFloat(r.Std(), 'f', -1, 64),
		strconv.FormatFloat(r.Min(), 'f', -1, 64),
		strconv.FormatFloat(r.Max(), 'f', -1, 64),
	}
}

// TimeRun wraps stat.Run so that we can add methods to it.
type TimeRun struct {
	stat.Run
}

func (r TimeRun) MarshalJSON() ([]byte, error) {
	var buf json.Buffer
	buf.StartObject()
	buf.WriteKey("mean").WriteFloat64AsDuration(r.Mean())
	buf.WriteKey("std").WriteFloat64AsDuration(r.Std())
	buf.WriteKey("min").WriteFloat64AsDuration(r.Min())
	buf.WriteKey("max").WriteFloat64AsDuration(r.Max())
	buf.EndObject()
	return buf.Bytes(), nil
}

func (r TimeRun) Header() []string {
	return []string{"mean", "std", "min", "max"}
}

func (r TimeRun) Record() []string {
	return []string{
		json.Duration(r.Mean()).String(),
		json.Duration(r.Std()).String(),
		json.Duration(r.Min()).String(),
		json.Duration(r.Max()).String(),
	}
}

// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package accum

import (
	"bytes"
	"fmt"
	"math"
	"strconv"

	"github.com/cassava/hdrhistogram"
	"gitlab.com/dromi/dromi/internal/json"
	"gitlab.com/dromi/dromi/simt"
)

var (
	quantileSanitized = []string{
		"0_0", "0_1", "0_2", "0_3", "0_4", "0_5", "0_6", "0_7", "0_75", "0_8", "0_85",
		"0_9", "0_925", "0_95", "0_975", "0_99", "0_995", "0_999", "0_9999", "0_99999", "1_0",
	}
	quantileStrings = []string{
		"0.0", "0.1", "0.2", "0.3", "0.4", "0.5", "0.6", "0.7", "0.75", "0.8", "0.85",
		"0.9", "0.925", "0.95", "0.975", "0.99", "0.995", "0.999", "0.9999", "0.99999", "1.0",
	}
	quantiles = []float64{0, 10, 20, 30, 40, 50, 60, 70, 75, 80, 85, 90, 92.5, 95, 97.5, 99, 99.5, 99.9, 99.99, 99.999, 100}
)

type Histogram struct {
	hdrhistogram.Histogram
	PanicOnOutOfRange bool
	OutOfRange        int64
}

func NewHistogram(min, max int64) *Histogram {
	return &Histogram{
		Histogram: *hdrhistogram.New(min, max, 2),
	}
}

func (h *Histogram) Copy() *Histogram {
	s := h.Histogram.Export()
	return &Histogram{
		Histogram:         *hdrhistogram.Import(s),
		PanicOnOutOfRange: h.PanicOnOutOfRange,
		OutOfRange:        h.OutOfRange,
	}
}

func (h *Histogram) Add(n int64) {
	err := h.Histogram.RecordValue(n)
	if err != nil {
		if h.PanicOnOutOfRange {
			panic(err)
		}
		h.OutOfRange++
	}
}

func (h *Histogram) AddN(n, count int64) {
	err := h.Histogram.RecordValues(n, count)
	if err != nil {
		if h.PanicOnOutOfRange {
			panic(err)
		}
		h.OutOfRange += count
	}
}

// MarshalJSON returns a JSON representation of Histogram:
//
//  {
//      "min_trackable": number,
//      "max_trackable": number,
//      "significant_figures": number,
//
//      "total": number,
//      "min": number,
//      "max": number,
//      "mean": number,
//      "sd": number,
//      "cumulative": {
//      },
//      "distribution": {
//      },
//      "quantiles": {
//      }
//  }
//
func (h *Histogram) MarshalJSON() ([]byte, error) {
	var buf json.Buffer
	buf.StartObject()

	buf.Write([]json.KV{
		{"min_trackable", h.Histogram.LowestTrackableValue()},
		{"max_trackable", h.Histogram.HighestTrackableValue()},
		{"significant_figures", h.Histogram.SignificantFigures()},

		{"total", h.Histogram.TotalCount()},
		{"outofrange", h.OutOfRange},
		{"min", h.Histogram.Min()},
		{"max", h.Histogram.Max()},
		{"mean", h.Histogram.Mean()},
		{"sd", h.Histogram.StdDev()},
	})

	for i, s := range quantileSanitized {
		k := fmt.Sprintf("q%s", s)
		q := quantiles[i]
		v := h.Histogram.ValueAtQuantile(q)
		buf.WriteKey(k).Write(v)
	}

	// Cumulative
	buf.WriteKey("cumulative_distribution").StartArray()
	for _, b := range h.Histogram.CumulativeDistribution() {
		buf.Next()
		buf.Write(json.Object{
			{"quantile", b.Quantile},
			{"value_at", b.ValueAt},
			{"count", b.Count},
		})
	}
	buf.EndArray()

	// Distribution
	buf.WriteKey("distribution").StartArray()
	for _, b := range h.Histogram.Distribution() {
		buf.Next()
		buf.Write(json.Object{
			{"from", b.From},
			{"to", b.To},
			{"count", b.Count},
		})
	}
	buf.EndArray()
	buf.EndObject()

	return buf.Bytes(), nil
}

func (h *Histogram) DistributionCSV() ([]byte, error) {
	var buf bytes.Buffer
	buf.WriteString("from,to,count\n")
	for _, x := range h.Distribution() {
		buf.WriteString(strconv.FormatInt(x.From, 10))
		buf.WriteRune(',')
		buf.WriteString(strconv.FormatInt(x.To, 10))
		buf.WriteRune(',')
		buf.WriteString(strconv.FormatInt(x.Count, 10))
		buf.WriteRune('\n')
	}
	return buf.Bytes(), nil
}

func (h *Histogram) CumulativeDistributionCSV() ([]byte, error) {
	var buf bytes.Buffer
	buf.WriteString("quantile,value_at\n")
	writeq := func(q float64) {
		buf.WriteString(strconv.FormatFloat(q, 'f', -1, 64))
		buf.WriteRune(',')
		buf.WriteString(strconv.FormatInt(h.Histogram.ValueAtQuantile(q), 10))
		buf.WriteRune('\n')
	}
	for i := 0; i < 99; i++ {
		writeq(float64(i))
	}
	for i := 0; i < 9; i++ {
		writeq(99 + float64(i)/10)
	}
	for i := 0; i < 10; i++ {
		writeq(99.9 + float64(i)/100)
	}
	q := 99.99
	for i := 0; i < 9; i++ {
		q += 9 / math.Pow(10, 3+float64(i))
		writeq(q)
	}
	writeq(100)
	return buf.Bytes(), nil
}

func (h *Histogram) Header() []string {
	return []string{
		"min_trackable", "max_trackable", "significant_figures", "total", "outofrange", "min", "max", "mean", "sd",
		"q0_0", "q0_1", "q0_2", "q0_3", "q0_4", "q0_5", "q0_6", "q0_7", "q0_75", "q0_8", "q0_85",
		"q0_9", "q0_925", "q0_95", "q0_975", "q0_99", "q0_995", "q0_999", "q0_9999", "q0_99999", "q1_0",
	}
}
func (h *Histogram) Record() []string {
	record := make([]string, 0, 9+len(quantiles))
	for _, v := range []int64{
		h.Histogram.LowestTrackableValue(),
		h.Histogram.HighestTrackableValue(),
		h.Histogram.SignificantFigures(),
		h.Histogram.TotalCount(),
		h.OutOfRange,
		h.Histogram.Min(),
		h.Histogram.Max(),
	} {
		record = append(record, strconv.FormatInt(v, 10))
	}
	for _, v := range []float64{h.Histogram.Mean(), h.Histogram.StdDev()} {
		record = append(record, strconv.FormatFloat(v, 'f', -1, 64))
	}
	for _, v := range quantiles {
		record = append(record, strconv.FormatInt(h.Histogram.ValueAtQuantile(v), 10))
	}
	return record
}

// DurationHistogram {{{1

type DurationHistogram struct {
	hdrhistogram.Histogram
	PanicOnOutOfRange bool
	OutOfRange        int64
}

func NewDurationHistogram(min, max simt.Duration) *DurationHistogram {
	return &DurationHistogram{
		Histogram: *hdrhistogram.New(int64(min), int64(max), 2),
	}
}

func (h *DurationHistogram) Copy() *DurationHistogram {
	s := h.Histogram.Export()
	return &DurationHistogram{
		Histogram:         *hdrhistogram.Import(s),
		PanicOnOutOfRange: h.PanicOnOutOfRange,
		OutOfRange:        h.OutOfRange,
	}
}

func (h *DurationHistogram) Add(d simt.Duration) {
	err := h.Histogram.RecordValue(int64(d))
	if err != nil {
		if h.PanicOnOutOfRange {
			panic(err)
		}
		h.OutOfRange++
	}
}

func (h *DurationHistogram) AddN(d simt.Duration, count int64) {
	err := h.Histogram.RecordValues(int64(d), count)
	if err != nil {
		if h.PanicOnOutOfRange {
			panic(err)
		}
		h.OutOfRange += count
	}
}

func (h *DurationHistogram) MarshalJSON() ([]byte, error) {
	var buf json.Buffer
	buf.StartObject()

	buf.WriteKey("min_trackable").Write(simt.Duration(h.Histogram.LowestTrackableValue()))
	buf.WriteKey("max_trackable").Write(simt.Duration(h.Histogram.HighestTrackableValue()))
	buf.WriteKey("significant_figures").Write(h.Histogram.SignificantFigures())

	buf.WriteKey("total").Write(h.Histogram.TotalCount())
	buf.WriteKey("outofrange").Write(h.OutOfRange)
	buf.WriteKey("min").Write(simt.Duration(h.Histogram.Min()))
	buf.WriteKey("max").Write(simt.Duration(h.Histogram.Max()))
	buf.WriteKey("mean").Write(json.Duration(h.Histogram.Mean()))
	buf.WriteKey("sd").Write(json.Duration(h.Histogram.StdDev()))

	for i, s := range quantileSanitized {
		k := fmt.Sprintf("q%s", s)
		q := quantiles[i]
		v := simt.Duration(h.Histogram.ValueAtQuantile(q))
		buf.WriteKey(k).Write(v)
	}

	// Cumulative
	buf.WriteKey("cumulative_distribution").StartArray()
	for _, b := range h.Histogram.CumulativeDistribution() {
		buf.NextObject()
		buf.WriteKey("quantile").Write(b.Quantile)
		buf.WriteKey("value_at").Write(simt.Duration(b.ValueAt))
		buf.WriteKey("count").Write(b.Count)
		buf.EndObject()
	}
	buf.EndArray()

	// Distribution
	buf.WriteKey("distribution").StartArray()
	for _, b := range h.Histogram.Distribution() {
		buf.NextObject()
		buf.WriteKey("from").Write(simt.Duration(b.From))
		buf.WriteKey("to").Write(simt.Duration(b.To))
		buf.WriteKey("count").Write(b.Count)
		buf.EndObject()
	}
	buf.EndArray()
	buf.EndObject()

	return buf.Bytes(), nil
}

func (h *DurationHistogram) DistributionCSV() ([]byte, error) {
	var buf bytes.Buffer
	buf.WriteString("from,to,count\n")
	for _, x := range h.Distribution() {
		buf.WriteString(simt.Duration(x.From).String())
		buf.WriteRune(',')
		buf.WriteString(simt.Duration(x.To).String())
		buf.WriteRune(',')
		buf.WriteString(strconv.FormatInt(x.Count, 10))
		buf.WriteRune('\n')
	}
	return buf.Bytes(), nil
}

func (h *DurationHistogram) CumulativeDistributionCSV() ([]byte, error) {
	var buf bytes.Buffer
	buf.WriteString("quantile,value_at\n")
	writeq := func(q float64) {
		buf.WriteString(strconv.FormatFloat(q, 'f', -1, 64))
		buf.WriteRune(',')
		buf.WriteString(simt.Duration(h.Histogram.ValueAtQuantile(q)).String())
		buf.WriteRune('\n')
	}
	for i := 0; i < 99; i++ {
		writeq(float64(i))
	}
	for i := 0; i < 9; i++ {
		writeq(99 + float64(i)/10)
	}
	for i := 0; i < 10; i++ {
		writeq(99.9 + float64(i)/100)
	}
	q := 99.99
	for i := 0; i < 9; i++ {
		q += 9 / math.Pow(10, 3+float64(i))
		writeq(q)
	}
	writeq(100)
	return buf.Bytes(), nil
}

func (h *DurationHistogram) Header() []string {
	return []string{
		"min_trackable", "max_trackable", "significant_figures", "total", "outofrange", "min", "max", "mean", "sd",
		"q0_0", "q0_1", "q0_2", "q0_3", "q0_4", "q0_5", "q0_6", "q0_7", "q0_75", "q0_8", "q0_85",
		"q0_9", "q0_925", "q0_95", "q0_975", "q0_99", "q0_995", "q0_999", "q0_9999", "q0_99999", "q1_0",
	}
}
func (h *DurationHistogram) Record() []string {
	record := make([]string, 9, 9+len(quantiles))
	record[0] = simt.Duration(h.Histogram.LowestTrackableValue()).String()
	record[1] = simt.Duration(h.Histogram.HighestTrackableValue()).String()
	record[2] = strconv.FormatInt(h.Histogram.SignificantFigures(), 10)
	record[3] = strconv.FormatInt(h.Histogram.TotalCount(), 10)
	record[4] = strconv.FormatInt(h.OutOfRange, 10)
	record[5] = simt.Duration(h.Histogram.Min()).String()
	record[6] = simt.Duration(h.Histogram.Max()).String()
	record[7] = simt.Duration(h.Histogram.Mean()).String()
	record[8] = simt.Duration(h.Histogram.StdDev()).String()
	for _, v := range quantiles {
		s := simt.Duration(h.Histogram.ValueAtQuantile(v)).String()
		record = append(record, s)
	}
	return record
}

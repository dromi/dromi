// Copyright (c) 2015, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package obs

import (
	"errors"

	"gitlab.com/dromi/dromi"
	"gitlab.com/dromi/dromi/event"
	"gitlab.com/dromi/dromi/script"
)

func init() {
	script.CreateFunc("mark", markFn)
	script.CreateFunc("marks", marksFn)
	script.CreateFunc("bit", bitFn)
	script.CreateFunc("bits", bitsFn)

	script.CreateFunc("obs|filter", Filter)
	script.CreateFunc("ff|and", And)
	script.CreateFunc("ff|or", Or)
	script.CreateFunc("ff|not", Not)
	script.CreateFunc("ff.equal", EqualTo)
	script.CreateFunc("ff.all", AllOf)
	script.CreateFunc("ff.any", AnyOf)
	script.CreateFunc("ff.unequal", UnequalTo)
	script.CreateFunc("ff.none", NoneOf)
	script.CreateFunc("ff.notall", NotAllOf)
}

func markFn(x int64) (uint8, error) {
	if x < 0x0 || x > 0xff {
		return 0, errors.New("mark must be between 0 and 255")
	}
	return uint8(x), nil
}

func marksFn(xs ...int64) (uint8, error) {
	var marks uint8
	for _, x := range xs {
		mark, err := markFn(x)
		if err != nil {
			return 0, err
		}
		marks |= mark
	}
	return marks, nil
}

func bitFn(x int64) (uint8, error) {
	if x > 7 {
		return 0, errors.New("bit must be between 0 and 7")
	}
	return 1 << uint8(x), nil
}

func bitsFn(xs ...int64) (uint8, error) {
	var marks uint8
	for _, x := range xs {
		mark, err := bitFn(x)
		if err != nil {
			return 0, err
		}
		marks |= mark
	}
	return marks, nil
}

type FilterFunc func(marks uint8) bool

func UnequalTo(marks uint8) FilterFunc {
	return func(xs uint8) bool {
		return xs != marks
	}
}

func NoneOf(marks uint8) FilterFunc {
	return func(xs uint8) bool {
		return xs&marks == 0
	}
}

func NotAllOf(marks uint8) FilterFunc {
	return func(xs uint8) bool {
		return xs&marks != marks
	}
}

func AnyOf(marks uint8) FilterFunc {
	return func(xs uint8) bool {
		return xs&marks != 0
	}
}

func AllOf(marks uint8) FilterFunc {
	return func(xs uint8) bool {
		return xs&marks == marks
	}
}

func EqualTo(marks uint8) FilterFunc {
	return func(xs uint8) bool {
		return xs == marks
	}
}

func And(fn ...FilterFunc) FilterFunc {
	return func(xs uint8) bool {
		for _, f := range fn {
			if !f(xs) {
				return false
			}
		}
		return true
	}
}

func Or(fn ...FilterFunc) FilterFunc {
	return func(xs uint8) bool {
		for _, f := range fn {
			if f(xs) {
				return true
			}
		}
		return false
	}
}

func Not(fn FilterFunc) FilterFunc {
	return func(xs uint8) bool {
		return !fn(xs)
	}
}

func Filter(id string, fn FilterFunc, s *dromi.Slave) *dromi.Slave {
	if id == "" {
		panic("id must be non-empty")
	}
	if s.Observe == nil {
		panic("cannot apply filter")
	}
	if s.Trigger&event.Observation != 0 {
		panic("filter incompatible with slaves that generate observations")
	}

	s.Endpoint += ".filter_" + id
	observe := s.Observe
	s.Observe = func(e *event.Event) {
		if fn(e.Job.Pod.Marks) {
			observe(e)
		}
	}
	return s
}

// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package obs

import (
	"fmt"

	"github.com/goulash/csv"

	"gitlab.com/dromi/dromi"
	"gitlab.com/dromi/dromi/event"
	"gitlab.com/dromi/dromi/obs/accum"
	"gitlab.com/dromi/dromi/script"
	"gitlab.com/dromi/dromi/simt"
)

func init() {
	script.CreateFunc("obs.hist-job-total-wait", DurationHistogramOfJobTotalWaitingTime)
	script.CreateFunc("obs.hist-job-each-wait", DurationHistogramOfJobWaitingTimes)
	script.CreateFunc("obs.hist-job-total-sched", DurationHistogramOfJobTotalSchedulingTime)
	script.CreateFunc("obs.hist-job-each-sched", DurationHistogramOfJobSchedulingTimes)
	script.CreateFunc("obs.hist-job-assignment", DurationHistogramOfJobAssignmentTime)
	script.CreateFunc("obs.hist-job-conflicts", HistogramOfJobConflicts)
	script.CreateFunc("obs.hist-job-sched-attempts", HistogramOfJobSchedulingAttempts)
	script.CreateFunc("obs.hist-job-run-time", DurationHistogramOfJobRunTime)
	script.CreateFunc("obs.hist-job-alive-time", DurationHistogramOfJobAliveTime)
	script.CreateFunc("obs.hist-accepted-cores", HistogramOfAcceptedJobCores)
	script.CreateFunc("obs.hist-accepted-memory", HistogramOfAcceptedJobMemory)
	script.CreateFunc("obs.hist-rejected-cores", HistogramOfRejectedJobCores)
	script.CreateFunc("obs.hist-rejected-memory", HistogramOfRejectedJobMemory)
	script.CreateFunc("obs.hist-scheduler-size", HistogramOfSchedulerSize)
}

func DurationHistogram(
	fn func(h *accum.DurationHistogram, e *event.Event),
	min, max simt.Duration,
	trigger event.Type,
	endpoint string) *dromi.Slave {
	h := accum.NewDurationHistogram(min, max)
	return &dromi.Slave{
		Endpoint: endpoint,
		Serve:    func() interface{} { return h.Copy() },
		ServeMap: map[string]func() interface{}{
			"/dist": func() interface{} {
				return csv.NewMarshaler(h.DistributionCSV())
			},
			"/cumdist": func() interface{} {
				return csv.NewMarshaler(h.CumulativeDistributionCSV())
			},
		},
		Reset:   func(_ simt.Time) { h.Reset() },
		Trigger: trigger,
		Observe: func(e *event.Event) { fn(h, e) },
	}
}

func Histogram(fn func(h *accum.Histogram, e *event.Event), min, max int64, trigger event.Type, endpoint string) *dromi.Slave {
	h := accum.NewHistogram(min, max)
	return &dromi.Slave{
		Endpoint: endpoint,
		Serve:    func() interface{} { return h.Copy() },
		ServeMap: map[string]func() interface{}{
			"/dist": func() interface{} {
				return csv.NewMarshaler(h.DistributionCSV())
			},
			"/cumdist": func() interface{} {
				return csv.NewMarshaler(h.CumulativeDistributionCSV())
			},
		},
		Reset:   func(_ simt.Time) { h.Reset() },
		Trigger: trigger,
		Observe: func(e *event.Event) { fn(h, e) },
	}
}

func DurationHistogramOfJobTotalWaitingTime() *dromi.Slave {
	return DurationHistogram(
		func(h *accum.DurationHistogram, e *event.Event) {
			h.Add(e.Job.WaitingDuration())
		},
		1*simt.Microsecond,               // min
		24*simt.Hour,                     // max
		event.Assignment|event.Rejection, // trigger
		"/histograms/job-total-waiting",  // endpoint
	)
}

func DurationHistogramOfJobWaitingTimes() *dromi.Slave {
	return DurationHistogram(
		func(h *accum.DurationHistogram, e *event.Event) {
			for _, d := range e.Job.WaitingDurations() {
				h.Add(d)
			}
		},
		1*simt.Microsecond,
		24*simt.Hour,
		event.Assignment|event.Rejection,
		"/histograms/job-each-waiting",
	)
}

func DurationHistogramOfJobSchedulingTimes() *dromi.Slave {
	return DurationHistogram(
		func(h *accum.DurationHistogram, e *event.Event) {
			for _, d := range e.Job.SchedulingDurations() {
				h.Add(d)
			}
		},
		1*simt.Microsecond,
		24*simt.Hour,
		event.Assignment|event.Rejection, // trigger
		"/histograms/job-each-scheduling",
	)
}

func DurationHistogramOfJobTotalSchedulingTime() *dromi.Slave {
	return DurationHistogram(
		func(h *accum.DurationHistogram, e *event.Event) {
			h.Add(e.Job.SchedulingDuration())
		},
		1*simt.Microsecond,
		24*simt.Hour,
		event.Assignment|event.Rejection, // trigger
		"/histograms/job-total-scheduling",
	)
}

func DurationHistogramOfJobAssignmentTime() *dromi.Slave {
	return DurationHistogram(
		func(h *accum.DurationHistogram, e *event.Event) {
			h.Add(e.Job.AssignmentDuration())
		},
		1*simt.Microsecond,               // min
		24*simt.Hour,                     // max
		event.Assignment|event.Rejection, // trigger
		"/histograms/job-assignment",     // endpoint
	)
}

func HistogramOfJobConflicts() *dromi.Slave {
	return Histogram(
		func(h *accum.Histogram, e *event.Event) {
			h.Add(int64(e.Job.NumConflicts()))
		},
		0,
		100,
		event.Assignment|event.Rejection, // trigger
		"/histograms/job-conflicts",
	)
}

func HistogramOfJobSchedulingAttempts() *dromi.Slave {
	return Histogram(
		func(h *accum.Histogram, e *event.Event) {
			h.Add(int64(e.Job.NumSchedulingAttempts()))
		},
		0,
		100,
		event.Assignment|event.Rejection, // trigger
		"/histograms/job-scheduling-attempts",
	)
}

func DurationHistogramOfJobRunTime() *dromi.Slave {
	return DurationHistogram(
		func(h *accum.DurationHistogram, e *event.Event) {
			h.Add(e.Job.RunningDuration())
		},
		1*simt.Millisecond,
		40*simt.Day,
		event.Departure,
		"/histograms/job-run-time",
	)
}

func DurationHistogramOfJobAliveTime() *dromi.Slave {
	return DurationHistogram(
		func(h *accum.DurationHistogram, e *event.Event) {
			h.Add(e.Job.AliveDuration())
		},
		1*simt.Millisecond,
		40*simt.Day,
		event.Departure,
		"/histograms/job-alive-time",
	)
}

func HistogramOfAcceptedJobCores() *dromi.Slave {
	return Histogram(
		func(h *accum.Histogram, e *event.Event) {
			c := int64(float64(e.Job.Pod.MinCores) * 1000000)
			h.Add(c)
		},
		0,
		32000000,
		event.Assignment,
		"/histograms/accepted-job-microcores",
	)
}

func HistogramOfAcceptedJobMemory() *dromi.Slave {
	return Histogram(
		func(h *accum.Histogram, e *event.Event) {
			h.Add(int64(e.Job.Pod.Memory))
		},
		0,
		128000,
		event.Assignment,
		"/histograms/accepted-job-memory",
	)
}

func HistogramOfRejectedJobCores() *dromi.Slave {
	return Histogram(
		func(h *accum.Histogram, e *event.Event) {
			c := int64(float64(e.Job.Pod.MinCores) * 1000000)
			h.Add(c)
		},
		0,
		32000000,
		event.Rejection,
		"/histograms/rejected-job-microcores",
	)
}

func HistogramOfRejectedJobMemory() *dromi.Slave {
	return Histogram(
		func(h *accum.Histogram, e *event.Event) {
			h.Add(int64(e.Job.Pod.Memory))
		},
		0,
		128000,
		event.Rejection,
		"/histograms/rejected-job-memory",
	)
}

// HistogramOfSchedulerSize is a histogram of the number of jobs currently
// in the scheduler component, regardless of whether they are being processed
// or not. This is determined by event occurrences.
func HistogramOfSchedulerSize() *dromi.Slave {
	var (
		last simt.Time
		n    int64
	)
	return Histogram(
		func(h *accum.Histogram, e *event.Event) {
			diff := simt.Duration(e.Time - last)
			last = e.Time

			// We want to not add such heavily weighted values, so we scale it
			// down by rounding down to microseconds. Problem is what to do
			// when count is zero... do we round everything up?
			count := int64(diff/simt.Microsecond) + 1
			h.AddN(n, count)

			switch e.Type {
			case event.Arrival:
				n++
			case event.Assignment, event.Rejection:
				n--
			default:
				panic(fmt.Sprintf("unexpected event type; event is %v", e))
			}
		},
		0,
		1000000, // if it's more, you're in trouble anyway
		event.Arrival|event.Assignment|event.Rejection,
		"/histograms/scheduler-size",
	)
}

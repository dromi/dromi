// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package obs

import (
	"strconv"

	"github.com/goulash/csv"
	"gitlab.com/dromi/dromi"
	"gitlab.com/dromi/dromi/event"
	"gitlab.com/dromi/dromi/obs/accum"
	"gitlab.com/dromi/dromi/script"
	"gitlab.com/dromi/dromi/simt"
)

func init() {
	script.CreateFunc("obs.job-stats", JobStats)
	script.CreateFunc("obs.rejected-job-stats", RejectedJobStats)
}

// JobStatsEndpoint is the default endpoint for JobStats observers.
var JobStatsEndpoint = "/jobstats"
var RejectedJobStatsEndpoint = "/rejstats"

// JobStats creates a new observer that keeps track of all the different
// statistics that can be applied to a job at the end of its life.
func JobStats() *dromi.Slave {
	var js jobStats
	return &dromi.Slave{
		Endpoint: JobStatsEndpoint,
		Serve:    func() interface{} { return js.Copy() },
		Reset:    js.Reset,
		Trigger:  event.Assignment | event.Departure,
		Observe:  js.Observe,
	}
}

func RejectedJobStats() *dromi.Slave {
	var js jobStats
	return &dromi.Slave{
		Endpoint: RejectedJobStatsEndpoint,
		Serve:    func() interface{} { return js.Copy() },
		Reset:    js.Reset,
		Trigger:  event.Rejection,
		Observe:  js.Observe,
	}
}

type jobStats struct {
	Time simt.Time `json:"time"` // time of statistic
	M    int       `json:"m"`    // number of assignments scanned
	N    int       `json:"n"`    // number of departures scanned

	AliveTime          accum.TimeRun `json:"alive_time"`          // time from birth til death
	WaitingTime        accum.TimeRun `json:"waiting_time"`        // time from birth til first scheduling
	SchedulingTime     accum.TimeRun `json:"scheduling_time"`     // time from first scheduling til assignment
	AssignmentTime     accum.TimeRun `json:"assignment_time"`     // time from birth til assignment
	SchedulingAttempts accum.Run     `json:"scheduling_attempts"` // number of scheduling attempts
	Conflicts          accum.Run     `json:"conflicts"`           // number of conflicts
	RunTime            accum.TimeRun `json:"run_time"`            // time from assignment til death
}

// We can do this because shallow copies of run and timeRun are full copies.
func (j jobStats) Copy() *jobStats { return &j }

func (j *jobStats) Reset(t simt.Time) {
	j.Time = t
	j.M = 0
	j.N = 0
	j.AliveTime.Reset()
	j.WaitingTime.Reset()
	j.SchedulingTime.Reset()
	j.AssignmentTime.Reset()
	j.SchedulingAttempts.Reset()
	j.Conflicts.Reset()
	j.RunTime.Reset()
}

func (j *jobStats) Observe(e *event.Event) {
	d := e.Job
	j.Time = e.Time
	switch e.Type {
	case event.Assignment:
		j.M++
		j.WaitingTime.Add(float64(d.Sched[0] - d.Arrival))
		j.SchedulingTime.Add(float64(d.SchedEnd - d.Sched[0]))
		j.AssignmentTime.Add(float64(d.SchedEnd - d.Arrival))
		j.SchedulingAttempts.Add(float64(d.NumSchedulingAttempts()))
		j.Conflicts.Add(float64(d.Conflicts))
	case event.Rejection:
		j.M++
		j.N++
		j.WaitingTime.Add(float64(d.Sched[0] - d.Arrival))
		j.SchedulingTime.Add(float64(d.SchedEnd - d.Sched[0]))
		j.AssignmentTime.Add(float64(d.SchedEnd - d.Arrival))
		j.SchedulingAttempts.Add(float64(d.NumSchedulingAttempts()))
		j.Conflicts.Add(float64(d.Conflicts))
		j.AliveTime.Add(float64(d.Departure - d.Arrival))
		j.RunTime.Add(0)
	case event.Departure:
		j.N++
		j.AliveTime.Add(float64(d.Departure - d.Arrival))
		j.RunTime.Add(float64(d.Departure - d.SchedEnd))
	default:
		panic("unexpected event")
	}
}

func (j *jobStats) Header() []string {
	return []string{
		"time",
		"m",
		"n",

		"alive_time_mean",
		"alive_time_std",
		"alive_time_min",
		"alive_time_max",

		"waiting_time_mean",
		"waiting_time_std",
		"waiting_time_min",
		"waiting_time_max",

		"scheduling_time_mean",
		"scheduling_time_std",
		"scheduling_time_min",
		"scheduling_time_max",

		"assignment_time_mean",
		"assignment_time_std",
		"assignment_time_min",
		"assignment_time_max",

		"scheduling_attempts_mean",
		"scheduling_attempts_var",
		"scheduling_attempts_std",
		"scheduling_attempts_min",
		"scheduling_attempts_max",

		"conflicts_mean",
		"conflicts_var",
		"conflicts_std",
		"conflicts_min",
		"conflicts_max",

		"run_time_mean",
		"run_time_std",
		"run_time_min",
		"run_time_max",
	}
}

func (j *jobStats) Record() []string {
	rs := []csv.Recorder{&j.AliveTime,
		&j.WaitingTime,
		&j.SchedulingTime,
		&j.AssignmentTime,
		&j.SchedulingAttempts,
		&j.Conflicts,
		&j.RunTime,
	}
	// TODO: this length is probably too much
	records := make([]string, 3, 3+len(rs)*5)
	records[0] = j.Time.String()
	records[1] = strconv.FormatInt(int64(j.M), 10)
	records[2] = strconv.FormatInt(int64(j.N), 10)
	for _, r := range rs {
		records = append(records, r.Record()...)
	}
	return records
}

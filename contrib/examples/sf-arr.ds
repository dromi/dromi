; vim: set filetype=lisp:
#include "common.ds"

(var NAME "sf-arr.2")
(var DESC "scheduler frameworks vs. arrival rate: monolith, partition, concurrent")

(set REPLICATIONS 4)

(set QUALITY_ASSURANCE true)

(set CLUST_SIZE 100)
(set CLUSTER (cls.default hw.laptop))

(func monoshufn8 () (sf.monolith (eq.arraylist 16) (sch.shufn 8)))
(func shufn8 () (sch.shufn 8))

(var sch (DIMENSION "sch" "Scheduler"
                    "single:shufn16"
                    "monolith:shufn16"
                    "partition:shufn8x2"
                    "concurrent:shufn8x2"
                    "qsPartition:shufn8x2"
                    "qsConcurrent:shufn8x2"
                    "blockingPartition:shufn8x8"
                    "blockingConcurrent:shufn8x8"
                    ))
(set SCHEDULER (switch sch
    (case "single:shufn16"
        (sf.single (sch.shufn 16)))
    (case "monolith:shufn16"
        (sf.monolith (eq.arraylist 32) (sch.shufn 16)))
    (case "partition:shufn8x2"
        (sf.partition (part.equal) (mux.robin 2)
                      (monoshufn8) (monoshufn8)))
    (case "concurrent:shufn8x2"
        (sf.concurrent (mux.robin 2)
                       (monoshufn8) (monoshufn8)))
    (case "qsPartition:shufn8x2"
        (sf.queueSharingPartition (part.equal) (eq.arraylist 32)
                                  (shufn8) (shufn8)))
    (case "qsConcurrent:shufn8x2"
        (sf.queueSharingConcurrent (eq.arraylist 32)
                                   (shufn8) (shufn8)))
    (case "blockingPartition:shufn8x8"
        (sf.blockingPartition (part.equal)
                              (shufn8) (shufn8) (shufn8) (shufn8)
                              (shufn8) (shufn8) (shufn8) (shufn8)))
    (case "blockingConcurrent:shufn8x8"
        (sf.blockingConcurrent (shufn8) (shufn8) (shufn8) (shufn8)
                               (shufn8) (shufn8) (shufn8) (shufn8)))
    ))

(var bps (DIMENSION "aps" "Arrivals/s" 1.0 10.0 20.0 30.0 40.0 50.0 60.0 70.0))
(set GENERATOR (gen.orig (proc.poisson bps)))
(var SLAVES slaves.default)

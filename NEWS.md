NEWS
====

------------------------------------------------------------------------------

## 3. June
  - Change: package paths moved from github to gitlab

## 20. April (65cde3f81)
  - Change: improved QA reporting in ClusterQA
  - Bugfix: #9 revising Concurrent conflict definition to include equal times
  - Bugfix: Machine.Free should not set reaper to nil

## 18. April
  - New: Machine interface now has String() method for better debugging
  - New: ReaperQA available (though not exported yet)
  - Change: Job.String has more scheduling info
  - Change: performance improvement to reaper (caching Next)
  - Change: performance improvement to machine (caching NJobs)
  - Bugfix: data race resolved by removing last event list
  - Bugfix: #7 Monolith did not pop jobs from the queue

## 13. April
  - WIP: #7 trying to track down a bug
  - Bugfix: rejections are no longer pushed back onto the event queue
  - Refactor: cl.Cluster -> c.cluster for simplified documentation and
    understanding.

## 12. April (06091c786)
  - New: Single `sf.single` scheduler framework
  - New: QueueSharing scheduler framework with `sf.queueSharingPartition` and
    `sf.queueSharingConcurrent` DromiScript functions.
  - New: Blocking scheduler framework wish `sf.blockingPartition` and
    `sf.blockingConcurrent` DromiScript functions.
  - New: Podder Multiplexer
  - Change: Simplifying `sched` package and unifying multiplexer functions
    between Partition, Concurrent, and SchedulerMultiplexer (for
    micro-schedulers).
  - Change: `dromi.Scheduler.Submit` API changed to now return an error.
    Simulation was modified to take this into account.
  - Bugfix: Simulation did not modify events that were rejected to have a type
    that matched before calling Observe.

------------------------------------------------------------------------------

## 11. April
  - New: Retimer `sched.retime` scheduler


// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

// Package dromi provides an environment for running cluster simulations.
//
// Each simulation consists of a Simulation object, which manages the
// interactions between a Generator, a Scheduler, and a Cluster.
// It does this event by event. At each event, it cedes control back
// to the Master object. The master takes care of running a simulation,
// pausing it when the user wants to look inside, gathering all observers,
// providing online access, etc.
//
// Interfaces
//
//  Generator
//  Scheduler
//  Cluster
//  Machine
//
// Each interface is accompanied by a quality assurance wrapper, which
// ensures that all the requirements are met by an implementation.
package dromi

// Copyright (c) 2015, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package webutil

import (
	"encoding/json"
	"fmt"
	"net/http"
	"text/template"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/goulash/csv"
)

func TemplateHandler(t *template.Template, v interface{}) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		err := t.Execute(w, v)
		if err != nil {
			log.Errorln("Error executing template:", err)
			http.Error(w, "error executing template", 500)
		}
	})
}

func CachelessHandler(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
		w.Header().Set("Pragma", "no-cache")
		w.Header().Set("Expires", "0")
		h.ServeHTTP(w, r)
	})
}

func LoggingHandler(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		h.ServeHTTP(w, r)
		end := time.Now()
		log.Debugf("[%s] %q %v", r.Method, r.URL.String(), end.Sub(start))
	})
}

func StructHandler(v interface{}) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ServeStruct(w, r, v)
	})
}

func TextHandler(s string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, s)
	})
}

func StringerHandler(v fmt.Stringer) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, v)
	})
}

func ServeStruct(w http.ResponseWriter, r *http.Request, v interface{}) {
	q := r.URL.Query()
	switch q.Get("type") {
	case "string":
		t, ok := v.(fmt.Stringer)
		if !ok {
			http.Error(w, "error: this endpoint does not support string type", 400)
			return
		}
		fmt.Fprintln(w, t)
	case "csv":
		bs, err := csv.Marshal(v)
		if err != nil {
			http.Error(w, "error: this endpoint does not support csv type: "+err.Error(), 400)
		} else {
			w.Write(bs)
		}
	case "json":
		bs, err := json.Marshal(v)
		if err != nil {
			http.Error(w, fmt.Sprintf(`{"error": %q}`, err), 500)
		} else {
			w.Write(bs)
		}
	case "": // this is the default if type is not defined
		bs, err := json.MarshalIndent(v, "", "    ")
		if err != nil {
			http.Error(w, fmt.Sprintf("{\n    \"error\": %q\n}", err), 500)
		} else {
			w.Write(bs)
		}
	default:
		http.Error(w, "error: type unknown", 400)
	}
}

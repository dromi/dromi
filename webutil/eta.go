// Copyright (c) 2015, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package webutil

import (
	"bytes"
	"fmt"
	"sync"
	"time"
)

// ETA calculates the estimated time of arrival based on the updates it receives.
type ETA struct {
	prev float64
	cur  float64
	beg  time.Time
	end  time.Time
	eta  time.Duration
}

func (e ETA) Float64() float64 { return e.cur }

func (e ETA) ETA() time.Duration {
	return (e.eta / time.Second) * time.Second
}

func (e ETA) Elapsed() time.Duration {
	if e.cur < 1 {
		return time.Now().Sub(e.beg)
	}
	return e.end.Sub(e.beg)
}

func (e ETA) String() string {
	return fmt.Sprintf("%.1f%% complete with ETA %v", e.cur, e.ETA())
}

func (e ETA) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf(`{"complete":%.1f,"eta":%q}`, e.cur, e.ETA())), nil
}

func (e ETA) MarshalCSV() ([]byte, error) {
	return []byte(fmt.Sprintf("complete,eta\n%.1f,%s\n", e.cur, e.ETA())), nil
}

func (e ETA) Bar(width int) string {
	var buf bytes.Buffer
	e.bar(&buf, width)
	return buf.String()
}

func (e ETA) BarWithETA(width int) string {
	var buf bytes.Buffer
	e.bar(&buf, width)
	buf.WriteRune(' ')

	if e.cur <= 0 {
		buf.WriteString("  0.0%")
	} else if e.cur < 1 {
		buf.WriteString(fmt.Sprintf("%5.1f%%  –%v", e.cur*100, e.ETA()))
	} else {
		buf.WriteString("100.0%")
	}
	return buf.String()
}

// BarWithSpecialETA is the same as BarWithETA except that it embeds
// the elapsed time once it is complete, so it looks like this:
//
//   |===============               |  51.6%  –8s
//   |============- 16s -===========| 100.0%
func (e ETA) BarWithSpecialETA(width int) string {
	if e.cur < 1 {
		return e.BarWithETA(width)
	}

	et := (e.Elapsed() / time.Second) * time.Second // round it down to seconds
	elapsed := et.String()
	mid := len(elapsed) + 4 // +4 for '- ' on each side
	if mid > (width - 2) {  // -2 because we want at least one = on each side
		return e.BarWithETA(width)
	}
	right := (width - mid) / 2
	left := width - mid - right

	var buf bytes.Buffer
	buf.WriteRune('|')
	for i := 0; i < left; i++ {
		buf.WriteRune('=')
	}
	buf.WriteString("- ")
	buf.WriteString(elapsed)
	buf.WriteString(" -")
	for i := 0; i < right; i++ {
		buf.WriteRune('=')
	}
	buf.WriteString("| 100.0%")
	return buf.String()
}

func (e ETA) bar(buf *bytes.Buffer, w int) {
	buf.WriteRune('|')
	n := int(e.cur * float64(w))
	for i := 0; i < n; i++ {
		buf.WriteRune('=')
	}
	for i := n; i < w; i++ {
		buf.WriteRune(' ')
	}
	buf.WriteRune('|')
}

func (e *ETA) Begin() {
	e.beg = time.Now()
}

func (e *ETA) End() {
	e.end = time.Now()
	e.eta = time.Duration(0)
	e.cur = 1
}

// Update assumes that p is within 0 and 1 inclusive.
func (e *ETA) Update(p float64) {
	dur := time.Now().Sub(e.beg)
	e.prev = e.cur
	e.eta = time.Duration(float64(dur)/p) - dur
	e.cur = p
}

func (e *ETA) UpdateSafe(p float64) {
	if p < 0 {
		p = 0
	} else if p > 1 {
		p = 1
	}
	e.Update(p)
}

type SyncETA struct {
	eta ETA
	mut sync.Mutex
	cur int
	lim int

	gran float64
	next float64
	f    func(ETA)
}

func (e *SyncETA) ETA() ETA {
	e.mut.Lock()
	defer e.mut.Unlock()
	return e.eta
}

// Cannot be used synchronously.
func (e *SyncETA) Every(granularity float64, f func(e ETA)) {
	if granularity <= 0 {
		panic("granularity must be positive")
	}

	e.gran = granularity
	e.f = f
	e.next = 0
	p := e.eta.Float64()
	for e.next <= p {
		e.next += e.gran
	}
}

func (e *SyncETA) Begin() {
	e.mut.Lock()
	e.eta.Begin()
	e.mut.Unlock()
}

func (e *SyncETA) End() {
	e.mut.Lock()
	e.eta.End()
	e.mut.Unlock()
}

func (e *SyncETA) Add(n int) {
	e.mut.Lock()
	e.lim += n
	e.update()
	e.mut.Unlock()
}

func (e *SyncETA) Done() {
	e.mut.Lock()
	e.cur++
	e.update()
	e.mut.Unlock()
}

func (e *SyncETA) update() {
	e.eta.Update(float64(e.cur) / float64(e.lim))

	p := e.eta.Float64()
	if e.f != nil && e.next <= p {
		e.f(e.eta)
		for e.next <= p {
			e.next += e.gran
		}
	}
}

// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package mux

import (
	"github.com/goulash/stat/dist"
	"gitlab.com/dromi/dromi"
	"gitlab.com/dromi/dromi/event"
	"gitlab.com/dromi/dromi/script"
	"gitlab.com/dromi/dromi/simt"
)

func init() {
	script.CreateFunc("mux.min", NewMinimumMux)
	script.CreateFunc("mux.random", NewRandomMux)
	script.CreateFunc("mux.robin", func(n int64) MultiplexFunc {
		return NewRoundRobinMux(int(n))
	})
	script.CreateFunc("mux.slice", func(xs []int64) MultiplexFunc {
		ys := make([]int, len(xs))
		for i, x := range xs {
			ys[i] = int(x)
		}
		return NewAlternatingMux(ys)
	})
	script.CreateFunc("mux.prio", func(def int64, prio ...int64) MultiplexFunc {
		cutoff := make([]int, len(prio))
		for i, p := range prio {
			cutoff[i] = int(p)
		}
		return NewPriorityMux(int(def), cutoff...)
	})
	script.CreateFunc("mux.runtime", func(def int64, dur ...simt.Duration) MultiplexFunc {
		return NewRuntimeMux(int(def), dur...)
	})
	script.CreateFunc("mux.runtimeS", func(def int64, dur ...string) MultiplexFunc {
		cutoff := make([]simt.Duration, len(dur))
		for i, d := range dur {
			cd, err := simt.ParseDuration(d)
			if err != nil {
				panic(err)
			}
			cutoff[i] = cd
		}
		return NewRuntimeMux(int(def), cutoff...)
	})
	script.CreateFunc("mux.eqmarks", func(def int64, marks ...int64) MultiplexFunc {
		mmap := make([]uint8, len(marks))
		for i, p := range marks {
			mmap[i] = uint8(p)
		}
		return NewMarkerMux(int(def), mmap...)
	})
	script.CreateFunc("mux.hasmarks", func(def int64, marks ...int64) MultiplexFunc {
		mmap := make([]uint8, len(marks))
		for i, p := range marks {
			mmap[i] = uint8(p)
		}
		return NewPartialMarkerMux(int(def), mmap...)
	})
}

type MultiplexFunc func(e *event.Event) int

// resolve returns an index to one of the schedulers.
func NewMinimumMux(sched ...dromi.Scheduler) MultiplexFunc {
	return func(_ *event.Event) int {
		// We need to find the scheduler with the least number of jobs.
		j, min := 0, sched[0].Len()
		for i, s := range sched[1:] {
			m := s.Len()
			if m < min {
				min = m
				j = i + 1

				// There is no point looking for anything less than zero,
				// so quit as soon as this happens.
				if min == 0 {
					break
				}
			}
		}
		return j
	}
}

func NewRandomMux(d dist.Discrete) MultiplexFunc {
	return func(_ *event.Event) int { return int(d.Int63()) }
}

func NewRoundRobinMux(n int) MultiplexFunc {
	var idx = -1
	return func(_ *event.Event) int {
		idx = (idx + 1) % n
		return idx
	}
}

// NewAlternatingMux creates a very similar multiplexer to a round-robin
// multiplexer, except that the options are specified explicitely.
func NewAlternatingMux(xs []int) MultiplexFunc {
	var idx, n = -1, len(xs)
	return func(_ *event.Event) int {
		idx = (idx + 1) % n
		return xs[idx]
	}
}

// NewPriorityMux returns a multiplexer that routes events to schedulers based
// on priority, the index returned is the first that has a cutoff value higher
// than the priority of the event.
//
// For example, to give jobs with a cutoff higher than 9 to a special scheduler
// at index 1, you could call:
//
//  NewPriorityMux(1, 9)
//
// Or equivalently, assuming that priorities never exceed 99:
//
//  NewPriorityMux(0, 9, 100)
//
func NewPriorityMux(def int, cutoff ...int) MultiplexFunc {
	return func(e *event.Event) int {
		for i, c := range cutoff {
			if c > int(e.Job.Pod.Priority) {
				return i
			}
		}
		return def
	}
}

// NewRuntimeMux returns a multiplexer that routes events to schedulers based
// on runtime in the same way that NewPriorityMux does.
//
// For example, to give jobs with a runtime higher than 5s
// to a special scheduler at index 1, you could call:
//
//  NewRuntimeMux(1, 5*simt.Second)
//
func NewRuntimeMux(def int, cutoff ...simt.Duration) MultiplexFunc {
	return func(e *event.Event) int {
		for i, c := range cutoff {
			if c > e.Job.Pod.Runtime {
				return i
			}
		}
		return def
	}
}

func NewMarkerMux(def int, marks ...uint8) MultiplexFunc {
	return func(e *event.Event) int {
		for i, c := range marks {
			if e.Job.Marks() == c {
				return i
			}
		}
		return def
	}
}

func NewPartialMarkerMux(def int, marks ...uint8) MultiplexFunc {
	return func(e *event.Event) int {
		for i, c := range marks {
			if e.Job.Marks()&c == c {
				return i
			}
		}
		return def
	}
}

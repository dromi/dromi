// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package dromi

import (
	"errors"
	"fmt"

	log "github.com/Sirupsen/logrus"
	"gitlab.com/dromi/dromi/event"
	"gitlab.com/dromi/dromi/internal/qa"
	"gitlab.com/dromi/dromi/script"
	"gitlab.com/dromi/dromi/simt"
)

var (
	ErrSchedulerQueueFull   = errors.New("scheduler queue is full")
	ErrSchedulingImpossible = errors.New("no machine in cluster can support the job")
	ErrSchedulingFailed     = errors.New("no machine could be found to support the job")
)

type SchedulingConflictError struct {
	Layer           int
	Machine         Machine
	LastAssignment  simt.Time
	SchedulingStart simt.Time
}

func (e *SchedulingConflictError) Error() string {
	return fmt.Sprintf("scheduling conflict: scheduling start time %v before last assignment time %v", e.SchedulingStart, e.LastAssignment)
}

// Scheduler schedules jobs to machines in a Cluster. For this reason,
// a scheduler must be pre-initialized to a cluster.
type Scheduler interface {
	// Initialize initializes a scheduler with the simulation cluster.
	Initialize(c Cluster) error

	// Finalize is called to indicate that the scheduler will no longer
	// be used and can discard as much state as possible.
	Finalize() error

	// Len returns the number of queued events, including any jobs currently
	// being scheduled. Thus, 0 means that the scheduler is free, 1 means
	// that there are no jobs queued.
	Len() int

	// QueueLen returns the number of queued event, excluding any jobs currently
	// being scheduled.
	QueueLen() int

	// Submit gives the scheduler a job to schedule and the time at
	// which it arrives. The scheduler is guaranteed that the cluster
	// clock has been advanced to the time at, but no further.
	//
	// We pass the entire event to the scheduler so it can reuse
	// the allocation. This saves us a significant amount of processing.
	//
	// If a submission fails (because the queue is full for example), then
	// an error should be returned, and the event left unmodified.
	//
	// Pre-conditions
	//
	//  ∙ The cluster should have advanced to at least the time of the last assignment event.
	//  ∙ The cluster must not be in the future (past the event).
	//  ∙ The time of the submitted event must be in the future.
	//  ∙ No two events can be submitted at the same time.
	//    Note: while we are guaranteed that two arrival events are not the same,
	//    it is possible for two different events to be submitted at the same time:
	//    SchedulingFailure and Arrival.
	Submit(*event.Event) error

	// Peek returns the time of the next action of the scheduler, be it scheduling
	// a job or returning a scheduled job. It returns simt.Max if there are no events.
	Peek() simt.Time

	// Preview calculates and returns the event that will be returned on the next
	// call to Next. Do not call Preview.
	Preview() *event.Event

	// Next lets the scheduler return any assignments it has made.
	// The cluster clock will be advanced to the time of assignment,
	// so that the next time Next is called, a schedule may be made.
	//
	// An error may be returned when it applies to the returned
	// assignment. They therefore come togther. If there is no
	// scheduled assignment before until, (nil, nil) shall be
	// returned.
	//
	// No two assignments should complete at the same time.
	//
	// Although Next is not allowed to return an event that has the same time
	// as a past event, it is possible for s to be asked multiple times if
	// there is another event, because of observations, etc.
	//
	// It is also possible to receive calls to Next with: b < a. When a job
	// departs from a cluster, it is often in the past, because of the lazy
	// cluster.
	//
	// Pre-conditions
	//
	//  ∙ The cluster time must be past last returned assignment event.
	//  ∙ The until time is not less than a previous call.
	//
	// Post-conditions
	//
	//  ∙ Event properties:
	//      * must be before or equal to until
	//      * the assignment must be strictly after the previous returned
	//        (this would be nice, but it's not possible when you have
	//        multiple schedulers working at the same time.)
	//      * must have (type, data) tuple of:
	//          - (event.Assignment, Machine)
	//          - (event.SchedulingFailure, error)
	//          - (event.SchedulingConflict, error)
	//
	Next(until simt.Time) *event.Event
}

// A Microscheduler will not be accessed concurrently.
type Microscheduler interface {
	// Initialize initializes a scheduler with a cluster. It may be reinitialized at
	// any time.
	Initialize(c Cluster) error
	Finalize() error

	// Schedule schedules the job in the event and modifies the event
	// to reflect all changes in state.
	Schedule(e *event.Event)
}

// SchedulerQA {{{1

// SchedulerQA provides quality assurance for Scheduler instances.
type SchedulerQA struct {
	mt []simt.Time // machine times
	et simt.Time   // last event time
	at simt.Time   // last assignment time
	st simt.Time   // last submission time
	ut simt.Time   // last until time
	c  Cluster

	Scheduler
}

func init() { script.CreateFunc("qa|scheduler", NewSchedulerQA) }

func NewSchedulerQA(s Scheduler) Scheduler {
	if _, ok := s.(*SchedulerQA); ok {
		log.Warn("Ignoring scheduler QA nesting.")
		return s
	}
	return &SchedulerQA{
		at:        -1,
		et:        -1,
		ut:        0,
		Scheduler: s,
	}
}

func (s *SchedulerQA) Initialize(c Cluster) error {
	s.c = c
	s.mt = make([]simt.Time, c.Len())
	c.Iterate(func(i int, m Machine) error {
		s.mt[i] = m.Time()
		return nil
	})
	return s.Scheduler.Initialize(c)
}

func (s *SchedulerQA) Finalize() error {
	s.mt = nil
	s.c = nil
	return s.Scheduler.Finalize()
}

// Submit submits an event to the scheduler and performs quality assurance.
func (s *SchedulerQA) Submit(e *event.Event) error {
	const from = "Scheduler.Submit"

	// Pre-conditions
	//
	//  ∙ The cluster should have advanced to at least the time of the last assignment event.
	t := e.Time
	ct := s.c.Time()
	if ct < s.at {
		qa.Panic(from, "cluster time must be past last assignment event",
			qa.Expr("s.c.Time() ≥ prev event time: %v ≥ %v", ct, s.at),
			qa.Data("e", e),
		)
	}
	//  ∙ The cluster must not be in the future (past the event).
	if ct > t {
		qa.Panic(from, "cluster must not be in future",
			qa.Expr("s.c.Time() ≤ submitted event time: %v ≤ %v", ct, t),
			qa.Data("e", e),
		)
	}
	//  ∙ The time of the submitted event must be in the future.
	//  ∙ No two events can be submitted at the same time.
	//    Note: while we are guaranteed that two arrival events are not the same,
	//    it is possible for two different events to be submitted at the same time:
	//    SchedulingFailure and Arrival.
	if s.et >= t && s.st > t {
		qa.Panic(from, "submitted event must be after the last event",
			qa.Expr("s.et < Event.Time: %v < %v", s.et, t),
			qa.Expr("s.st ≤ Event.Time: %v ≤ %v", s.st, t),
			qa.Data("e", e),
		)
	}
	s.st = t

	// After submitted e, we should not access it anymore.
	err := s.Scheduler.Submit(e)
	if err != nil && err != ErrSchedulerQueueFull {
		qa.Panic(from, "scheduler returned unknown error",
			qa.Data("err", err),
			qa.Data("e", e),
		)
	}
	return err
}

// Next returns the next event from the scheduler and performs quality assurance.
//
func (s *SchedulerQA) Next(until simt.Time) *event.Event {
	const from = "Scheduler.Next"

	// Pre-conditions
	//
	//  ∙ the cluster time must be past last returned event
	if s.c.Time() < s.at {
		qa.Panic(from, "cluster time must be past last returned assignment event",
			qa.Expr("s.c.Time() ≥ last returned event time: %v ≥ %v", s.c.Time(), s.at),
		)
	}
	//  ∙ The until time is not less than a previous call.
	if until < s.ut {
		qa.Panic(from, "being passed an until less than a previous one",
			qa.Expr("until ≥ s.ut: %v ≥ %v", until, s.ut),
		)
	}
	s.ut = until

	e := s.Scheduler.Next(until)
	if e == nil {
		return nil
	}

	// Post-conditions
	//
	//  ∙ Event properties:
	//      * must be before or equal to until
	//      * the assignment must be strictly after the previous returned
	//        (this would be nice, but it's not possible when you have
	//        multiple schedulers working at the same time.)
	t := e.Time
	if t > until {
		qa.Panic(from, "event returned cannot be after until",
			qa.Expr("e.Time ≤ until: %v ≤ %v", t, until),
			qa.Data("e", e),
		)
	}
	if s.et > t {
		qa.Panic(from, "assignment must be strictly after the previous returned",
			qa.Expr("s.et <= e.Time: %v <= %v", s.et, t),
			qa.Data("e", e),
		)
	}
	s.et = t
	//      * must have (type, data) tuple of:
	//          - (event.Assignment, Machine)
	//          - (event.SchedulingFailure, error)
	//          - (event.SchedulingConflict, error)
	//
	y := e.Type
	switch y {
	case event.Assignment:
		m, ok := e.Data.(Machine)
		if !ok {
			qa.Panic(from, "expect e.Data to be of Machine type", qa.Data("e", e))
		}
		s.at = t

		// Make sure that no conflict assignments are made. The last scheduling time
		// of the event must be after the last assignment time to that machine.
		idx := m.IndexAt(s.c.Layer())
		mt := s.mt[idx]
		start := e.Job.SchedLastStart()
		if start < mt {
			qa.Panic(from, "assignment conflict not caught",
				qa.Expr("sched-start >= last-machine-assignment: %v >= %v", start, mt),
				qa.Data("e", e),
			)
		}
		s.mt[idx] = e.Job.SchedEnd
	case event.SchedulingConflict:
		// Check to make sure it really is the scheduling conflict it claims to be.
		// TODO
	default:
		if _, ok := e.Data.(error); !ok {
			qa.Panic(from, "expect e.Data to be of error type", qa.Data("e", e))
		}
	}

	return e
}

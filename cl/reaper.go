// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package cl

import (
	"fmt"
	"os"

	"gitlab.com/dromi/dromi/event"
	"gitlab.com/dromi/dromi/internal/qa"
	"gitlab.com/dromi/dromi/mdl"
	"gitlab.com/dromi/dromi/script"
	"gitlab.com/dromi/dromi/simt"
)

func init() {
	script.CreateFunc("exec.stingy", func() ReaperFunc { return NewStingyReaper })
	script.CreateFunc("exec.fair", func() ReaperFunc { return NewFairReaper })
	script.CreateFunc("exec.besteffort", func() ReaperFunc { return NewBestEffortReaper })
	// TODO: The following reapers could be made:
	//script.CreateFunc("exec.deadline", NewDeadlineReaper)
	//script.CreateFunc("exec.priority", NewPriorityReaper)
	//script.CreateFunc("exec.balance", NewBalanceReaper)
}

type ReaperFunc func(mdl.Hardware) Reaper

// A Reaper determines when jobs die.
type Reaper interface {
	// Finalize frees resources, including the event queue.
	Finalize() error

	Available() *mdl.Hardware
	Queue() event.Queue
	Next() simt.Time
	NextPtr() *simt.Time

	// CoreUtilization returns the true core utilization, as determined by the
	// reaper itself.  When all cores are being used, then 1 is returned, when
	// only half of them are used, then 0.5 is returned, and so on.
	CoreUtilization() float64

	// Insert updates each element in the queue,
	// and inserts e into the queue.
	Insert(e *event.Event)

	// Pop updates each element in the queue after popping the top event.
	// It is assumed that the machine time is that of the popped event.
	//
	// Note: calling Pop() on an empty queue will cause a panic.
	Pop() *event.Event
}

var DefaultReaperHeapSize = 2

// reaper {{{

type reaper struct {
	av   mdl.Hardware
	q    event.EventHeap
	next simt.Time // cache next here for faster access
	cu   float64

	// fix repairs the departure times of all the jobs in the queue
	// based on the current time t (which is used to calculate how
	// far done the job currently is).
	fix func(simt.Time)
}

func (r *reaper) Finalize() error {
	r.q.Reset()
	return nil
}

func (r *reaper) Available() *mdl.Hardware { return &r.av }
func (r *reaper) Queue() event.Queue       { return &r.q }
func (r *reaper) Next() simt.Time          { return r.next }

// Since Reaper is an interface, the Reaper.Next() function can't be
// inlined, and so it causes a lot of work to be done. If we get a
// pointer though, we don't have this problem.
func (r *reaper) NextPtr() *simt.Time      { return &r.next }
func (r *reaper) CoreUtilization() float64 { return r.cu }

func (r *reaper) Insert(e *event.Event) {
	t := e.Time
	dur, _ := e.Job.Pod.RuntimeWith(0)
	e.Time += simt.Time(dur)
	r.av.Reduce(&e.Job.Pod)
	r.q.Push(e)
	r.fix(t)
	r.next = r.q.NextTime()
}

func (r *reaper) Pop() *event.Event {
	e := r.q.Pop()
	r.av.Increase(&e.Job.Pod)
	r.fix(e.Time)
	r.next = r.q.NextTime()
	return e
}

// }}}

// qaReaper {{{

type qaReaper struct {
	reaper
	events []eventLog
}

func (r *qaReaper) Finalize() error {
	r.events = nil
	return r.reaper.Finalize()
}

func (r *qaReaper) Pop() *event.Event {
	e := r.reaper.Pop()
	r.register(e, popEvent)
	r.test(e)
	return e
}

func (r *qaReaper) Insert(e *event.Event) {
	r.register(e, insertEvent)
	r.test(e)
	r.reaper.Insert(e)
	r.test(r.reaper.q.Head())
}

func (r *qaReaper) register(v *event.Event, y reaperEvent) {
	r.events = append(r.events, eventLog{
		Time: v.Time,
		Type: y,

		JobID:     int8(v.Job.ID & 0xff), // last 8 bits of the id is enough for identification hopefully :-D
		NumEvents: int8(r.q.Len()),       // should be enough ...
	})
}

// Whenever anything happens with the reaper, have a look here.
func (r *qaReaper) test(v *event.Event) {
	// Make sure that the reaper still makes sense.
	// 1. The head of the queue should be correct (least event)
	// 2. There should not be any other jobs with a Time that is less than the
	//    time of the event.
	var fail bool
	qht := r.Next() // qht should be the smallest time
	for _, e := range r.q.View() {
		if e.Time < qht {
			qa.Stack("reaper", "reaper head element does not have the least time",
				qa.Expr("qht ≤ e.Time: %s ≤ %s", qht, e.Time),
				qa.Data("v", v),
				qa.Data("e", e),
				qa.Data("view", r.q.View()),
			)
			fail = true
		}

		if e.Time < v.Time {
			qa.Stack("reaper", "reaper contains elements that should have been popped",
				qa.Expr("v.Time ≤ e.Time: %s ≤ %s", v.Time, e.Time),
				qa.Data("v", v),
				qa.Data("e", e),
				qa.Data("view", r.q.View()),
			)
			fail = true
		}
	}

	if fail {
		fmt.Fprintln(os.Stderr, "Last events for this reaper:")
		for _, e := range r.events {
			fmt.Fprintln(os.Stderr, "\t", e)
		}
		panic("QUALITY ASSURANCE FAILED")
	}
}

type reaperEvent int8

const (
	insertEvent reaperEvent = iota
	popEvent
)

type eventLog struct {
	Time simt.Time
	Type reaperEvent

	JobID     int8
	NumEvents int8
}

func (e eventLog) String() string {
	var v string
	if e.Type == insertEvent {
		v = "Insert"
	} else {
		v = "Pop   "
	}

	return fmt.Sprintf("%s %s\t (id=%d, len=%d)", v, e.Time, e.JobID, e.NumEvents)
}

// }}}

// StingyReaper uses the minimum number of resources, effectively causing jobs
// to take the maximum possible time.  Jobs that reserved 0 cores will never
// come to completion.
func NewStingyReaper(hw mdl.Hardware) Reaper {
	r := &reaper{
		av:   hw,
		q:    *event.NewEventHeap(DefaultReaperHeapSize),
		next: simt.Time(simt.Max),
	}
	r.fix = func(simt.Time) {
		r.cu = 1.0 - float64(r.av.Cores)/float64(hw.Cores)
	}
	return r
}

// FairReaper tries to distribute the remaining cores to the jobs in the queue.
//
// If a job cannot make use of the extra cores, these cores are left unused.
// In that sense, it is fair, but it is not as useful.
func NewFairReaper(hw mdl.Hardware) Reaper {
	r := &reaper{
		av:   hw,
		q:    *event.NewEventHeap(DefaultReaperHeapSize),
		next: simt.Time(simt.Max),
	}
	r.fix = func(t simt.Time) {
		c := r.av.Cores
		n := r.q.Len()
		extra := c / float32(n)
		for _, e := range r.q.View() {
			d, x := e.Job.Pod.RuntimeWith(extra)
			e.Time = t + simt.Time(remaining(e, t)*float64(d))
			c -= x
		}
		r.cu = 1.0 - float64(c)/float64(hw.Cores)
		r.q.Init()
	}
	return r
}

// BestEffortReaper tries to distribute the remaining cores to the jobs in the queue.
//
// If a job cannot make use of the extra cores, these cores are given to other
// jobs, though not fairly.
func NewBestEffortReaper(hw mdl.Hardware) Reaper {
	r := &reaper{
		av:   hw,
		q:    *event.NewEventHeap(DefaultReaperHeapSize),
		next: simt.Time(simt.Max),
	}
	r.fix = func(t simt.Time) {
		c := r.av.Cores
		n := r.q.Len()
		for i, e := range r.q.View() {
			d, x := e.Job.Pod.RuntimeWith(c / float32(n-i))
			e.Time = t + simt.Time(remaining(e, t)*float64(d))
			c -= x
		}
		r.cu = 1.0 - float64(c)/float64(hw.Cores)
		r.q.Init()
	}
	return r
}

// remaining returns the remaining percentage of the task, given time t.
//
//  start                   t              end
//  |----------------------------------------|
//                          ^----------------^
//                                  rp
//
func remaining(e *event.Event, t simt.Time) float64 {
	return float64(e.Time-t) / float64(e.Time-e.Job.SchedEnd)
}

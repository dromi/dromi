// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package cl

import (
	"github.com/goulash/errs"

	"gitlab.com/dromi/dromi"
	"gitlab.com/dromi/dromi/event"
	"gitlab.com/dromi/dromi/mdl"
	"gitlab.com/dromi/dromi/script"
	"gitlab.com/dromi/dromi/simt"
)

func init() {
	script.CreateFunc("cl.new", NewCluster)
	script.CreateFunc("cl.uniform", func(hw mdl.Hardware, rf ReaperFunc, n int64) dromi.Cluster {
		return NewUniformCluster(hw, rf, int(n))
	})
	script.CreateFunc("cl.random", func(fn dromi.MachineFunc, n int64) dromi.Cluster {
		return NewRandomCluster(fn, int(n))
	})
}

func NewRandomCluster(fn dromi.MachineFunc, n int) dromi.Cluster {
	if fn == nil {
		panic("machine func cannot be nil")
	}
	if n <= 0 {
		panic("number of machines must be positive")
	}

	machines := make([]dromi.Machine, n)
	for i := range machines {
		machines[i] = fn()
	}
	return NewCluster(machines)
}

// NewUniformCluster returns a cluster with n machines that each have the same
// hardware and reaper.
func NewUniformCluster(hw mdl.Hardware, rf ReaperFunc, n int) dromi.Cluster {
	if rf == nil {
		panic("reaper func cannot be nil")
	}

	machines := make([]dromi.Machine, n)
	for i := range machines {
		machines[i] = NewMachine(hw, rf(hw))
	}
	return NewCluster(machines)
}

// Compile-time interface compliancy check.
var _ = dromi.Cluster(&Cluster{})

type Cluster struct {
	layer    int
	time     simt.Time
	machines []dromi.Machine
	exit     func(e *event.Event)
	labels   map[string]dromi.Cluster
}

func NewCluster(ms []dromi.Machine) *Cluster {
	return &Cluster{
		time:     0,
		machines: ms,
		labels:   make(map[string]dromi.Cluster),
	}
}

func (c *Cluster) Initialize(exitFn func(e *event.Event)) error {
	if exitFn == nil {
		c.exit = func(*event.Event) {}
	} else {
		c.exit = exitFn
	}

	ec := errs.NewCollector("cluster initialization failed")
	for i, m := range c.machines {
		if err := m.Initialize(c, i); err != nil {
			ec.Add(err)
		}
	}
	return ec.Error()
}

func (c *Cluster) Finalize() error {
	ec := errs.NewCollector("cluster finalization failed")
	for _, m := range c.machines {
		if err := m.Finalize(); err != nil {
			ec.Add(err)
		}
	}
	c.machines = nil
	c.labels = nil
	return ec.Error()
}

func (c *Cluster) Layer() int { return c.layer }

func (c *Cluster) Len() int { return len(c.machines) }

func (c *Cluster) Time() simt.Time { return c.time }

func (c *Cluster) Machines() []dromi.Machine { return c.machines }

func (c *Cluster) Get(i int) dromi.Machine { return c.machines[i] }

func (c *Cluster) Set(i int, m dromi.Machine) { c.machines[i] = m }

func (c *Cluster) Iterate(fn func(i int, m dromi.Machine) error) error {
	for i, m := range c.machines {
		err := fn(i, m)
		if err != nil {
			if err == dromi.ErrBreak {
				return nil
			}
			return err
		}
	}
	return nil
}

func (c *Cluster) Slice(a, b int) dromi.Cluster {
	slice := make([]dromi.Machine, b-a)
	for i, m := range c.machines[a:b] {
		slice[i] = dromi.NewMachineIndexer(m, i)
	}
	return &Cluster{
		layer:    c.layer + 1,
		time:     c.time,
		machines: slice,
		exit:     c.exit,
	}
}

func (c *Cluster) Subset(xs []int) dromi.Cluster {
	slice := make([]dromi.Machine, len(xs))
	for i := range slice {
		slice[i] = dromi.NewMachineIndexer(c.machines[xs[i]], i)
	}
	return &Cluster{
		layer:    c.layer + 1,
		time:     c.time,
		machines: slice,
		exit:     c.exit,
	}
}

func (c *Cluster) Labels() map[string]dromi.Cluster {
	return c.labels
}

func (c *Cluster) SetLabelSubset(key string, xs []int) {
	c.labels[key] = c.Subset(xs)
}

func (c *Cluster) Assign(e *event.Event) {
	m := e.Data.(dromi.Machine)
	c.time = e.Time
	// After inserting e into the machine, the event time is changed;
	// So the above assignment MUST occur before this statement.
	m.Insert(e)
}

func (c *Cluster) Depart(e *event.Event) {
	// The invariant held for the cluster is that all the machines are <= c.time.
	// Hence when an event departs, it probably is not after c.time.
	if e.Time > c.time {
		c.time = e.Time
	}
	c.exit(e)
}

// Update updates all machines in the cluster to the time t.
// There is no guarantee made that the machines are not already beyond t.
func (c *Cluster) Update(t simt.Time) {
	for _, m := range c.machines {
		m.Update(t)
	}
	c.time = t
}

func (c *Cluster) Finish() simt.Time {
	var max simt.Time
	for _, m := range c.machines {
		t := m.Finish()
		if t > max {
			max = t
		}
	}
	c.time = max
	return max
}

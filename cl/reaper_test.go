// Copyright (c) 2015, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package cl

import (
	"testing"

	"gitlab.com/dromi/dromi/event"
	"gitlab.com/dromi/dromi/mdl"
	"gitlab.com/dromi/dromi/simt"
)

// scheduleEntry is a simple type for testing reapers.
type scheduleEntry struct {
	Deadline string
	Runtime  string
	Priority int8
	MinCores float32
	MaxCores float32

	Arrival   string
	Departure string
}

type scheduleTest struct {
	NewReaper func(mdl.Hardware) Reaper
	Name      string
	Cores     float32
	Schedule  []scheduleEntry
}

var tests = []scheduleTest{
	// Stingy
	scheduleTest{
		NewStingyReaper,
		"StingyReaper",
		1.0,
		[]scheduleEntry{
			{"30s", "1s", 0, 0.2, 1.0, "5s", "10s"},
			{"30s", "10s", 0, 0.5, 1.0, "8s", "28s"},
			{"30s", "10s", 0, 0.5, 1.0, "15s", "35s"},
		},
	},
	scheduleTest{
		NewStingyReaper,
		"StingyReaper",
		1.0,
		[]scheduleEntry{
			{"30s", "1s", 0, 0.2, 2.0, "5s", "10s"},
			{"30s", "10s", 0, 0.5, 2.0, "8s", "28s"},
			{"30s", "10s", 0, 0.5, 2.0, "15s", "35s"},
		},
	},

	// Fair
	scheduleTest{
		NewFairReaper,
		"FairReaper",
		1.0,
		[]scheduleEntry{
			{"30s", "1s", 0, 0.1, 1.0, "5s", "6s"},
			{"30s", "10s", 0, 0.5, 1.0, "8s", "23s"},
			{"30s", "10s", 0, 0.5, 1.0, "13s", "28s"},
		},
	},
	scheduleTest{
		NewFairReaper,
		"FairReaper",
		1.0,
		[]scheduleEntry{
			{"30s", "1s", 0, 0.1, 2.0, "5s", "6s"},
			{"30s", "10s", 0, 0.5, 2.0, "8s", "23s"},
			{"30s", "10s", 0, 0.5, 2.0, "13s", "28s"},
		},
	},

	// Best-effort
	scheduleTest{
		NewBestEffortReaper,
		"BestEffortReaper",
		1.0,
		[]scheduleEntry{
			{"30s", "1s", 0, 0.1, 1.0, "5s", "6s"},
			{"30s", "10s", 0, 0.5, 1.0, "8s", "23s"},
			{"30s", "10s", 0, 0.5, 1.0, "13s", "28s"},
		},
	},
	scheduleTest{
		NewBestEffortReaper,
		"BestEffortReaper",
		1.0,
		[]scheduleEntry{
			{"30s", "1s", 0, 0.1, 2.0, "5s", "6s"},
			{"30s", "10s", 0, 0.5, 2.0, "8s", "23s"},
			{"30s", "10s", 0, 0.5, 2.0, "13s", "28s"},
		},
	},
}

const eps = 100 // nanoseconds from rounding-errors :-(

func TestReaper(z *testing.T) {
	for _, t := range tests {
		reaper := t.NewReaper(mdl.Hardware{Cores: t.Cores})
		depart := make([]simt.Time, len(t.Schedule))
		check := func(e *event.Event) {
			id := e.Data.(int)
			d := e.Time - depart[id]
			if d < 0 {
				d = -d
			}
			if d > 0 {
				z.Logf("Difference between actual and expected departure is %d nanoseconds.", d)
			}
			if d > eps {
				z.Errorf("From %s(%.1f): job %d departed %v, expecting %v", t.Name, t.Cores, id, e.Time, depart[id])
			}
		}
		for i, se := range t.Schedule {
			arrival, _ := simt.ParseTime(se.Arrival)
			depart[i], _ = simt.ParseTime(se.Departure)
			runtime, _ := simt.ParseDuration(se.Runtime)
			deadline, _ := simt.ParseTime(se.Deadline)
			e := &event.Event{
				Time: arrival,
				Type: event.Assignment,
				Job: &mdl.Job{
					Arrival:  arrival,
					SchedEnd: arrival,
					Pod: mdl.Pod{
						Deadline: deadline,
						Runtime:  runtime,
						Priority: se.Priority,
						MinCores: se.MinCores,
						MaxCores: se.MaxCores,
					},
				},
				Data: i,
			}

			// Insert the reaper:
			for reaper.Next() < e.Time {
				check(reaper.Pop())
			}
			reaper.Insert(e)
		}

		for reaper.Queue().Len() > 0 {
			check(reaper.Pop())
		}
	}
}

// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package cl

import (
	"fmt"
	"math/rand"

	"gitlab.com/dromi/dromi"
	"gitlab.com/dromi/dromi/event"
	"gitlab.com/dromi/dromi/mdl"
	"gitlab.com/dromi/dromi/script"
	"gitlab.com/dromi/dromi/simt"
)

func init() {
	script.CreateFunc("mc.new", NewMachine)
	script.CreateFunc("mc.sampler", NewHardwareSampler)
}

func NewHardwareSampler(s rand.Source, rf ReaperFunc, xs []mdl.Hardware) dromi.MachineFunc {
	r := rand.New(s)
	n := len(xs)
	return func() dromi.Machine {
		h := xs[r.Intn(n)]
		return NewMachine(h, rf(h))
	}
}

// Compile-time interface compliancy check.
var _ = dromi.Machine(new(Machine))

// Upon adding a machine to a cluster, the cluster gives the machine its ID.
type Machine struct {
	c   dromi.Cluster
	idx int

	t  simt.Time
	n  int // cache the number of jobs for performance
	ob dromi.MachineObservers

	hw   mdl.Hardware
	re   Reaper
	next *simt.Time
}

func NewMachine(hw mdl.Hardware, r Reaper) *Machine {
	return &Machine{
		hw: hw,
		re: r,
	}
}

func (m *Machine) Initialize(c dromi.Cluster, idx int) error {
	m.c = c
	m.idx = idx
	m.next = m.re.NextPtr()
	return nil
}

func (m *Machine) Finalize() error {
	err := m.re.Finalize()
	m.c = nil
	m.ob = nil
	m.re = nil
	return err
}

func (m *Machine) String() string { return fmt.Sprintf("Machine{%d}", m.idx) }
func (m *Machine) Layer() int     { return 0 }
func (m *Machine) Index() int     { return m.idx }
func (m *Machine) IndexAt(layer int) int {
	if layer != 0 {
		panic("machine can only return index for layer 0")
	}
	return m.idx
}
func (m *Machine) Unwrap() dromi.Machine { return nil }
func (m *Machine) UnwrapTo(layer int) dromi.Machine {
	if layer != 0 {
		panic("machine can not be unwrapped any further")
	}
	return m
}

func (m *Machine) Hardware() *mdl.Hardware               { return &m.hw }
func (m *Machine) Available() *mdl.Hardware              { return m.re.Available() }
func (m *Machine) Utilization() *mdl.HardwareUtilization { return mdl.Fraction(&m.hw, m.re.Available()) }
func (m *Machine) NJobs() int                            { return m.n }
func (m *Machine) Jobs() []*event.Event                  { return m.re.Queue().View() }

func (m *Machine) RegisterObserver(o dromi.MachineObserver) { m.ob = append(m.ob, o) }

func (m *Machine) Insert(e *event.Event) {
	m.Update(e.Time)
	e.Type = event.Departure
	m.ob.Insert(e)
	m.re.Insert(e) // give up ownership
	m.n++
}

func (m *Machine) Time() simt.Time { return m.t }

func (m *Machine) Update(t simt.Time) {
	for *m.next < t {
		m.pop()
	}
	m.t = t
}

func (m *Machine) Finish() simt.Time {
	q := m.re.Queue()
	for q.Len() > 0 {
		e := m.pop()
		m.t = e.Time
	}
	return m.t
}

// Note: pop should only be called by Machine methods.
func (m *Machine) pop() *event.Event {
	e := m.re.Pop() // get ownership
	m.n--
	e.Job.Departure = e.Time
	m.ob.Remove(e)
	m.c.Depart(e) // don't we theoretically give up ownership here?
	return e
}

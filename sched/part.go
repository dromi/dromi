// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package sched

import (
	"gitlab.com/dromi/dromi"
	"gitlab.com/dromi/dromi/script"
)

func init() {
	script.CreateFunc("part.equal", func() PartitionFunc { return EqualPartitioner })
}

type PartitionFunc func(c dromi.Cluster, n int) []dromi.Cluster

func EqualPartitioner(c dromi.Cluster, n int) []dromi.Cluster {
	m := c.Len()
	if m < n {
		panic("cannot partition cluster")
	}

	each := m / n
	cls := make([]dromi.Cluster, n)
	for i := range cls[:n-1] {
		cls[i] = c.Slice(i*each, (i+1)*each)
	}
	cls[n-1] = c.Slice((n-1)*each, m)
	return cls
}

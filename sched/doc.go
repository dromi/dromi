// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

/*
Package sched provides cluster schedulers.

DromiScript

A simulation requires a scheduler to assign jobs to machines.
The scheduler is built as a framework employing simpler schedulers and a queue.

It is recommended to use one of the following queues:

    eq.cap
    eq.linkedlist
    eq.arraylist
    eq.stack
    eq.heapByPriority
    eq.heapByRuntime
    eq.heapByTime
    eq.heapByEvent

Schedulers that are available are:

    sched.first :: ("delay" simt.Duration) -> ("overhead" simt.Duration) => Scheduler
    sched.randn :: (rand.Source) -> ("n" int64) -> ("delay" simt.Duration) -> ("overhead" simt.Duration) => Scheduler
    sched.shufn :: (rand.Source) -> ("n" int64) -> ("delay" simt.Duration) -> ("overhead" simt.Duration) => Scheduler

The following Scheduler take other Scheduler(s) and make something new of them.

    sched.rr :: ...Scheduler => Scheduler
    sched.rs :: (dist.Discrete) -> ...Scheduler => Scheduler

Framework "sf.monolith" can only schedule one job at a time.

    sf.monolith :: event.Queue -> Scheduler => dromi.Scheduler

Framework "sf.partition" partitions the cluster and gives
each partition to a scheduler, which can work parallel. This
is possible

    sf.partition :: PartitionInitFunc -> PartitionScheduleFunc -> ...dromi.Scheduler => dromi.Scheduler

    ; Evenly partition the cluster among all the schedulers.
    (sf.partition
        (pif.even)
        (psf.dist (stat.stairs 2 4 10))
        (sf.monolith (eq.arraylist 10) (sched.first (dur "1ms") (dur "500us")))
        (sf.monolith (eq.arraylist 10) (sched.randn RAND_SOURCE, 10, (dur "1ms") (dur "500us)))
        (sf.monolith (eq.arraylist 10) (sched.shufn RAND_SOURCE, 10, (dur "1ms") (dur "500us)))
    )

*/
package sched

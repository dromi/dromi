// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package sched

import (
	"github.com/goulash/errs"
	"gitlab.com/dromi/dromi"
	"gitlab.com/dromi/dromi/event"
	"gitlab.com/dromi/dromi/mux"
	"gitlab.com/dromi/dromi/script"
	"gitlab.com/dromi/dromi/simt"
)

func init() {
	script.CreateFunc("sf.single", NewSingle)
	script.CreateFunc("sf.monolith", NewMonolith)
	script.CreateFunc("sf.partition", NewPartition)
	script.CreateFunc("sf.concurrent", NewConcurrent)
	script.CreateFunc("sf.sharedq-partition", NewQueueSharingPartition)
	script.CreateFunc("sf.sharedq-concurrent", NewQueueSharingConcurrent)
	script.CreateFunc("sf.blocking-partition", NewBlockingPartition)
	script.CreateFunc("sf.blocking-concurrent", NewBlockingConcurrent)
}

// Single {{{1

func NewSingle(s dromi.Microscheduler) dromi.Scheduler {
	return &single{s, nil}
}

// Compile-time interface compliancy check.
var _ = dromi.Scheduler(&single{})

type single struct {
	sched dromi.Microscheduler
	done  *event.Event
}

func (sf *single) Initialize(c dromi.Cluster) error {
	return sf.sched.Initialize(c)
}

func (sf *single) Finalize() error {
	err := sf.sched.Finalize()
	sf.done = nil
	sf.sched = nil
	return err
}

func (sf *single) Len() int {
	if sf.done == nil {
		return 0
	} else {
		return 1
	}
}

func (sf *single) QueueLen() int {
	return 0
}

func (sf *single) Submit(e *event.Event) error {
	if sf.done != nil {
		return dromi.ErrSchedulerQueueFull
	}
	sf.sched.Schedule(e)
	sf.done = e
	return nil
}

func (sf *single) Peek() simt.Time {
	if sf.done == nil {
		return simt.Time(simt.Max)
	}
	return sf.done.Time
}

func (sf *single) Preview() *event.Event {
	return sf.done
}

func (sf *single) Next(until simt.Time) *event.Event {
	if sf.done == nil {
		return nil
	}
	if sf.done.Time > until {
		return nil
	}
	done := sf.done
	sf.done = nil
	return done
}

// Monolith {{{1

// Monolith is a single monolithic scheduler. All incoming jobs pass through
// a single scheduler, one after another.
//
// The queue and the scheduler are customizable; it is also possible for multiple
// Monoliths to share the same queue. If that is the case however, it is necessary
// for the chooser function to always pick the scheduler with the minimum queue.
func NewMonolith(q event.Queue, s dromi.Microscheduler) dromi.Scheduler {
	if q == nil || s == nil {
		return nil
	}

	return &monolith{
		queue: q,
		sched: s,
	}
}

// Compile-time interface compliancy check.
var _ = dromi.Scheduler(&monolith{})

type monolith struct {
	sched dromi.Microscheduler
	queue event.Queue
	done  *event.Event
	last  simt.Time
}

func (sf *monolith) Initialize(c dromi.Cluster) error {
	return sf.sched.Initialize(c)
}

func (sf *monolith) Finalize() error {
	err := sf.sched.Finalize()
	sf.queue.Reset()
	sf.done = nil
	sf.sched = nil
	sf.queue = nil
	return err
}

func (sf *monolith) Len() int {
	size := sf.queue.Len()
	if sf.done != nil {
		size++
	}
	return size
}

func (sf *monolith) QueueLen() int {
	return sf.queue.Len()
}

func (sf *monolith) Submit(e *event.Event) error {
	// Because the queue is volatile, something could have been inserted
	// in the mean time.
	if sf.done == nil && sf.queue.Len() == 0 {
		sf.sched.Schedule(e)
		sf.done = e
		sf.last = e.Time
	} else {
		ok := sf.queue.Push(e)
		if !ok {
			return dromi.ErrSchedulerQueueFull
		}
	}
	return nil
}

func (sf *monolith) Peek() simt.Time {
	if sf.done != nil {
		return sf.done.Time
	} else if e := sf.queue.Head(); e != nil {
		if e.Time < sf.last {
			return sf.last
		} else {
			return e.Time
		}
	}
	return simt.Time(simt.Max)
}

func (sf *monolith) Preview() *event.Event {
	if sf.done != nil {
		return sf.done
	}

	e := sf.queue.Pop()
	if e == nil {
		return nil
	}

	if e.Time < sf.last {
		e.Time = sf.last
	}
	sf.sched.Schedule(e)
	sf.done = e
	sf.last = e.Time
	return sf.done
}

func (sf *monolith) Next(until simt.Time) *event.Event {
	// We are assuming that until != simt.Max.
	// If there are no events to schedule or return, sf.Peek() returns
	// simt.Max, which is greater than until, causing nil to be returned.
	if sf.Peek() > until {
		return nil
	}

	// We don't know whether the peeked time was for a scheduled item
	// or not, so get the next scheduled item (this will schedule it
	// if it hasn't already been).
	done := sf.Preview()
	if done.Time > until {
		return nil
	}

	sf.done = nil
	return done
}

// Partition {{{1

// Partition lets multiple schedulers run on disjoint cluster partition.
//
// Note: no check for scheduling conflicts is made, it is a logic error
// to pass in a PartitionFunc that does not create a disjoint
// partition. The errors be on your own head!
func NewPartition(pf PartitionFunc, mf mux.MultiplexFunc, sched ...dromi.Scheduler) dromi.Scheduler {
	if len(sched) == 0 {
		panic("there must be at least one scheduler in partition")
	}
	return &partition{
		sched: sched,
		pf:    pf,
		mf:    mf,
	}
}

// Compile-time interface compliancy check.
var _ = dromi.Scheduler(&partition{})

type partition struct {
	sched []dromi.Scheduler
	pf    PartitionFunc
	mf    mux.MultiplexFunc
	size  int
}

func (sf *partition) Initialize(cluster dromi.Cluster) error {
	ec := errs.NewCollector("partition scheduler initialization failed")
	for i, c := range sf.pf(cluster, len(sf.sched)) {
		ec.Collect(sf.sched[i].Initialize(c))
	}
	return ec.Error()
}

func (sf *partition) Finalize() error {
	ec := errs.NewCollector("partition scheduler finalization failed")
	for _, s := range sf.sched {
		ec.Collect(s.Finalize())
	}
	sf.sched = nil
	sf.pf = nil
	sf.mf = nil
	return ec.Error()
}

func (sf *partition) Len() int {
	return sf.size
}

func (sf *partition) QueueLen() int {
	var n int
	for _, s := range sf.sched {
		n += s.QueueLen()
	}
	return n
}

func (sf *partition) Submit(e *event.Event) error {
	idx := sf.mf(e)
	if err := sf.sched[idx].Submit(e); err != nil {
		return err
	}
	sf.size++
	return nil
}

func (sf *partition) Peek() simt.Time {
	_, min := sf.peek()
	return min
}

// peek returns the index to the scheduler with the minimum time.
// If there are no events, then -1 is returned.
func (sf *partition) peek() (int, simt.Time) {
	idx, min := -1, simt.Time(simt.Max)
	for i, s := range sf.sched {
		t := s.Peek()
		if t < min {
			min = t
			idx = i
		}
	}
	return idx, min
}

func (sf *partition) Preview() *event.Event {
	// Keep previewing events until the one with the minimum time has
	// stabilized. If there are no events, then idx == i right away
	// and e = nil is returned.
	var e *event.Event
	idx := -1
	i, _ := sf.peek()
	for idx != i {
		e = sf.sched[i].Preview()
		idx = i
		i, _ = sf.peek()
	}
	return e
}

func (sf *partition) Next(until simt.Time) *event.Event {
	// We cannot use sf.Preview directly because it could cause Preview
	// to be called for schedulers that already have a Peek time of greater
	// than until.
	//
	// For example, we have elements: a, b, c; with a < until and b > until.
	// After a.Preview() = A is called, we may have the ordering: b, A, c;
	// all of which would be greater than until. The function sf.Preview wants
	// to return the minimum element, so it would then call b.Preview().
	//
	// Hence here we need to make sure that min is always <= until before
	// calling Preview for that scheduler.
	idx := -1
	i, min := sf.peek()
	for idx != i && min <= until {
		sf.sched[i].Preview()
		idx = i
		i, min = sf.peek()
	}
	if min > until {
		return nil
	}

	sf.size--
	return sf.sched[i].Next(until)
}

// Concurrent {{{1

// Concurrent lets multiple schedulers run concurrently over the same cluster
// space. This can lead to scheduling conflicts, which occur when a job
// is scheduled to a machine and the machine has had another assignment
// since the first job was scheduled.
//
// If a SchedulerChooserFunc returns -1 as the index for a scheduler, then
// the scheduler with the least amount of jobs in the queue is chosen.
//
// Conflicts
//
// In the following example, job A arrives first and is scheduled first and
// assigned to machine 1. Job B arrives after job A and completes afterwards,
// but when it comes to a conflict because the information it had about machine
// 1 at the start (*) is outdated, since then job a has been assigned (|).
//
//  Job A:1 ...-------*-------|=========... ✓
//  Job B:1 ...----------*--------|xxxxx... ✗
//
// When different schedulers take differing amounts of time, it can also look
// like this:
//
//  Job A:1 ...-------*-----------|xxxxx... ✗
//  Job B:1 ...-----------*----|========... ✓
//
// It could even look like this, where job B completed already, so that the
// machine could actually be in the same state as when A looked at it.
//
//  Job A:1 ...-------*-----------|xxxxx... ✗
//  Job B:1 ...---------*--|===|--------... ✓
//
// In each of the above cases, the essential component is that something else
// happened between the start (*) and the end of scheduling (|).
//
//  Job X:i ...-------*-------x----|xxxx... ✗
//
func NewConcurrent(mf mux.MultiplexFunc, sched ...dromi.Scheduler) dromi.Scheduler {
	if len(sched) == 0 {
		panic("there must be at least one scheduler in partition")
	}
	return &concurrent{
		sched: sched,
		mf:    mf,
	}
}

// Compile-time interface compliancy check.
var _ = dromi.Scheduler(&concurrent{})

type jobtime struct {
	Job  int64
	Time simt.Time
}

type concurrent struct {
	// times keeps track of the last event times for each machine in the cluster.
	// Whenever we return an event via Next or Preview, we update times.
	times []jobtime
	layer int

	sched []dromi.Scheduler
	mf    mux.MultiplexFunc
	size  int
}

func (sf *concurrent) Initialize(cluster dromi.Cluster) error {
	sf.times = make([]jobtime, cluster.Len())
	sf.layer = cluster.Layer()
	cluster.Iterate(func(i int, m dromi.Machine) error {
		sf.times[i] = jobtime{-1, m.Time()}
		return nil
	})
	ec := errs.NewCollector("concurrent scheduler initialization failed")
	for _, s := range sf.sched {
		ec.Collect(s.Initialize(cluster))
	}
	return ec.Error()
}

func (sf *concurrent) Finalize() error {
	ec := errs.NewCollector("concurrent scheduler finalization failed")
	for _, s := range sf.sched {
		ec.Collect(s.Finalize())
	}
	sf.times = nil
	sf.sched = nil
	sf.mf = nil
	return ec.Error()
}

func (sf *concurrent) Len() int {
	return sf.size
}

func (sf *concurrent) QueueLen() int {
	var n int
	for _, s := range sf.sched {
		n += s.QueueLen()
	}
	return n
}

func (sf *concurrent) Submit(e *event.Event) error {
	idx := sf.mf(e)
	if err := sf.sched[idx].Submit(e); err != nil {
		return err
	}
	sf.size++
	return nil
}

func (sf *concurrent) Peek() simt.Time {
	_, min := sf.peek()
	return min
}

// peek returns the index to the scheduler with the minimum time.
// If there are no events, then -1 is returned.
func (sf *concurrent) peek() (int, simt.Time) {
	idx, min := -1, simt.Time(simt.Max)
	for i, s := range sf.sched {
		t := s.Peek()
		if t < min {
			min = t
			idx = i
		}
	}
	return idx, min
}

func (sf *concurrent) Preview() *event.Event {
	// Keep previewing events until the one with the minimum time has
	// stabilized. If there are no events, then idx == i right away
	// and e = nil is returned.
	var e *event.Event
	idx := -1
	i, _ := sf.peek()
	for idx != i {
		e = sf.previewScheduler(i)
		idx = i
		i, _ = sf.peek()
	}
	return e
}

// previewScheduler calls preview for given scheduler index and checks
// for conflicts. This is what should be used instead of sf.sched[i].Preview()
//
// This is also the only place that sf schedules the events.
func (sf *concurrent) previewScheduler(i int) *event.Event {
	e := sf.sched[i].Preview()
	if e.Type != event.Assignment {
		// Conflicts only occur on assignments, so just return it.
		return e
	}

	m, _ := e.Data.(dromi.Machine)
	mi := m.IndexAt(sf.layer)
	jt := sf.times[mi]
	if jt.Job == e.Job.ID {
		return e
	}
	mt := jt.Time
	st := e.Job.SchedLastStart()
	// When the start of a job st is less (or equal to) than the last assignment to that
	// machine, then we have a conflict:
	//
	//  Job A: ...--------------*------------+----...
	//  Job B: ...-----------------*----+---------...
	//
	// In this case, job B has priority, because it's done first (+), even though
	// it started afterwards (*). If that happens, we rewrite the event as a
	// conflict. Even though this is a preview, events returned from previews
	// are required to not change anymore, so it is safe to modify it. Also,
	// any other concurrent schedulers will not go down this path again, because
	// the type has been changed from it's original assignment type.
	if st <= mt {
		e.Type = event.SchedulingConflict
		e.Data = &dromi.SchedulingConflictError{
			Layer:           sf.layer,
			Machine:         m,
			LastAssignment:  mt,
			SchedulingStart: st,
		}
		e.Job.Conflicts++
	} else {
		// Since there was no conflict, write this in the books as a modification time,
		// even though it hasn't programmatically happened yet; it will.
		sf.times[mi] = jobtime{e.Job.ID, e.Job.SchedEnd}
	}
	return e
}

func (sf *concurrent) Next(until simt.Time) *event.Event {
	// We cannot use sf.Preview directly because it could cause Preview
	// to be called for schedulers that already have a Peek time of greater
	// than until.
	//
	// For example, we have elements: a, b, c; with a < until and b > until.
	// After a.Preview() = A is called, we may have the ordering: b, A, c;
	// all of which would be greater than until. The function sf.Preview wants
	// to return the minimum element, so it would then call b.Preview().
	//
	// Hence here we need to make sure that min is always <= until before
	// calling Preview for that scheduler.
	idx := -1
	i, min := sf.peek()
	for idx != i && min <= until {
		_ = sf.previewScheduler(i)
		idx = i
		i, min = sf.peek()
	}
	if min > until {
		return nil
	}

	sf.size--
	return sf.sched[i].Next(until)
}

// QueueSharing {{{1

func NewQueueSharing(q event.Queue, sched ...dromi.Microscheduler) []dromi.Scheduler {
	if q == nil || len(sched) == 0 {
		return nil
	}

	xs := make([]dromi.Scheduler, len(sched))
	for i, s := range sched {
		xs[i] = NewMonolith(q, s)
	}
	return xs
}

func NewQueueSharingPartition(pf PartitionFunc, q event.Queue, sched ...dromi.Microscheduler) dromi.Scheduler {
	if pf == nil || q == nil || len(sched) == 0 {
		return nil
	}

	xs := NewQueueSharing(q, sched...)
	return NewPartition(pf, mux.NewMinimumMux(xs...), xs...)
}

func NewQueueSharingConcurrent(q event.Queue, sched ...dromi.Microscheduler) dromi.Scheduler {
	if q == nil || len(sched) == 0 {
		return nil
	}

	xs := NewQueueSharing(q, sched...)
	return NewConcurrent(mux.NewMinimumMux(xs...), xs...)
}

// Blocking {{{1

func NewBlocking(sched ...dromi.Microscheduler) []dromi.Scheduler {
	xs := make([]dromi.Scheduler, len(sched))
	for i, s := range sched {
		xs[i] = NewSingle(s)
	}
	return xs
}

func NewBlockingPartition(pf PartitionFunc, sched ...dromi.Microscheduler) dromi.Scheduler {
	if len(sched) == 0 {
		return nil
	}

	xs := NewBlocking(sched...)
	return NewPartition(pf, mux.NewMinimumMux(xs...), xs...)
}

func NewBlockingConcurrent(sched ...dromi.Microscheduler) dromi.Scheduler {
	if len(sched) == 0 {
		return nil
	}

	xs := NewBlocking(sched...)
	return NewConcurrent(mux.NewMinimumMux(xs...), xs...)
}

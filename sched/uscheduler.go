// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

// Package sched provides various cluster schedulers.
package sched

import (
	"math/rand"

	"github.com/goulash/errs"

	"gitlab.com/dromi/dromi"
	"gitlab.com/dromi/dromi/event"
	"gitlab.com/dromi/dromi/internal"
	"gitlab.com/dromi/dromi/mux"
	"gitlab.com/dromi/dromi/script"
	"gitlab.com/dromi/dromi/simt"
)

func init() {
	script.CreateFunc("sched.first", func(delay, overhead simt.Duration) dromi.Microscheduler {
		return NewDeterministicMicroscheduler(NewFirst(), delay, overhead)
	})
	script.CreateFunc("sched.randn", func(s rand.Source, n int64, delay, overhead simt.Duration) dromi.Microscheduler {
		return NewDeterministicMicroscheduler(NewRandomFirstN(s, int(n)), delay, overhead)
	})
	script.CreateFunc("sched.shufn", func(s rand.Source, n int64, delay, overhead simt.Duration) dromi.Microscheduler {
		return NewDeterministicMicroscheduler(NewShuffleFirstN(s, int(n)), delay, overhead)
	})
	script.CreateFunc("sched|mux", NewMicroschedulerMultiplexer)
	script.CreateFunc("sched|retime", NewMicroschedulerRetimer)

}

// MicroschedulerFunc: First, RandomN, ShuffleN {{{1

type MicroschedulerFunc func(dromi.Cluster, *event.Event) (int, interface{})

// First is a scheduler that takes the first machine where the job fits.
//
// It starts at the beginning of the cluster, and moves to the end.
// If no machine is found, dromi.ErrSchedulingImpossible is returned.
//
// Note: at the point of scheduling, a snapshot of the cluster is taken.
// Thus time in the cluster across scheduling should remain at e.Time.
func NewFirst() MicroschedulerFunc {
	return first
}

func first(c dromi.Cluster, e *event.Event) (n int, v interface{}) {
	n, v = c.Len(), dromi.ErrSchedulingImpossible
	c.Iterate(func(i int, a dromi.Machine) error {
		if dromi.MachineSupports(a, e) {
			n, v = i+1, a
			return dromi.ErrBreak
		}
		return nil
	})
	return
}

// RandomFirstN is a scheduler that assigns the job to the first fitting machine
// it randomly picks (with putback).
//
// It picks randomly n times, putting the machine back in the bag. The
// scheduling time is : 1ms overhead + 500μs per machine. If no machine can be
// found, SchedulingFailure event is returned with e.Data = dromi.ErrSchedulingFailed.
// Because the search is not exhaustive, we cannot categorically say that there
// is no such machine.
//
// Note: at the point of scheduling, a snapshot of the cluster is taken.
// Thus time in the cluster across scheduling should remain at e.Time.
func NewRandomFirstN(s rand.Source, n int) MicroschedulerFunc {
	r := rand.New(s)
	return func(c dromi.Cluster, e *event.Event) (int, interface{}) {
		m := c.Len()
		machines := c.Machines()
		for i := 1; i <= n; i++ {
			a := machines[r.Intn(m)]
			if dromi.MachineSupports(a, e) {
				return i, a
			}
		}
		return n, dromi.ErrSchedulingFailed
	}
}

// ShuffleFirstN is a scheduler that assigns the job to the first fitting machine
// it randomly picks (without putback).
//
// It picks randomly n times, not putting the machine back in the bag. The scheduling
// time is similar to RandomFirstN: 2ms overhead + 500μs per machine. If no machine
// can be found, SchedulingFailure event is returned with e.Data = dromi.ErrSchedulingFailed.
// Because the search is not exhaustive, we cannot categorically say that there is
// no such machine.
//
// Note: at the point of scheduling, a snapshot of the cluster is taken.
// Thus time in the cluster across scheduling should remain at e.Time.
func NewShuffleFirstN(s rand.Source, n int) MicroschedulerFunc {
	return func(c dromi.Cluster, e *event.Event) (int, interface{}) {
		m := c.Len()
		xs := internal.List0N(m)
		internal.ShuffleFirstN(s, n, xs)
		machines := c.Machines()
		for i, idx := range xs[:n] {
			// Note: the following c.Get(idx) call may panic if n is larger than m.
			a := machines[idx]
			if dromi.MachineSupports(a, e) {
				return i + 1, a
			}
		}
		return n, dromi.ErrSchedulingFailed
	}
}

// Deterministic microscheduler {{{1

// DeterministicMicroscheduler implements the Microscheduler interface and schedules jobs
// according to a simpler MicroschedulerFunc.
//
// The delay and overhead of scheduling can also be configured separately.
type DeterministicMicroscheduler struct {
	c dromi.Cluster

	ScheduleFunc MicroschedulerFunc
	Delay        simt.Duration // initial scheduling delay
	Overhead     simt.Duration // overhead per machine processed
}

func NewDeterministicMicroscheduler(sf MicroschedulerFunc, delay, overhead simt.Duration) *DeterministicMicroscheduler {
	return &DeterministicMicroscheduler{
		ScheduleFunc: sf,
		Delay:        delay,
		Overhead:     overhead,
	}
}

func (s *DeterministicMicroscheduler) Initialize(c dromi.Cluster) error {
	s.c = c
	return nil
}

func (s *DeterministicMicroscheduler) Finalize() error {
	s.c = nil
	return nil
}

func (s *DeterministicMicroscheduler) Schedule(e *event.Event) {
	if e.Type == event.Assignment || e.Type == event.Departure {
		panic("trying to schedule an already scheduled event")
	}

	n, v := s.ScheduleFunc(s.c, e)
	z := s.Delay + s.Overhead*simt.Duration(n)
	e.Job.AddSched(e.Time, z)
	e.Time = e.Job.SchedEnd
	e.Data = v
	if _, ok := v.(dromi.Machine); ok {
		e.Type = event.Assignment
	} else {
		e.Type = event.SchedulingFailure
	}
}

// Microscheduler multiplexer {{{1

func NewMicroschedulerMultiplexer(mf mux.MultiplexFunc, sched ...dromi.Microscheduler) dromi.Microscheduler {
	return &multiplexer{mf, sched}
}

type multiplexer struct {
	mf    mux.MultiplexFunc
	sched []dromi.Microscheduler
}

func (s *multiplexer) Initialize(c dromi.Cluster) error {
	ec := errs.NewCollector("microscheduler multiplexer initialization failed")
	for _, o := range s.sched {
		ec.Collect(o.Initialize(c))
	}
	return ec.Error()
}

func (s *multiplexer) Finalize() error {
	ec := errs.NewCollector("microscheduler multiplexer finalization failed")
	for _, s := range s.sched {
		ec.Collect(s.Finalize())
	}
	s.mf = nil
	s.sched = nil
	return ec.Error()
}

func (s *multiplexer) Schedule(e *event.Event) {
	// Note: the following call could panic if you passed in a distribution
	// that is not tailored to the number of schedulers.
	s.sched[s.mf(e)].Schedule(e)
}

// Microscheduler retimer {{{1

func NewMicroschedulerRetimer(d simt.Distribution, s dromi.Microscheduler) dromi.Microscheduler {
	return &retimer{
		Microscheduler: s,
		d:              d,
	}
}

// Compile-time interface compliancy check.
var _ = dromi.Microscheduler(&retimer{})

type retimer struct {
	dromi.Microscheduler
	d simt.Distribution
}

func (s *retimer) Schedule(e *event.Event) {
	s.Microscheduler.Schedule(e)

	// Replace however long the previous scheduling took with
	// whatever we get out of our distribution.
	d := s.d()
	n := e.Job.NumSchedulingAttempts() - 1
	e.Job.SchedDur[n] = d
	e.Job.SchedEnd = e.Job.Sched[n] + simt.Time(d)
	e.Time = e.Job.SchedEnd
}

// Copyright (c) 2015, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package dromi

import (
	"fmt"
	"net/http"
	"text/template"

	"gitlab.com/dromi/dromi/webutil"
)

func (m *Master) slaveHandler(s *Slave) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		m.mutex.RLock()
		webutil.ServeStruct(w, r, s.Serve())
		m.mutex.RUnlock()
	})
}

func (m *Master) slaveFnHandler(fn func() interface{}) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		m.mutex.RLock()
		webutil.ServeStruct(w, r, fn())
		m.mutex.RUnlock()
	})
}

func (m *Master) etaHandler(w http.ResponseWriter, r *http.Request) {
	webutil.ServeStruct(w, r, m.ETA())
}

func (m *Master) endpointsHandler(w http.ResponseWriter, r *http.Request) {
	for _, ep := range m.Endpoints() {
		fmt.Fprintln(w, ep)
	}
}

func (m *Master) rootHandler(w http.ResponseWriter, r *http.Request) {
	RootTmpl.Execute(w, m)
}

var Tmpl = template.New("-")

var CountersTmpl = template.Must(Tmpl.New("counters").Parse(`
    arrivals:       {{.Arrivals}}
    departures:     {{.Departures}}
    submissions:    {{.Submissions}}
    scheduling-
        failures:   {{.SchedulingFailures}}
        conflicts:  {{.SchedulingConflicts}}
        errors:     {{.SchedulingErrors}}
    rejections:     {{.Rejections}}
    assignments:    {{.Assignments}}
        errors:     {{.AssignmentErrors}}
    evictions:      {{.Evictions}}
    observations:   {{.Observations}}
    errors:         {{.Errors}}
    duration:       {{.Time}}
    events:         {{.Events}}
`))

var PropertiesTmpl = template.Must(Tmpl.New("properties").Parse(`
    quality assurance:  {{.QualityAssurance}}
    max sched retries:  {{.MaxSchedRetries}}
    lazy cluster:       {{.LazyCluster}}
    job overhead:       {{.JobOverhead}}
    blocking overhead:  {{.BlockingOverhead}}{{if .Cluster}}
    ---
    cluster size:       {{.Cluster.Len}}
    cluster time:       {{.Cluster.Time}}{{end}}
`))

var RootTmpl = template.Must(Tmpl.New("root").Parse(`<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Dromi</title>
    <style>
        body {
            font-family: monospace;
        }
    </style>
</head>
<body>
<pre>
<b>Simulation #{{.ID}}</b>

Progress: {{.ETA.BarWithETA 20}}

Counters:{{template "counters" .Simulation.Counters}}
Properties:{{template "properties" .Simulation}}

The following endpoints have been registered:

    <a href="{{.EndpointPrefix}}/eta">{{.EndpointPrefix}}/eta</a>
    <a href="{{.EndpointPrefix}}/endpoints">{{.EndpointPrefix}}/endpoints</a>

    {{range .SlaveEndpoints}}<a href="{{.}}">{{.}}</a>
    {{end}}

</pre>
</body>
</html>
`))

// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package dromi

import (
	log "github.com/Sirupsen/logrus"
	"gitlab.com/dromi/dromi/event"
	"gitlab.com/dromi/dromi/internal/qa"
	"gitlab.com/dromi/dromi/script"
	"gitlab.com/dromi/dromi/simt"
)

// A Generator creates jobs according to some distribution, which contain pods
// that need to be scheduled and assigned in the cluster.
type Generator interface {
	// Next returns the next arrival event for the simulation.
	//
	// Post-conditions
	//
	//  ∙ The event time must be the same or after the last event returned.
	//    The time should not reach simt.Max nor should it overflow.
	//  ∙ Event properties:
	//      * Event.Type = event.Arrival
	//      * Event.Data = nil
	//      * Event.Job != nil
	//  ∙ Job properties:
	//      * Job.Arrival = Event.Time
	//      * Job.ID should be unique
	//      * len(Job.Sched) = 0
	//      * len(Job.SchedDur) = 0
	//      * Job.SchedEnd = 0
	//      * Job.Departure = 0
	//      * Job.Completed = false
	//  ∙ Pod properties:
	//      * If Pod.Deadline != 0:
	//          - Pod.Deadline > Job.Arrival
	//          - Pod.Runtime/Pod.MaxCores + Job.Arrival < Pod.Deadline
	//      * Pod.MinCores <= Pod.MaxCores
	//      * Pod.Memory > 0
	//      * Pod.Priority ∈ [0,12]
	Next() *event.Event

	// Finalize is called to indicate that the Generator will no longer be used.
	// Any internal state can be discarded.
	Finalize() error
}

// GeneratorQA {{{1

// GeneratorQA provides quality assurance for Scheduler instances.
type GeneratorQA struct {
	t simt.Time

	Generator
}

func init() { script.CreateFunc("qa|generator", NewGeneratorQA) }

func NewGeneratorQA(g Generator) Generator {
	if _, ok := g.(*GeneratorQA); ok {
		log.Warn("Generator QA should not be nested.")
		return g
	}
	return &GeneratorQA{
		t:         0,
		Generator: g,
	}
}

func (g *GeneratorQA) Next() *event.Event {
	e := g.Generator.Next()

	// Post-conditions
	//
	//  ∙ The event time must be the same or after the last event returned.
	//    The time should not reach simt.Max nor should it overflow.
	t := e.Time
	if t < g.t { // want: t >= g.t
		qa.Panic(
			"Generator.Next",
			"the event time must be the same or after the previous event time",
			qa.Expr("e.Time < g.t: %v < %v", t, g.t),
			qa.Data("e", e),
		)
	}
	g.t = t

	// Unchecked post-conditions
	//
	//  ∙ Event properties:
	//      * Event.Type = event.Arrival
	//      * Event.Data = nil
	//      * Event.Job != nil
	//  ∙ Job properties:
	//      * Job.Arrival = Event.Time
	//      * Job.ID should be unique
	//      * len(Job.Sched) = 0
	//      * len(Job.SchedDur) = 0
	//      * Job.SchedEnd = 0
	//      * Job.Departure = 0
	//      * Job.Completed = false
	//  ∙ Pod properties:
	//      * If Pod.Deadline != 0:
	//          - Pod.Deadline > Job.Arrival
	//          - Pod.Runtime/Pod.MaxCores + Job.Arrival < Pod.Deadline
	//      * Pod.MinCores <= Pod.MaxCores
	//      * Pod.Memory > 0
	//      * Pod.Priority ∈ [0,12]
	return e
}

// Copyright (c) 2015, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

// Package simt (simulation time) provides a more compact and efficient time library.
package simt

import (
	"bytes"
	"time"

	"github.com/goulash/twikutil"
	"gitlab.com/dromi/dromi/script"
)

func init() {
	script.CreateFunc("time", timeFn)
	script.CreateFunc("dur", durationFn)
	script.CreateFunc("durx", durationUnitFn)
}

// Duration as defined below is good for about 600 years.
type Duration int64

const (
	Nanosecond  Duration = 1
	Microsecond          = 1000 * Nanosecond
	Millisecond          = 1000 * Microsecond
	Second               = 1000 * Millisecond
	Minute               = 60 * Second
	Hour                 = 60 * Minute
	Day                  = 24 * Hour
	Week                 = 7 * Day
	Month                = 30 * Day
	Year                 = 356 * Day
)

const (
	Min Duration = -1 << 63
	Max Duration = 1<<63 - 1
)

// ParseDuration parses a duration string.
// A duration string is a possibly signed sequence of
// decimal numbers, each with optional fraction and a unit suffix,
// such as "300ms", "-1.5h" or "2h45m".
// Valid time units are "ns", "us" (or "µs"), "ms", "s", "m", "h".
func ParseDuration(s string) (Duration, error) {
	d, err := time.ParseDuration(s)
	return Duration(d), err
}

func MustParseDuration(s string) Duration {
	d, err := time.ParseDuration(s)
	if err != nil {
		panic(err)
	}
	return Duration(d)
}

func (d Duration) String() string { return time.Duration(d).String() }
func (d Duration) Hours() float64 { return float64(d) / float64(Hour) }

func (d Duration) MarshalJSON() ([]byte, error) {
	var buf bytes.Buffer
	buf.WriteRune('"')
	buf.WriteString(d.String())
	buf.WriteRune('"')
	return buf.Bytes(), nil
}

func durationFn(val interface{}) (Duration, error) {
	var want = []string{"int64", "string", "simt.Time"}
	switch t := val.(type) {
	case Duration:
		return t, nil
	case Time:
		return Duration(t), nil
	case int64:
		return Duration(t), nil
	case string:
		return ParseDuration(t)
	default:
		return 0, twikutil.NewTypeError(val, want)
	}
}

func durationUnitFn(val interface{}, unit interface{}) (Duration, error) {
	d, err := durationFn(unit)
	if err != nil {
		return 0, err
	}

	var want = []string{"int64", "float64"}
	switch t := val.(type) {
	case int64:
		return Duration(t) * d, nil
	case float64:
		return Duration(t * float64(d)), nil
	default:
		return 0, twikutil.NewTypeError(val, want)
	}
}

type Time int64

func ParseTime(s string) (Time, error) {
	d, err := time.ParseDuration(s)
	return Time(d), err
}

func MustParseTime(s string) Time {
	d, err := time.ParseDuration(s)
	if err != nil {
		panic(err)
	}
	return Time(d)
}

func (t Time) String() string               { return time.Duration(t).String() }
func (t Time) Hours() float64               { return float64(t) / float64(Hour) }
func (t Time) MarshalJSON() ([]byte, error) { return Duration(t).MarshalJSON() }

func timeFn(val interface{}) (Time, error) {
	var want = []string{"int64", "string", "simt.Duration"}
	switch t := val.(type) {
	case Duration:
		return Time(t), nil
	case int64:
		return Time(t), nil
	case string:
		return ParseTime(t)
	default:
		return 0, twikutil.NewTypeError(val, want)
	}
}

// Copyright (c) 2015, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package simt

import (
	"math/rand"

	"github.com/goulash/stat/dist"
	"gitlab.com/dromi/dromi/script"
)

func init() {
	script.CreateFunc("ddur.reg", Regular)
	script.CreateFunc("ddur.exp", Exponential)
	script.CreateFunc("ddur.discrete", Discrete)
	script.CreateFunc("ddur.continuous", Continuous)

	script.CreateFunc("proc.discrete", func(d dist.Discrete, unit Duration) Process {
		return NewProcess(Discrete(d, unit))
	})
	script.CreateFunc("proc.continuous", func(d dist.Continuous, unit Duration) Process {
		return NewProcess(Continuous(d, unit))
	})
	script.CreateFunc("proc.exp", func(s rand.Source, a float64, d Duration) Process {
		return NewProcess(Exponential(s, a, d))
	})
	script.CreateFunc("proc.unif", func(s rand.Source, a, z Duration) Process {
		return NewProcess(Uniform(s, a, z))
	})
	script.CreateFunc("proc.reg", func(d Duration) Process {
		return NewProcess(Regular(d))
	})
	script.CreateFunc("proc.fib", Fibonacci)
	script.CreateFunc("proc.once", Once)
	script.CreateFunc("proc|ceil", Ceil)
	script.CreateFunc("proc|combine", Combine)
	script.CreateFunc("proc|jitter", Jitter)
	script.CreateFunc("proc|taskapprox", TaskApproximator)
}

// A Distribution is a function that returns a (usually random) simulation
// duration.
//
// One common use is to use a Distribution to create a Process.
type Distribution func() Duration

// Distributions {{{

func Discrete(d dist.Discrete, unit Duration) Distribution {
	if unit == 0 {
		return func() Duration {
			return Duration(d.Int63())
		}
	} else {
		return func() Duration {
			return Duration(d.Int63()) * unit
		}
	}
}

// Continues returns a continuous time distribution, based on d * unit.
// If unit is 0, then we have the same as 1, which is nanoseconds.
func Continuous(d dist.Continuous, unit Duration) Distribution {
	if unit == 0 {
		return func() Duration {
			return Duration(d.Float64())
		}
	} else {
		return func() Duration {
			return Duration(d.Float64()) * unit
		}
	}
}

func Regular(d Duration) Distribution {
	return func() Duration {
		return d
	}
}

func Exponential(s rand.Source, arrivals float64, tau Duration) Distribution {
	e := dist.NewExponential(s, arrivals/float64(tau))
	return func() Duration {
		return Duration(e.Float64())
	}
}

func Uniform(s rand.Source, min, max Duration) Distribution {
	u := dist.NewUniformDiscrete(s, int64(min), int64(max))
	return func() Duration {
		return Duration(u.Int63())
	}
}

// }}}

// A Process should return a new time that is strictly greater than or equal to
// from.
//
// Usually, the interarrival times is taken either randomly or discretely
// and added onto from.
//
// If from is simt.Max, then the behavior is currently not specified.
// In practice, this scenario should be avoided. For Dromi, it makes
// most sense to return simt.Max rather than overflow.
// However, since it is unlikely to happen, and because an extra check
// could result in a significant slowdown, this check may be elided.
type Process interface {
	Next(from Time) Time
}

// NewProcess returns a simple time Process where the interarrival times
// are distributed according to d.
func NewProcess(d Distribution) Process { return &proc{d} }

// proc is a generic process.
type proc struct{ d Distribution }

func (p *proc) Next(from Time) Time {
	return from + Time(p.d())
}

// Once process {{{

// Once returns a process that will return t once and then Max.
func Once(t Time) Process {
	return &oncep{t}
}

type oncep struct {
	next Time
}

func (p *oncep) Next(t Time) Time {
	next := p.next
	p.next = Time(Max)
	return next
}

// }}}

// Combine two processes {{{

// Combine returns a new process that combines p and q.
//
// Next always returns the next value in the series, ignoring the time parameter.
// This means that it only makes sense if the Process starts with Next(0)
// followed by whatever that returns, etc.
func Combine(p, q Process) Process {
	if p == nil {
		return q
	} else if q == nil {
		return p
	}
	return &combp{p: p, q: q}
}

type combp struct {
	t      Time
	p, q   Process
	pn, qn Time
}

// TODO: Double check whether this here is accurate.
func (p *combp) Next(_ Time) Time {
	if p.pn == p.t {
		p.pn = p.p.Next(p.t)
	}
	if p.qn == p.t {
		p.qn = p.q.Next(p.t)
	}
	if p.pn < p.qn {
		p.t = p.pn
	}
	p.t = p.qn
	return p.t
}

// }}}

// TODO: Min process
// TODO: Max process
// TODO: Random process?

// Ceil process {{{

// Ceil restricts the maximum interarrival duration to max.
//
// Example
//
// The Fibonacci process quickly reaches simt.Max, so if the beginning phase
// should be more fine-grained, and then pan out, it can be done so:
//
//	Ceil(10*Minute, Fibonacci(0))
//
func Ceil(max Duration, p Process) Process { return &ceilp{p, max} }

type ceilp struct {
	p   Process
	max Duration
}

func (h *ceilp) Next(from Time) Time {
	return min(h.p.Next(from), from+Time(h.max))
}

// }}}

// Fibonacci process {{{

// Fibonacci will return a process that increases the interarrival distance.
// This will quickly reach Max, so it is only useful for analyzing the
// beginning of processes.
func Fibonacci(start Duration) Process {
	if start == 0 {
		start = 1 * Millisecond
	}
	return &fib{false, 0, Time(start)}
}

type fib struct {
	overflow bool
	first    Time
	second   Time
}

func (f *fib) Next(from Time) Time {
	if f.overflow {
		return Time(Max)
	}
	next := f.first + f.second
	if next < f.second {
		f.overflow = true
		return Time(Max)
	}
	f.first = f.second
	f.second = next

	retval := from + next
	if retval < max(from, next) {
		return Time(Max)
	}
	return retval
}

// }}}

// Jitter process {{{

// Jitter adds a jitter to every value returned from the process.
// In order to prevent invalidity, every value that is less than zero is inverted.
func Jitter(j Distribution, p Process) Process {
	return &jitter{p, j}
}

type jitter struct {
	p Process
	j Distribution
}

func (p *jitter) Next(from Time) Time {
	j := Time(p.j())
	if j < 0 {
		j *= -1
	}
	return p.Next(from) + j
}

// }}}

// Task approximator process {{{

// TaskApproximator is a process that repeats the last time several times based
// on the number from the tasks distribution.
//
// The tasks distribution should return integers starting with zero. This is
// the number of additional tasks. If zero is always returned, it is the same
// as if only the process p were acting.
//
// This effectively approximates jobs with tasks.
func TaskApproximator(tasks dist.Discrete, p Process) Process {
	return &taskApproximator{p, tasks, 0}
}

type taskApproximator struct {
	p  Process       // arrival times process
	td dist.Discrete // tasks distribution
	n  int           // remaining tasks to emit
}

func (p *taskApproximator) Next(from Time) Time {
	if p.n == 0 {
		// Get a new time and set n.
		p.n = int(p.td.Int63())
		if p.n < 0 {
			p.n = 0
		}
		return p.p.Next(from)
	}
	p.n--
	return from
}

// }}}

func max(s, t Time) Time {
	if s < t {
		return t
	}
	return s
}

func min(s, t Time) Time {
	if s < t {
		return s
	}
	return t
}

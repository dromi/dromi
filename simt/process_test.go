// Copyright (c) 2015, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package simt

import (
	"math/rand"
	"testing"
)

func TestProcess(z *testing.T) {
	s := rand.NewSource(0)
	p := NewProcess(Exponential(s, 1, 10*Millisecond))

	var last Time
	for i := 0; i < 1000; i++ {
		t := p.Next(last)
		if t < last {
			z.Errorf("time must be monotonically increasing!")
		}
	}
}

func TestParseDuration(z *testing.T) {
	tests := []struct {
		Test string
		Exp  Duration
	}{
		{"1ms", 1 * Millisecond},
	}

	for _, t := range tests {
		s, err := ParseDuration(t.Test)
		if err != nil {
			z.Error(err)
		}
		if s != t.Exp {
			z.Errorf("ParseDuration(%q) = %v, want %v", t.Test, s, t.Exp)
		}
	}
}

func TestFib(z *testing.T) {
	exp := []int{1, 3, 6, 11, 19, 32}
	fib := Fibonacci(1 * Millisecond)
	var t Time
	for _, i := range exp {
		j := Time(Duration(i) * Millisecond)
		s := fib.Next(t)
		if s != j {
			z.Errorf("Fibonacci.Next(%v) = %v; want %v", t, s, j)
		}
		t = s
	}
}

func TestCeilFib(z *testing.T) {
	exp := []int{1, 3, 6, 11, 19, 32, 53, 87, 137, 187, 237}
	fib := Ceil(50*Millisecond, Fibonacci(1*Millisecond))
	var t Time
	for _, i := range exp {
		j := Time(Duration(i) * Millisecond)
		s := fib.Next(t)
		if s != j {
			z.Errorf("Fibonacci.Next(%v) = %v; want %v", t, s, j)
		}
		t = s
	}
}

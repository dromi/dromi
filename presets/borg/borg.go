// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package borg

import (
	"math/rand"

	"gitlab.com/dromi/dromi"
	"gitlab.com/dromi/dromi/cl"
	"gitlab.com/dromi/dromi/internal"
	"gitlab.com/dromi/dromi/mdl"
	"gitlab.com/dromi/dromi/script"
)

func init() {
	script.CreateFunc("preset.borg-cluster", func(hw mdl.Hardware, rf cl.ReaperFunc, n int64, rs rand.Source) dromi.Cluster {
		return NewCluster(hw, rf, int(n), rs)
	})
}

// NewCluster creates a cluster structure that is similar to the cluster
// from the Google cluster trace. The hardware hw passed in is scaled down for
// a certain percentage of machines:
//
//  Number of machines | Platform | CPUs | Memory
//                6732 | B        | 0.50 | 0.50
//                3863 | B        | 0.50 | 0.25
//                1001 | B        | 0.50 | 0.75
//                 795 | C        | 1.00 | 1.00
//                 126 | A        | 0.25 | 0.25
//                  52 | B        | 0.50 | 0.12
//                   5 | B        | 0.50 | 0.03
//                   5 | B        | 0.50 | 0.97
//                   3 | C        | 1.00 | 0.50
//                   1 | B        | 0.50 | 0.06
//
// It is recommended, based on the above table, to pass in a hardware
// containing at number of cores that is a multiple of 4.
// Platform types are not specified in the paper, hence we make up the
// following:
//
//  A - has no network capacity
//  B - no change
//  C - strongly reduced graphics capacity
//  D - left-over machines have hardware capacity hw
//
// If rs is nil, then the resulting cluster is not shuffled.
func NewCluster(hw mdl.Hardware, rf cl.ReaperFunc, n int, rs rand.Source) dromi.Cluster {
	// ratios is a constant
	ratios := []struct {
		N        float64
		Platform int
		Cores    float32
		Memory   float64
	}{
		{0.5350, 1, 0.50, 0.50},
		{0.3070, 1, 0.50, 0.25},
		{0.0796, 1, 0.50, 0.75},
		{0.0632, 2, 1.00, 1.00},
		{0.0100, 0, 0.25, 0.25},
		{0.0041, 1, 0.50, 0.12},
		{0.0004, 1, 0.50, 0.03},
		{0.0004, 1, 0.50, 0.97},
		{0.0002, 2, 1.00, 0.50},
		{0.0001, 1, 0.50, 0.06},
	}
	ratioNames := []string{
		"B53", "B30", "B8", "C6", "A1", "B.4", "B.04", "B.04b", "C.02", "B.01", "D",
	}

	if rf == nil {
		panic("reaper func cannot be nil")
	}

	var (
		machines = make([]dromi.Machine, n)
		ratioNs  = make([]int, len(ratios)+1)
		i        = 0
	)

	// Set up shuffling, if rs is not nil
	indices := internal.List0N(n)
	if rs != nil {
		internal.Shuffle(rs, indices)
	}

	// Add machines, modifying them as the ratios state
	for ridx, r := range ratios {
		// TODO: could round this number instead of truncating...
		// That should in theory mean we don't need class D machines.
		m := int(r.N * float64(n))
		if m == 0 {
			// It's not going to get larger, so quit at this point.
			break
		}
		ratioNs[ridx] = m

		h := hw
		h.Cores *= r.Cores
		h.Memory = uint32(float64(h.Memory) * r.Memory)
		switch r.Platform {
		case 0: // A
			h.Network = 0
		case 2: // C
			h.Graphics /= 4
		default: // B, D
			// no change
		}

		for j := 0; j < m; j++ {
			k := indices[i]
			machines[k] = cl.NewMachine(h, rf(h))
			i++
		}
	}

	// Add the rest of the machines (class D)
	ratioNs[len(ratios)] = n - i
	for i < n {
		k := indices[i]
		machines[k] = cl.NewMachine(hw, rf(hw))
		i++
	}

	c := cl.NewCluster(machines)

	// Create labels, we reset i at this point
	i = 0
	for j, key := range ratioNames {
		m := ratioNs[j]
		c.SetLabelSubset(key, indices[i:i+m])
		i += m
	}
	if i != n {
		panic("programming error")
	}

	return c
}

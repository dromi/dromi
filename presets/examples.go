// Copyright (c) 2015, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package presets

import (
	"math/rand"

	"github.com/goulash/stat/dist"
	"gitlab.com/dromi/dromi/gen"
	"gitlab.com/dromi/dromi/mdl"
	"gitlab.com/dromi/dromi/script"
	"gitlab.com/dromi/dromi/simt"
)

func init() {
	script.CreateFunc("preset.podder-simple", Simple)
	script.CreateFunc("preset.podder-simple-long", SimpleLong)
	script.CreateFunc("preset.podder-simple-d", SimpleD)
	script.CreateFunc("preset.podder-simple-long-d", SimpleLongD)
	script.CreateFunc("preset.podmod-servicer", NewServicer)
}

// Servicer {{{1

func NewServicer(d *gen.Distributed) gen.Podder {
	return &servicer{d}
}

type servicer struct {
	*gen.Distributed
}

func (p *servicer) Pod(t simt.Time) mdl.Pod {
	pod := p.Distributed.Pod(t)
	if pod.Priority > 9 {
		pod.Runtime = 0
		pod.Deadline *= 100
	}
	return pod
}

// Podder Examples {{{1

func Simple(s rand.Source) *gen.Distributed {
	return SimpleD(s, 10*simt.Second)
}

func SimpleD(s rand.Source, d simt.Duration) *gen.Distributed {
	return &gen.Distributed{
		Runtime:  simt.Exponential(s, 1, d),
		Deadline: simt.Exponential(s, 1, 10*d),
		Priority: dist.NewUniformDiscrete(s, 0, 12),
		MinCores: dist.Highpass(dist.NewNormal(s, 0.5, 0.2), 0.1),
		MaxCores: dist.Highpass(dist.NewNormal(s, 1.0, 1.0), 1),
		Memory:   dist.Highpass(dist.NewNormal(s, 500, 500), 250),
		Network:  dist.Highpass(dist.NewNormal(s, 1*mdl.Mbps, 10*mdl.Mbps), 0),
	}
}

func SimpleLong(s rand.Source) *gen.Distributed {
	return SimpleLongD(s, 1*simt.Minute)
}

func SimpleLongD(s rand.Source, d simt.Duration) *gen.Distributed {
	return &gen.Distributed{
		Runtime:  simt.Exponential(s, 1, d),
		Deadline: simt.Exponential(s, 1, 5*d),
		Priority: dist.NewUniformDiscrete(s, 0, 12),
		MinCores: dist.Highpass(dist.NewNormal(s, 1.5, 0.5), 0.01),
		MaxCores: dist.Highpass(dist.NewNormal(s, 2.0, 1.0), 2),
		Memory:   dist.Highpass(dist.NewNormal(s, 500, 500), 250),
		Network:  dist.Highpass(dist.NewNormal(s, 10*mdl.Mbps, 10*mdl.Mbps), 1*mdl.Mbps),
	}
}

// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package thesis

import (
	"math/rand"

	"github.com/goulash/stat/dist"
	"gitlab.com/dromi/dromi"
	"gitlab.com/dromi/dromi/event"
	"gitlab.com/dromi/dromi/gen"
	"gitlab.com/dromi/dromi/mdl"
	"gitlab.com/dromi/dromi/script"
	"gitlab.com/dromi/dromi/simt"
)

func init() {
	script.CreateFunc("preset.thesis-batch", Batch)
	script.CreateFunc("preset.thesis-service", Service)
	script.CreateFunc("preset.thesis-gen", Generator)
}

func Batch(s rand.Source) *gen.Distributed {
	return &gen.Distributed{
		// Leads to the 50%-quantile being around 3 minutes
		Runtime:  simt.Continuous(dist.NewLogNormal(s, 50, 1000), 1*simt.Minute),
		Deadline: func() simt.Duration { return 0 },
		Priority: dist.NewNull(),
		MinCores: dist.Highpass(dist.NewNormal(s, 1, 2), 0.01),
		MaxCores: dist.Highpass(dist.NewNormal(s, 6, 2), 1),
		Memory:   dist.Highpass(dist.NewNormal(s, 500, 2000), 200),
		Network:  dist.Highpass(dist.NewNormal(s, 10*mdl.Mbps, 10*mdl.Mbps), 0),
	}
}

func Service(s rand.Source) *gen.Distributed {
	return &gen.Distributed{
		/* These parameters leads to:
		   mean: 995.3479
		   min: 6.939719
		   max: 19302.24
		   quantiles:
		       0%       25%     50%      75%      100%
		       6.939719 208.209 448.1527 1035.608 19302.24
		*/
		Runtime:  simt.Continuous(dist.NewLogNormal(s, 1000, 2000), 1*simt.Minute),
		Deadline: func() simt.Duration { return 0 },
		Priority: dist.NewNull(),
		MinCores: dist.Highpass(dist.NewNormal(s, 2.5, 2), 0.01),
		MaxCores: dist.Highpass(dist.NewNormal(s, 10, 2), 1),
		Memory:   dist.Highpass(dist.NewNormal(s, 500, 2000), 200),
		Network:  dist.Highpass(dist.NewNormal(s, 10*mdl.Mbps, 10*mdl.Mbps), 0),
	}
}

type generator struct {
	rnd       *rand.Rand
	batch     gen.Podder
	batchProb float64
	service   gen.Podder
	process   simt.Process
	time      simt.Time
	id        int64
}

func Generator(s rand.Source, batch gen.Podder, batchProb float64, service gen.Podder, p simt.Process) dromi.Generator {
	return &generator{
		rnd:       rand.New(s),
		batch:     batch,
		batchProb: batchProb,
		service:   service,
		process:   p,
	}
}

func (g *generator) Finalize() error {
	g.rnd = nil
	g.batch = nil
	g.service = nil
	g.process = nil
	return nil
}

func (g *generator) Next() *event.Event {
	var (
		prev simt.Time
		pod  mdl.Pod
	)

	g.id++
	prev, g.time = g.time, g.process.Next(g.time)
	if g.time == prev {
		// We don't really have service jobs bundled, so any bundled jobs with
		// a service job are batch jobs.
		pod = g.batch.Pod(g.time)
		pod.SetMark(1) // part of a bundle
	} else {
		// Roll a dice to find out whether it's going to be batch or service.
		if g.rnd.Float64() < g.batchProb {
			pod = g.batch.Pod(g.time)
		} else {
			pod = g.service.Pod(g.time)
			pod.SetMark(0) // a service job
		}
	}

	return &event.Event{
		Time: g.time,
		Type: event.Arrival,
		Job: &mdl.Job{
			ID:       g.id,
			Arrival:  g.time,
			Pod:      pod,
			Sched:    make([]simt.Time, 0, 2),
			SchedDur: make([]simt.Duration, 0, 2),
		},
	}
}

var _ = dromi.Generator(&generator{})

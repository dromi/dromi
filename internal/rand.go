package internal

import "math/rand"

func List0N(n int) []int {
	slice := make([]int, n)
	for i := range slice {
		slice[i] = i
	}
	return slice
}

func ShuffleFirstN(s rand.Source, n int, xs []int) {
	r := rand.New(s)
	m := len(xs)
	for i := 0; i < n; i++ {
		j := r.Intn(m-i) + i
		xs[i], xs[j] = xs[j], xs[i]
	}
}

func Shuffle(s rand.Source, xs []int) {
	r := rand.New(s)
	n := len(xs)
	for i := n - 1; i > 0; i-- {
		j := r.Intn(i + 1)
		xs[i], xs[j] = xs[j], xs[i]
	}
}

// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package json

import (
	"math"
	"strconv"

	"gitlab.com/dromi/dromi/simt"
)

type Float64 float64

func (f Float64) String() string {
	return strconv.FormatFloat(float64(f), 'f', -1, 64)
}

func (f Float64) StringJSON() string {
	v := float64(f)
	if math.IsNaN(v) {
		return "null"
	} else if math.IsInf(v, 0) {
		if math.IsInf(v, -1) {
			return `"-Inf"`
		} else {
			return `"+Inf"`
		}
	}
	return f.String()
}

func (f Float64) MarshalJSON() ([]byte, error) {
	return []byte(f.StringJSON()), nil
}

// Duration represents a duration as a float, which is useful when a time
// had to be manipulated as a float (for statistics purposes, for example).
type Duration float64

func (d Duration) String() string {
	v := float64(d)
	if math.IsNaN(v) {
		return "NaN"
	} else if math.IsInf(v, 0) {
		if math.IsInf(v, -1) {
			return "-Inf"
		} else {
			return "+Inf"
		}
	} else {
		return simt.Duration(v).String()
	}
}

func (d Duration) StringJSON() string {
	v := float64(d)
	if math.IsNaN(v) {
		return "null"
	} else if math.IsInf(v, 0) {
		if math.IsInf(v, -1) {
			return `"-Inf"`
		} else {
			return `"+Inf"`
		}
	} else {
		return `"` + simt.Duration(v).String() + `"`
	}

}

func (d Duration) MarshalJSON() ([]byte, error) {
	return []byte(d.StringJSON()), nil
}

func Must(bs []byte, err error) []byte {
	if err != nil {
		panic(err)
	}
	return bs
}

// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package json

import (
	"bytes"
	"encoding/json"
	"fmt"

	"gitlab.com/dromi/dromi/simt"
)

type Buffer struct {
	buf   bytes.Buffer
	ready bool
}

func (b *Buffer) Bytes() []byte  { return b.buf.Bytes() }
func (b *Buffer) String() string { return b.buf.String() }

func (b *Buffer) StartObject() {
	b.buf.WriteRune('{')
	b.ready = true
}

func (b *Buffer) NextObject() {
	if !b.ready {
		b.buf.WriteRune(',')
		b.ready = true
	}
	b.buf.WriteRune('{')
}

func (b *Buffer) EndObject() {
	b.buf.WriteRune('}')
	b.ready = false
}

func (b *Buffer) StartArray() {
	b.buf.WriteRune('[')
	b.ready = true
}

func (b *Buffer) EndArray() {
	b.buf.WriteRune(']')
	b.ready = false
}

func (b *Buffer) WriteKey(k string) *Buffer {
	if !b.ready {
		b.buf.WriteRune(',')
	}
	b.buf.WriteRune('"')
	b.buf.WriteString(k)
	b.buf.WriteString(`":`)
	return b
}

func (b *Buffer) WriteString(v string) {
	b.buf.WriteString(fmt.Sprintf("%q", v))
	b.ready = false
}

func (b *Buffer) WriteFloat64(v float64) {
	s := Float64(v).StringJSON()
	b.buf.WriteString(s)
	b.ready = false
}

func (b *Buffer) WriteFloat64AsDuration(v float64) {
	s := Duration(v).StringJSON()
	b.buf.WriteString(s)
	b.ready = false
}

func (b *Buffer) WriteDuration(v simt.Duration) {
	b.buf.WriteString(v.String())
	b.ready = false
}

func (b *Buffer) WriteMarshaler(v json.Marshaler) {
	bs, err := v.MarshalJSON()
	if err != nil {
		panic("unexpected error while marshaling")
	}
	b.buf.Write(bs)
	b.ready = false
}

type KV struct {
	Key   string
	Value interface{}
}

type Object []KV

type Array []interface{}

func (b *Buffer) Next() {
	if !b.ready {
		b.buf.WriteRune(',')
		b.ready = true
	}
}

func (b *Buffer) Write(v interface{}) {
	switch t := v.(type) {
	case Object:
		b.StartObject()
		for _, x := range t {
			b.WriteKey(x.Key).Write(x.Value)
		}
		b.EndObject()
	case Array:
		b.StartArray()
		for _, x := range t {
			b.Next()
			b.Write(x)
		}
		b.EndArray()
	case []KV:
		for _, x := range t {
			b.WriteKey(x.Key).Write(x.Value)
		}
	case KV:
		b.WriteKey(t.Key).Write(t.Value)
	default:
		bs, err := json.Marshal(v)
		if err != nil {
			panic("unexpected error while marshaling")
		}
		b.buf.Write(bs)
		b.ready = false
	}
}

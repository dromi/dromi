// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package json

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBuffer1(z *testing.T) {
	assert := assert.New(z)

	var buf Buffer
	buf.Write(Object{
		{"name", "Ben Morgan"},
		{"age", 26},
	})
	assert.Equal(`{"name":"Ben Morgan","age":26}`, buf.String())
}

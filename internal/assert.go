package internal

import "fmt"

func Assert(expr bool, v ...interface{}) {
	if !expr {
		if len(v) == 0 {
			panic("Assertion failed!")
		}
		panic(fmt.Sprint(v...))
	}
}

func Assertf(expr bool, format string, v ...interface{}) {
	if !expr {
		panic(fmt.Sprintf(format, v...))
	}
}

func AssertFunc(expr bool, f func() string) {
	if !expr {
		panic(f())
	}
}

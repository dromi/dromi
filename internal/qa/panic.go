package qa

import (
	"bytes"
	"fmt"
	"os"
	"reflect"
	"runtime/debug"
)

type expr string

func Expr(format string, v ...interface{}) expr {
	return expr(fmt.Sprintf(format, v...))
}

func Data(name string, v interface{}) expr {
	if reflect.TypeOf(v).Kind() == reflect.Slice {
		vv := reflect.ValueOf(v)
		var buf bytes.Buffer
		buf.WriteString(name)
		buf.WriteString(": [")
		for i := 0; i < vv.Len(); i++ {
			buf.WriteString("\n\t\t")
			buf.WriteString(fmt.Sprint(vv.Index(i).Interface()))
		}
		if vv.Len() != 0 {
			buf.WriteString("\n")
		}
		buf.WriteString("\t]")
		return expr(buf.String())
	}
	return expr(fmt.Sprintf("%s: %v", name, v))
}

func Panic(from, msg string, xs ...expr) {
	fmt.Fprintf(os.Stderr, "======================================================\n")
	fmt.Fprintf(os.Stderr, "%s QA failed: %s\n", from, msg)
	for _, x := range xs {
		fmt.Fprintf(os.Stderr, "\t%s\n", x)
	}
	fmt.Fprintf(os.Stderr, "======================================================\n")
	panic("QUALITY ASSURANCE FAILED")
}

func Stack(from, msg string, xs ...expr) {
	fmt.Fprintf(os.Stderr, "=====================================================>\n")
	fmt.Fprintf(os.Stderr, "%s QA failed: %s\n", from, msg)
	for _, x := range xs {
		fmt.Fprintf(os.Stderr, "\t%s\n", x)
	}
	debug.PrintStack()
	fmt.Fprintf(os.Stderr, "<=====================================================\n")
}

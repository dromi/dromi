// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package dromi

import (
	"errors"
	"net/http"
	"runtime"
	"sort"
	"sync"

	log "github.com/Sirupsen/logrus"
	"github.com/goulash/errs"
	"gitlab.com/dromi/dromi/simt"
	"gitlab.com/dromi/dromi/webutil"
)

var (
	ErrInitializeOnce = errors.New("initialization can only occur once")
	ErrImmutableNow   = errors.New("the variable can only be mutated before initialization")
	ErrFinalizeEarly  = errors.New("finalization can only occur after simulation termination")
)

var masterID int

const (
	builtinEndpoints = 3
)

// Master takes care of running a simulation, gathering all observers,
// providing online access, releasing memory after the simulation is
// over, and so on.
//
// A Master cannot be reused, and should only be created via the
// NewMaster function.
type Master struct {
	id int

	simulation *Simulation
	slaves     []*Slave
	endpoints  []string
	prefix     string

	// eta is updated every few loop iterations by a StopFunc function that
	// tells it how much progress has been made. The real remaining time
	// is then estimated with this object.
	eta webutil.ETA

	// mutex is used to prevent web requests from returning inconsistent and
	// invalid results. The mutex is locked at all times during simulation
	// execution, except between loop iterations. If a web request occurs, then
	// a read lock is set, which prevents the simulation from continuing until
	// the request is complete.
	mutex sync.RWMutex
	mux   *http.ServeMux

	initialized bool          // set to true after master has been initialized
	active      bool          // set to true after the simulation is started
	paused      bool          // set to true when the simulation is paused
	done        chan struct{} // used for signaling when complete
}

// NewMaster creates a new Master object. The parameter s can be nil,
// but must be set prior to initialization.
func NewMaster(s *Simulation) *Master {
	masterID++
	m := &Master{
		id:         masterID,
		simulation: s,
		endpoints:  []string{"/", "/endpoints", "/eta"},
		mux:        http.NewServeMux(),
		done:       make(chan struct{}),
	}
	m.mux.HandleFunc("/", m.rootHandler)
	m.mux.HandleFunc("/eta", m.etaHandler)
	m.mux.HandleFunc("/endpoints", m.endpointsHandler)
	return m
}

func (m *Master) ID() int { return m.id }
func (m *Master) SetID(id int) {
	if m.initialized {
		panic(ErrImmutableNow)
	}
	m.id = id
}

func (m *Master) Simulation() *Simulation { return m.simulation }
func (m *Master) SetSimulation(s *Simulation) {
	if m.initialized {
		panic(ErrImmutableNow)
	}
	m.simulation = s
}

func (m *Master) EndpointPrefix() string { return m.prefix }
func (m *Master) SetEndpointPrefix(s string) {
	if m.initialized {
		panic(ErrImmutableNow)
	}
	m.prefix = s
	m.endpoints = []string{s, s + "/endpoints", s + "/eta"}
}

// Initialize performs necessary setup for the simulation.
//
// In particular, the slaves are all initialized. Initialization can be
// performed as late as possible in order to minimize memory usage.
//
// Before intialization, it is still possible to use the ServeHTTP() method,
// however, the slaves will not be visible in the template.
func (m *Master) Initialize() error {
	// Neither simulation nor web access should occur.
	m.mutex.Lock()
	defer m.mutex.Unlock()

	// Ensure Initialize is only called once.
	if m.initialized {
		return ErrInitializeOnce
	}

	ec := errs.NewCollector("initialization failed")
	if err := m.simulation.Initialize(); err != nil {
		ec.Add(err)
	}
	for _, s := range m.slaves {
		// Registration as trigger-observer or observer happens in each slave,
		// as this ability needs to be passed on to private slaves, while
		// only top-level slaves get registered as servers, which is why,
		// as a special case, they are registered here.
		if err := s.initialize(m); err != nil {
			ec.Add(err)
		}

		if s.IsServer() {
			if s.Serve != nil {
				m.mux.Handle(s.Endpoint, m.slaveHandler(s))
				m.endpoints = append(m.endpoints, m.prefix+s.Endpoint)
			}
			if s.ServeMap != nil {
				for ep, f := range s.ServeMap {
					sep := s.Endpoint + ep
					m.mux.Handle(sep, m.slaveFnHandler(f))
					m.endpoints = append(m.endpoints, m.prefix+sep)
				}
			}
		}
	}
	sort.Strings(m.endpoints[builtinEndpoints:])
	m.initialized = true
	return ec.Error()
}

// Finalize can be called after a simulation is complete.
//
// As much information as possible is discarded. The final state of observers
// is saved and extra references (for example, to the simulation) are discarded.
// In this way, we lower the risk that a memory leak happens because data is
// still being referenced somewhere.
func (m *Master) Finalize() error {
	// Neither simulation nor web access should occur.
	m.mutex.Lock()
	defer m.mutex.Unlock()
	defer runtime.GC()

	select {
	case <-m.done:
		// ok
	default:
		return ErrFinalizeEarly
	}

	ec := errs.NewCollector("finalization failed")
	for _, s := range m.slaves {
		ec.Collect(s.finalize())
	}
	ec.Collect(m.simulation.Finalize())
	return ec.Error()
}

// Slaves returns the underlying slice of registered slaves.
// It should not be modified.
func (m *Master) Slaves() []*Slave {
	return m.slaves
}

func (m *Master) RegisterSlave(ss ...*Slave) {
	if m.initialized {
		panic(ErrImmutableNow)
	}

	for _, s := range ss {
		m.slaves = append(m.slaves, s)
	}
}

// ResetSlaves resets all slaves that implement the Reset function.
//
// NOTE: Do not call this function unless you really know what you are doing!
// This function should only be called during simulation, while the master is
// in a locked state.
func (m *Master) ResetSlaves() {
	t := m.simulation.Time()
	for _, s := range m.slaves {
		if s.Reset != nil && !s.Exempt {
			s.Reset(t)
		}
	}
}

// FindSlave finds and returns a registered slave by its endpoint string.
// This can be done before and after initialization.
func (m *Master) FindSlave(endpoint string) *Slave {
	for _, s := range m.slaves {
		if s.Endpoint == endpoint {
			return s
		}
	}
	return nil
}

// SlaveEndpoints returns a sorted slice of slave endpoints, excluding the
// built-in ones.
func (m *Master) SlaveEndpoints() []string { return m.endpoints[builtinEndpoints:] }

// Endpoints returns a sorted slice of all endpoints, including built-in ones.
func (m *Master) Endpoints() []string { return m.endpoints }

// slaveEndpoints adds the slave endpoints to out, sorts it, and returns it.
// If the capacity of out is not correct, the returned slice may refer to
// a different backing array.
func (m *Master) slaveEndpoints(out []string) []string {
	for _, x := range m.slaves {
		if x.IsServer() {
			out = append(out, m.prefix+x.Endpoint)
		}
	}
	return out
}

// ServeHTTP lets Master implement the http.Handler interface. You can
// thus put the API at whichever path suits you best by doing the following
// (note that the trailing slash is important!):
//
//  m := dromi.NewMaster(c, g, s)
//  http.Handle("/api/", http.StripPrefix("/api/", m))
//
func (m *Master) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	m.mux.ServeHTTP(w, r)
}

// ETA returns the estimated-time-of-arrival, which contains several metrics
// and functions pertaining to the progress of the simulation. It can be
// called at any time.
func (m *Master) ETA() webutil.ETA {
	m.mutex.RLock()
	eta := m.eta // make a copy
	m.mutex.RUnlock()
	return eta
}

// Wait returns when Master is done simulating.
//
// Example
//
//  m.Inititialize()
//  m.Execute(stopFunc)
//  go func() {
//      time.Sleep(waitingPeriod)
//      m.Unpause()
//  }()
//  m.Wait()
//
func (m *Master) Wait() { <-m.done }

// WaitChan returns the channel that is closed upon simulation completion.
//
// Example
//
//  m.Initialize()
//  m.Execute(stopFunc)
//  done := make(chan struct{})
//  go func() {
//      ch := m.WaitChan()
//      defer close(done)
//      for {
//          fmt.Print("Are we done yet? ")
//          select {
//          case <-ch:
//              fmt.Println("Yes!")
//              return
//          default:
//              fmt.Println("Not yet...")
//          }
//          time.Sleep(1*time.Second)
//      }
//  }()
//  <-done
//
func (m *Master) WaitChan() <-chan struct{} { return m.done }

// IsPaused returns true if the simulation is paused.
//
// The simulation by default is not paused, so this means that Pause was called.
// Pause can also be called even before the simulation is initialized or executed.
func (m *Master) IsPaused() bool {
	return m.paused
}

func (m *Master) Pause() {
	if !m.paused {
		m.paused = true
		m.mutex.RLock()
	}
}

func (m *Master) Unpause() {
	if m.paused {
		m.paused = false
		m.mutex.RUnlock()
	}
}

func (m *Master) Execute(fn StopFunc) {
	if fn == nil {
		panic("stop function cannot be nil")
	}

	// Initialize the simulation and all the slaves.
	if !m.initialized {
		m.Initialize()
	}

	m.mutex.Lock()
	defer m.mutex.Unlock()

	// Ensure the simulation is only run once.
	if m.active {
		panic("master cannot be reused or executed multiple times")
	} else {
		m.active = true
	}

	var i int
	log.Infof("Simulation %d commencing.", m.id)
	m.eta.Begin()
	m.simulation.loop(func(state *Counters) bool {
		// update progress before we unlock
		p := fn(state)
		if p < 0 {
			log.Warningf("Completion percentage %f below zero", p)
		}

		// Update only every 10 times to save CPU
		i++
		if i%10 == 0 {
			m.eta.UpdateSafe(p)
		}

		// We unlock mutex, so web requests and pause events can act.
		m.mutex.Unlock()
		defer m.mutex.Lock()

		return p < 1
	})
	m.eta.End()
	log.Infof("Simulation %d complete.", m.id)

	// Signal that we are done with the simulation.
	close(m.done)
}

// StopFunc is a function that returns between 0.0 and 1.0 to signal
// level of completion.
//
// The value is interpreted as the percentage complete, hence if a value
// greater or equal to 1 is returned, it indicates stop. If a value less than
// 0 is returned, it indicates error.
type StopFunc func(s *Counters) float64

func ForDuration(d simt.Duration) StopFunc {
	denom := float64(d)
	return func(s *Counters) float64 {
		return float64(s.Time) / denom
	}
}

func UntilAssigned(n int64) StopFunc {
	denom := float64(n)
	return func(s *Counters) float64 {
		return float64(s.Assignments) / denom
	}
}

func UntilSubmitted(n int64) StopFunc {
	denom := float64(n)
	return func(s *Counters) float64 {
		return float64(s.Submissions) / denom
	}
}

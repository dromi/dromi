// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package dromi

import (
	"errors"

	log "github.com/Sirupsen/logrus"
	"github.com/goulash/errs"
	"gitlab.com/dromi/dromi/event"
	"gitlab.com/dromi/dromi/simt"
)

// ErrSimulationInvalid is returned from Simulation.Init if
// the simulation is not set up in a valid way (see Simulation.IsValid).
var ErrSimulationInvalid = errors.New("simulation is invalid")

// Observer is the type stored in the Data field of an observation event.
type Observer interface {
	Observe(e *event.Event)
	simt.Process
}

// Counters stores a myriad of values that Master updates in the course
// of a simulation.
type Counters struct {
	Time simt.Time // simulated time

	Arrivals            int64 // jobs created
	Departures          int64 // jobs naturally terminated
	Evictions           int64 // jobs evicted to make space for higher-priority jobs
	Submissions         int64 // jobs submitted for scheduling (total)
	Rejections          int64 // jobs rejected because rescheduled too often
	SchedulingFailures  int64 // jobs that couldn't be assigned to a machine
	SchedulingConflicts int64 // scheduler internal conflict (is a failure)
	SchedulingErrors    int64 // haven't seen this happen yet (not a failure)
	Assignments         int64 // jobs assigned to machines
	AssignmentErrors    int64 // errors on assignment attempts
	Observations        int64 // observations made by observers
	Errors              int64 // don't know why we have this
	Events              int64 // total number of events processed
}

// Simulation stores all the parameters and types required for running a
// cluster simulation.
//
// The zero value of a Simulation cannot be used.
type Simulation struct {
	Cluster   Cluster
	Generator Generator
	Scheduler Scheduler

	// QualityAssurance wraps Cluster, Generator, and Scheduler with types
	// that perform a whole bunch of checks. This can significantly increase
	// the runtime of a simulation.
	QualityAssurance bool

	// A job will be rescheduled MaxSchedRetries before being rejected.
	// This does not apply to evictions.
	MaxSchedRetries int
	// Machines will be updated as late as possible if LazyCluster is true.
	LazyCluster bool

	// Every job should have a significant runtime overhead, such as 1ms.
	JobOverhead simt.Duration
	// When a submitted job blocks, this should also take a certain amount
	// of time.
	BlockingOverhead simt.Duration

	obs event.Observers
	eq  *event.CountingEventHeap
	cnt Counters
}

// NewSimulation returns a new Simulation object. The passed in Cluster,
// Generator, and Scheduler can be nil, but must not be once the simulation
// is started. Various other defaults are set here.
func NewSimulation(c Cluster, g Generator, s Scheduler) *Simulation {
	sim := &Simulation{
		Cluster:   c,
		Generator: g,
		Scheduler: s,

		QualityAssurance: false,

		MaxSchedRetries: 100,
		LazyCluster:     true,

		JobOverhead:      1 * simt.Millisecond,
		BlockingOverhead: 1 * simt.Millisecond,

		obs: event.NewObservers(),
		eq:  event.NewCountingEventHeap(32),
	}
	return sim
}

// Initialize is automatically called (by Master) before a simulation is started.
// It returns ErrSimulationInvalid is the simulation is not valid (per IsValid),
// and initializes Cluster and Scheduler, as well as sets up any quality assurance.
// As such, it should not be called multiple times.
func (s *Simulation) Initialize() error {
	if !s.IsValid() {
		return ErrSimulationInvalid
	}

	if s.QualityAssurance {
		s.Cluster = NewClusterQA(s.Cluster)
		s.Generator = NewGeneratorQA(s.Generator)
		s.Scheduler = NewSchedulerQA(s.Scheduler)
	}

	s.Scheduler.Initialize(s.Cluster)
	s.Cluster.Initialize(func(e *event.Event) {
		// Note: we receive only death events from the cluster observer,
		// and these might be in the past relative to our current simulation time.
		s.eq.Push(e)
	})
	return nil
}

// IsValid returns true when the simulation is set up sensibly.
func (s *Simulation) IsValid() bool {
	if s.Cluster == nil || s.Generator == nil || s.Scheduler == nil {
		return false
	}
	if s.Cluster.Len() == 0 {
		return false
	}
	return true
}

// Finalize frees as much memory as possible.
//
// NOTE: This happens after all observers are finalized, in case they
// still need something from the simulation.
func (s *Simulation) Finalize() error {
	ec := errs.NewCollector("simulation finalization failed")
	s.eq.Reset()
	for _, fn := range []func() error{
		s.Cluster.Finalize,
		s.Scheduler.Finalize,
		s.Generator.Finalize,
	} {
		ec.Collect(fn())
	}

	// TODO: Just to make sure!
	s.eq = nil
	s.Cluster = nil
	s.Scheduler = nil
	s.Generator = nil
	return ec.Error()
}

func (s *Simulation) Observers() event.Observers      { return s.obs }
func (s *Simulation) Time() simt.Time                 { return s.cnt.Time }
func (s *Simulation) Counters() *Counters             { return &s.cnt }
func (s *Simulation) Queue() *event.CountingEventHeap { return s.eq }

func (s *Simulation) loop(sf func(s *Counters) bool) {
	log := func() *log.Entry { return log.WithField("t", s.cnt.Time) }
	block := func(e *event.Event) {
		e.Type = event.Rejection
		e.Job.AddSched(e.Time, s.BlockingOverhead)
		s.cnt.Rejections++
		s.obs.Observe(e) // leave the system; may be out-of-order
	}
	reschedule := func(e *event.Event) {
		j := e.Job
		if j.NumSchedulingAttempts() > s.MaxSchedRetries {
			e.Type = event.Rejection
			s.cnt.Rejections++
			s.obs.Observe(e) // leave the system; may be out-of-order
		} else {
			s.cnt.Submissions++
			s.obs.ObserveType(event.Submission, e)
			err := s.Scheduler.Submit(e) // give up ownership
			if err != nil {              // job was rejected
				block(e)
			}
		}
	}

	// Inside this loop, we are guaranteed that the queue eq is never empty.
	s.eq.Push(s.Generator.Next())
	for sf(&s.cnt) {
		s.cnt.Events++

		// Departure events may be in the past, these we handle specially.
		var e *event.Event
		earliest := s.eq.NextTime()
		if earliest < s.cnt.Time {
			e = s.eq.Pop()
		} else {
			// We ask whether the scheduler has any items that come before the next
			// item in the queue. If no, then nil is returned, in which case we get
			// whatever is in the queue.
			e = s.Scheduler.Next(earliest)
			if e == nil {
				e = s.eq.Pop()
			}

			// Calling Next can have the effect that the cluster is updated, which
			// can result in a new departure event being put in the queue.
			// We can't assume therefore that the event time is less-or-equal to
			// the simulation time.
			if e.Time > s.cnt.Time {
				s.cnt.Time = e.Time
			}

			// Not updating the entire cluster at every time change saves us a lot
			// of computing power.
			if !s.LazyCluster {
				s.Cluster.Update(e.Time)
			}
		}

		// All observations of a type must happen before that type is handed off.
		// Rusts ownership model would help prevent bugs here, I think.
		switch e.Type {
		case event.Arrival:
			e.Job.Pod.Runtime += s.JobOverhead
			s.cnt.Arrivals++
			s.cnt.Submissions++
			s.obs.ObserveType(event.Arrival, e)
			s.obs.ObserveType(event.Submission, e)
			err := s.Scheduler.Submit(e) // give up ownership
			if err != nil {              // job was rejected
				block(e)
			}
			s.eq.Push(s.Generator.Next())
		case event.Assignment:
			s.cnt.Assignments++
			s.obs.Observe(e)
			s.Cluster.Assign(e) // give up ownership
		case event.Departure:
			s.cnt.Departures++
			s.obs.Observe(e) // may be out of order!
		case event.Observation:
			s.cnt.Observations++
			o := e.Data.(Observer)
			o.Observe(e)
			s.eq.Push(&event.Event{
				Time: o.Next(e.Time),
				Type: event.Observation,
				Job:  nil,
				Data: o,
			})
		case event.SchedulingError:
			s.cnt.SchedulingErrors++
			log().Warningf("SchedulingError (job %d): %s", e.Job.ID, e.Data.(error))
		case event.SchedulingConflict:
			s.cnt.SchedulingConflicts++
			s.obs.Observe(e)
			// Because a conflict is also a failure, we fall through. This will call
			// observe twice, once for SchedulingConflict and once for SchedulingFailure.
			// Observers should only subscribe to one or the other, not both.
			fallthrough
		case event.SchedulingFailure:
			s.cnt.SchedulingFailures++
			s.obs.ObserveType(event.SchedulingFailure, e) // e.Type could be SchedulingConflict
			reschedule(e)                                 // give up ownership
		case event.Eviction:
			s.cnt.Evictions++
			s.obs.Observe(e)
			s.cnt.Submissions++
			s.obs.ObserveType(event.Submission, e) // TODO: why?
			err := s.Scheduler.Submit(e)           // give up ownership
			if err != nil {                        // job was rejected
				block(e)
			}
		default:
			log().Fatalf("Unknown event.Type: %v", e.Type)
		}
	}

	// Since cluster might have been lazy, do one final update.
	// This can't hurt, and it means that if we free resources,
	// we don't have any problems with statistics later.
	s.Cluster.Update(s.cnt.Time)
}

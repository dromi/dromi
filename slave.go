// Copyright (c) 2015, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package dromi

import (
	"gitlab.com/dromi/dromi/event"
	"gitlab.com/dromi/dromi/script"
	"gitlab.com/dromi/dromi/simt"
)

// A Slave is an object that is registers with and enhances the functionality
// of a Master. It is used for gathering statistics and relaying information
// via web requests.
//
// It is illegal to use a slave with multiple masters: behavior is undefined.
//
// The implementation of slaves is inspired by the Cobra package by spf13.
type Slave struct {
	// Name has no functional influence on slaves, but can be useful for later
	// introspection by describing succintly what the slave does.
	Name string

	// Initialize is called upon start of a simulation.
	// As much work as possible should be done in this initialization
	// procedure, to keep memory usage as local as possible.
	//
	// Initialize should not ever be called directly, as this is done
	// automatically. This goes for Private as well.
	// Initialize is guaranteed to only be called once.
	Initialize  func(s *Simulation) error
	initialized bool // is set to true after initialization

	// Finalize is called when the simulation is complete, and serves to reign
	// in memory usage. As many unneeded references (especially to simulation
	// objects) should be removed, and data usage trimmed down to what is
	// needed for serve. If necessary, Finalize can also change the Serve
	// function at this point.
	//
	// The Finalize function is also called automatically for any private slaves.
	Finalize  func(self *Slave) error
	finalized bool // is set to true after finalization

	// Private is in effect, a sub-slave. This can be used by this slave,
	// and is initialized automatically when this slave is initialized.
	//
	// It is also finalized after this slave, in case this slave needs
	// something special from it.
	Private *Slave

	// Endpoint is the web endpoint suffix at which this slave should be
	// accessed.
	Endpoint string

	// Serve should return a struct, which will be used as input to
	// ServeStruct.
	// The type returned by Value can implement any of the following functions:
	//
	//  String() string
	//  Header() []string
	//  Record() []string
	//  MarshalCSV() ([]byte, error)
	//  MarshalJSON() ([]byte, error)
	//
	// What is passed out MUST be a copy of the data; while the simulation is
	// effectively paused while web requests are in progress, other slaves
	// (like plotter) may save the output of Serve.
	Serve    func() interface{}
	ServeMap map[string]func() interface{}

	Trigger event.Type
	Process simt.Process
	Observe func(e *event.Event)

	Reset func(t simt.Time)

	// Exempt can be set to true to prevent Reset from being called.
	// This is useful for functions that want to temporarily modify slaves.
	Exempt bool

	// master is an internal reference to the master that the slave is registered
	// with. This allows the slave to call functions such as master.ResetAll.
	master *Master
}

// initialize is called by master to initialize the slave.
//
// Initialization only happens once, slaves can't be reused.
// If s.Private is set, this will be initialized after s.
// If s.Observe is set, s is registered as a trigger-observer for all set
// triggers (except some that don't make sense).
// If s.IsObserver() returns true, a new event is pushed to the simulation queue.
func (s *Slave) initialize(m *Master) error {
	if s.initialized {
		panic("a slave should not be initialized multiple times")
	}

	// Initialize this slave first, who knows what it will do.
	s.master = m
	if s.Initialize != nil {
		if err := s.Initialize(m.simulation); err != nil {
			return err
		}
	}

	if s.Private != nil {
		s.Private.initialize(m)
	}

	if s.Observe != nil {
		// This might not do anything if s.Trigger does not match to any
		// triggerable event types; that is okay though.
		m.simulation.obs.Add(s.Trigger, s.Observe)
	}

	if s.IsObserver() {
		// s creates its own observation events, we create the first one.
		// The simulation will create the one after this.
		o := s.Observer()
		m.simulation.eq.Push(&event.Event{
			Time: o.Next(m.simulation.Time()),
			Type: event.Observation,
			Job:  nil,
			Data: o,
		})
	}

	s.initialized = true
	return nil
}

// finalize is called by master when a slave should be finalized.
func (s *Slave) finalize() error {
	if s.finalized {
		panic("slave cannot be finalized more than once")
	}

	if s.Finalize != nil {
		err := s.Finalize(s)
		if err != nil {
			return err
		}
	}
	if s.Private != nil {
		err := s.Private.finalize()
		if err != nil {
			return err
		}
	}

	s.finalized = true
	return nil
}

// AddInitializeFn replaces the current Initialize function with a function
// that calls the current Initialize function then fn.
func (s *Slave) AddInitializeFn(fn func(*Simulation) error) {
	if s.Initialize == nil {
		s.Initialize = fn
	} else {
		init := s.Initialize
		s.Initialize = func(sim *Simulation) error {
			if err := init(sim); err != nil {
				return err
			}
			return fn(sim)
		}
	}
}

func (s Slave) Endpoints() []string {
	if s.ServeMap == nil {
		return []string{s.Endpoint}
	}

	endpoints := make([]string, len(s.ServeMap))
	var i int
	for ep, _ := range s.ServeMap {
		endpoints[i] = s.Endpoint + ep
		i++
	}
	return endpoints
}

// IsServer returns true if the Value function will ever be called,
// which is if it has an endpoint and it can serve a value.
func (s Slave) IsServer() bool {
	return s.Endpoint != "" && (s.Serve != nil || s.ServeMap != nil)
}

// IsObserver returns true if the slave creates observation events.
//
// This requires that s.Trigger has event.Observation set, because s
// does not react to events, it creates them.
func (s Slave) IsObserver() bool {
	return s.Observe != nil && s.Process != nil && s.Trigger&event.Observation != 0
}

// Observer creates a new Observer event that can then be hooked into the simulation.
//
// NOTE: This function probably does not need to be used outside this file.
// But you never know, so for now it is exported.
func (s *Slave) Observer() Observer {
	if !s.IsObserver() {
		return nil
	}
	return &observer{
		next: s.Process.Next,
		obs:  s.Observe,
	}
}

type observer struct {
	obs  func(e *event.Event)
	next func(simt.Time) simt.Time
}

func (o observer) Observe(e *event.Event)     { o.obs(e) }
func (o observer) Next(t simt.Time) simt.Time { return o.next(t) }

// Resetter slave {{{1
func init() {
	script.CreateFunc("sim.resetter", Resetter)
}

// Resetter is a special slave that resets all other slaves at a specific point
// in time.
//
// Because it is not really an observer, it is not in obs. If custom resetters
// need to be written, this can be used as a prototype.
//
//  resetter := dromi.Resetter(0)
//  return &dromi.Slave{
//      Private: resetter, // resetter needs to be initialized
//      ...
//      Observe: func(e *event.Event) {
//          if ... {
//              // Now reset all other slaves:
//              resetter.Observe(nil)
//          }
//      },
//      ...
//  }
//
func Resetter(after simt.Duration) *Slave {
	s := &Slave{
		Name:    "Resetter",
		Trigger: event.Observation,
		Process: simt.Once(simt.Time(after)),
	}
	s.Observe = func(_ *event.Event) { s.master.ResetSlaves() }
	return s
}

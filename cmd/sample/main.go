// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package main

import (
	"fmt"
	"math/rand"
	_ "net/http/pprof"
	"os"
	"time"

	"gitlab.com/dromi/dromi/script"
	"gitlab.com/dromi/dromi/simt"

	"github.com/goulash/stat/dist"
	"github.com/spf13/cobra"
)

var (
	samples int
	seed    int64
	unit    string
	start   time.Duration
	value   interface{}
)

func init() {
	RootCmd.PersistentFlags().IntVarP(&samples, "samples", "n", 10, "the number of samples to generate")
	RootCmd.PersistentFlags().Int64Var(&seed, "seed", time.Now().UnixNano(), "number to seed random generator with")
	RootCmd.Flags().StringVarP(&unit, "unit", "u", "", "output numbers with this unit (={ns,us,ms,s,h})")
	RootCmd.Flags().DurationVar(&start, "start", 0, "time to start process output with")
}

func main() {
	err := RootCmd.Execute()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error:", err)
		os.Exit(1)
	}
}

var RootCmd = &cobra.Command{
	Use:   "sample <object function>",
	Short: "sample values from one of dromi's objects",
	Long: `Sampler is a simple tool to help inspect the output of Dromi distributions
and processes, or whatever creates a stream of values.

  Sampler expects a command and a DromiScript function to create an object.
  See the command "dromi script --help" for a list of functions that can be
  used to create processes.  The value RANDSRC can be used as a random source.

  This command can be used together with R to plot the results:

    xs <- system2("sample", "'(dist.ceil (dist.unif RANDSRC 0.0 10.0))' -n 1000", T)
    ys <- as.numeric(xs)
    plot(density(ys))

  This can also be condensed into a function:

    sample.dromi <- function(s, n=100, unit="ms") {
        xs <- system2("sample", paste("-n", n, "--unit", unit, "'", s, "'"), T)
        as.numeric(xs)
    }
`,
	PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
		if len(args) != 1 {
			return cmd.Usage()
		}

		// Get the object from the user
		code := fmt.Sprintf("(var VALUE %s)", args[0])
		env := script.New()
		env.Set("RANDSRC", rand.NewSource(seed))
		s, err := env.ExecString("-", code)
		if err != nil {
			return err
		}
		value, err = s.Get("VALUE")
		if err != nil {
			return err
		}

		return nil
	},
	RunE: func(cmd *cobra.Command, args []string) error {
		cmd.SilenceUsage = true

		// Figure out how to sample from it
		var fn func() simt.Duration
		switch t := value.(type) {
		case dist.Discrete:
			for i := 0; i < samples; i++ {
				fmt.Println(t.Int63())
			}
			return nil
		case dist.Continuous:
			for i := 0; i < samples; i++ {
				fmt.Println(t.Float64())
			}
			return nil
		case simt.Process:
			time := simt.Time(start)
			fn = func() simt.Duration {
				time = t.Next(time)
				return simt.Duration(time)
			}
			return printDurations(fn, samples)
		case simt.Distribution:
			return printDurations(t, samples)
		default:
			return fmt.Errorf("supplied functon must be a process or distribution, got %#v", value)
		}
	},
}

func printDurations(fn func() simt.Duration, n int) error {
	if unit == "" {
		for i := 0; i < n; i++ {
			d := fn()
			fmt.Println(d)
		}
	} else {
		u, err := simt.ParseDuration(fmt.Sprintf("1%s", unit))
		if err != nil {
			return err
		}
		div := float64(u)
		for i := 0; i < n; i++ {
			d := float64(fn())
			fmt.Println(d / div)
		}
	}
	return nil
}

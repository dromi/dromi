// Copyright (c) 2015, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"text/template"

	"gitlab.com/dromi/dromi/api"
	"github.com/goulash/osutil"
)

var rscriptTmpl = template.Must(indexTmpl.New("rscript").Parse(`

`))

type rscriptHandler struct {
	ep      string
	script  string
	address string
	ready   bool

	// These variables are to make sure only one request calls the R script.
	gen      bool
	genMutex sync.Mutex
	done     chan struct{}
	handler  http.Handler
}

func newRscriptHandler(ep string, ov *api.Overseer) (*rscriptHandler, error) {
	head := ov.Simulations[0]
	if _, err := exec.LookPath("Rscript"); err != nil {
		return nil, err
	}
	script, err := head.RealRscript()
	if err != nil {
		return nil, err
	}

	h := &rscriptHandler{
		ep:      ep,
		address: fmt.Sprintf("http://localhost:%d", Conf.Port),
		script:  script,
		ready:   head.RscriptAnytime,
		done:    make(chan struct{}),
	}

	// We want to know when all simulations are complete.
	if !head.RscriptAnytime {
		ov.NotifyFunc(func() { h.ready = true })
	}

	return h, nil
}

func (h *rscriptHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if !h.ready {
		http.Error(w, "Simulation study not complete yet", 400)
		return
	}

	h.genMutex.Lock()
	if h.gen {
		h.genMutex.Unlock()

		<-h.done
		h.handler.ServeHTTP(w, r)
		return
	}

	h.gen = true
	h.genMutex.Unlock()

	args := Env{
		"dpi":     100.0,
		"width":   5.0,
		"height":  5.0,
		"address": h.address,
	}

	dir, err := runRscript(h.script, "", args)
	if err != nil {
		if e, ok := err.(*CmdError); ok {
			http.Error(w, string(e.Output), 500)
		} else {
			http.Error(w, err.Error(), 500)
		}

		h.genMutex.Lock()
		close(h.done)
		h.done = make(chan struct{})
		h.gen = false
		h.genMutex.Unlock()
		return
	}

	h.handler = http.StripPrefix(h.ep, http.FileServer(http.Dir(dir)))
	close(h.done)
	h.handler.ServeHTTP(w, r)
}

func runRscript(script, dir string, args Env) (string, error) {
	if dir == "" {
		var err error
		dir, err = ioutil.TempDir("", "dromi")
		if err != nil {
			return dir, err
		}
	}
	ex, err := osutil.DirExists(dir)
	if err != nil {
		return dir, err
	}
	if !ex {
		err := os.MkdirAll(dir, 0600)
		if err != nil {
			return dir, err
		}
	}

	// Change into the destination directory
	wd, err := os.Getwd()
	if err != nil {
		return dir, err
	}
	err = os.Chdir(dir)
	if err != nil {
		return dir, err
	}
	defer os.Chdir(wd)

	// Copy script into destination directory
	err = osutil.CopyFile(script, filepath.Join(dir, filepath.Base(script)))
	if err != nil {
		return dir, err
	}

	// Run script
	cmd := exec.Command("Rscript", script)
	cmd.Env = args.List()
	cmd.Stdin = nil
	bs, err := cmd.CombinedOutput()
	if err != nil {
		return dir, &CmdError{bs, err}
	}
	return dir, nil
}

type CmdError struct {
	Output []byte
	Err    error
}

func (e *CmdError) Error() string {
	return e.Err.Error()
}

// ImgSize {{{

var (
	ErrEmpty         = errors.New("input string is empty")
	ErrInvalidFormat = errors.New("input string has invalid format")
)

type ImgSize struct {
	Width  int32
	Height int32
}

func (sz ImgSize) Inches(dpi float64) (w, h float64) {
	return float64(sz.Width) / dpi, float64(sz.Height) / dpi
}

func ParseImgSize(s string) (ImgSize, error) {
	if s == "" {
		return ImgSize{}, ErrEmpty
	}
	fs := strings.Split(s, "x")
	n := len(fs)
	if n > 2 {
		return ImgSize{}, ErrInvalidFormat
	}
	var w, h int64
	w, err := strconv.ParseInt(fs[0], 10, 32)
	if err != nil {
		return ImgSize{}, err
	}
	if n == 2 {
		h, err = strconv.ParseInt(fs[1], 10, 32)
		if err != nil {
			return ImgSize{}, err
		}
	} else {
		h = w
	}
	return ImgSize{int32(w), int32(h)}, nil
}

// }}}

// Query {{{

type Query struct {
	xs url.Values
}

func NewQuery(r *http.Request) *Query {
	return &Query{r.URL.Query()}
}

func (q *Query) Float64(name string, def float64) (float64, error) {
	v := q.xs.Get(name)
	if v == "" {
		return def, nil
	}
	return strconv.ParseFloat(v, 64)
}

func (q *Query) String(name string, def string) string {
	v := q.xs.Get(name)
	if v == "" {
		return def
	}
	return v
}

func (q *Query) ImgSize(name string, def ImgSize) (ImgSize, error) {
	v := q.xs.Get(name)
	if v == "" {
		return def, nil
	}
	return ParseImgSize(v)
}

// }}}

// Env {{{

type Env map[string]interface{}

func (e Env) List() []string {
	xs := make([]string, 0, len(e))
	for k, v := range e {
		xs = append(xs, fmt.Sprintf("DROMI_%s=%v", strings.ToUpper(k), v))
	}
	return xs
}

// }}}

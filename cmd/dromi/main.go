// Copyright (c) 2015, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package main

import (
	"errors"
	"fmt"
	_ "net/http/pprof"
	"os"
	"os/signal"
	"runtime/pprof"
	"strconv"
	"strings"

	"github.com/BurntSushi/toml"
	log "github.com/Sirupsen/logrus"
	"github.com/goulash/xdg"
	"github.com/spf13/cobra"
)

type Configuration struct {
	// Listen defines what port we should listen to for online statistics.
	// If left empty, nothing happens.
	Listen string `toml:"listen"`
	Port   int    `toml:"-"`

	// If Debug is true, debugging messages are printed.
	Debug bool `toml:"debug"`
}

// Default and global configuration
var Conf = &Configuration{
	Listen: ":8080",
	Port:   8080,
	Debug:  false,
}

var (
	profileCPU string
	profileMem string
)

func RootInit() {
	pf := RootCmd.PersistentFlags()
	pf.StringVar(&profileCPU, "cpu-profile", "", "write a cpu profile to this file")
	pf.StringVar(&profileMem, "mem-profile", "", "write a memory profile to this file")
	pf.StringVar(&Conf.Listen, "listen", Conf.Listen, "start an HTTP server and listen at this port")
	pf.BoolVar(&Conf.Debug, "debug", Conf.Debug, "print debugging messages")
}

var RootCmd = &cobra.Command{
	Use:   "dromi",
	Short: "simulate cluster schedulers",
	Long: `Dromi simulates and compares cluster schedulers.
It is primarily written for my master's thesis while at the University of Würzburg.
`,
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		if Conf.Debug {
			log.SetLevel(log.DebugLevel)
		}
		if Conf.Listen == "" {
			log.Fatal("Need an address to listen on")
		}
		i := strings.LastIndexByte(Conf.Listen, ':')
		port, err := strconv.Atoi(Conf.Listen[i+1:])
		if err != nil {
			log.Fatal(err.Error())
		}
		Conf.Port = port

		if profileCPU != "" {
			log.Info("Writing CPU profile.")
			f, err := os.Create(profileCPU)
			if err != nil {
				log.Fatal(err)
			}
			pprof.StartCPUProfile(f)
		}
	},
	PersistentPostRun: func(cmd *cobra.Command, args []string) {
		if profileCPU != "" {
			log.Info("Closing CPU profile.")
			pprof.StopCPUProfile()
		}
		if profileMem != "" {
			// This only writes a memory profile of whatever is in the memory after running everything.
			log.Info("Writing memory profile.")
			f, err := os.Create(profileMem)
			if err != nil {
				log.Errorln("Error creating memory profile:", err)
				return
			}
			defer f.Close()
			err = pprof.WriteHeapProfile(f)
			if err != nil {
				log.Error("Error writing heap profile:", err)
				return
			}
		}
	},
}

var Interrupt = make(chan os.Signal, 1)

func waitInterrupt() error {
	<-Interrupt
	fmt.Println()
	return errors.New("Interrupted.")
}

func main() {
	// Read configuration, if it exists.
	path := xdg.FindConfig("dromi/config.toml")
	if path != "" {
		log.Debugf("Reading configuration file %q...", path)
		_, err := toml.DecodeFile(path, Conf)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		log.Debug("No configuration file found.")
	}

	// Later commands should listen to Interrupt whenever doing something
	// that could take a while (like a simulation).
	signal.Notify(Interrupt, os.Interrupt)

	// Arguments from the command line override the configuration file,
	// so we have to add the flags after loading the configuration.
	RootInit()
	RootCmd.Execute()
}

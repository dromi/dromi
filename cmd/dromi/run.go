// Copyright (c) 2015, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package main

import (
	"bytes"
	"fmt"
	"math/rand"
	"net/http"
	"os"
	"os/exec"
	"runtime"
	"strconv"
	"strings"
	"text/template"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/goulash/pre"
	"github.com/justinas/alice"
	"github.com/spf13/cobra"
	"gitlab.com/dromi/dromi"
	"gitlab.com/dromi/dromi/api"
	"gitlab.com/dromi/dromi/simt"
	"gitlab.com/dromi/dromi/webutil"
)

var (
	parallel int
	postExec string
	wait     time.Duration
	exit     bool
)

func init() {
	RootCmd.AddCommand(runCmd)
	api.SetFlags(runCmd.Flags())
	runCmd.Flags().IntVar(&parallel, "parallel", runtime.NumCPU(), "number of simulations to run in parallel")
	runCmd.Flags().StringVar(&postExec, "exec", "", "execute command after simulation completion")
	runCmd.Flags().DurationVar(&wait, "wait", 0, "wait before exiting")
	runCmd.Flags().BoolVar(&exit, "exit", false, "exit after simulation completion")
}

var runCmd = &cobra.Command{
	Use:   "run <file>",
	Short: "Read configuration script and run simulation",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 1 {
			cmd.Usage()
			return
		}
		path := args[0]

		s, err := api.New(path)
		if err != nil {
			log.Errorln("Error reading configuration script:", err)
			return
		}

		err = runExperiments(s)
		if err != nil {
			log.Error(err)
		}
	},
}

func runExperiments(head *api.Simulation) error {
	xz, rz := head.Dimensions.Total(), head.Replications
	if xz == 0 {
		xz = 1
	}
	z := xz * head.Replications
	log.Infof("Running %d experiments with %d replications each for a total of %d runs.", xz, rz, z)

	// By default, what happens is that RANDSRC is created new every time
	// api.NewKeys is called. This should give a different RANDSRC for each
	// run of the simulation study.
	//
	// What we can alternatively do is, at the start of a study, set a
	// random value, and use this to create new sources for each run.
	// This is what AUTO_OVERRIDE does, when set to true.
	//
	// If we wish, we can try to correlate experiments, by using the
	// same random source for each i-th run of separate experiments.
	// This we do by getting head.Replications amount of random numbers
	// and using these for each rep.
	// ______________________________________________________________
	override := head.Keys["AUTO_OVERRIDE"].Get().(bool)
	correlate := head.Keys["CORRELATE"].Get().(bool)
	overrideFn := func(*api.Simulation, int, int) {}
	if override {
		log.Infof("Overriding RANDSRC variable for each run.")
		rnd := rand.New(rand.NewSource(time.Now().Unix()))
		if correlate {
			log.Infof("Correlating RANDSRC variables so they are the same on each i-th replication.")
			xs := make([]int64, head.Replications)
			for i := range xs {
				xs[i] = rnd.Int63()
			}
			overrideFn = func(s *api.Simulation, exp, rep int) {
				seed := xs[rep]
				log.Debugf("Overriding run-%d.%d RANDSRC with rand.NewSource(%d)", exp, rep, seed)
				s.Keys["RANDSRC"].Set(rand.NewSource(seed))
			}
		} else {
			overrideFn = func(s *api.Simulation, exp, rep int) {
				seed := rnd.Int63()
				log.Debugf("Overriding run-%d.%d RANDSRC with rand.NewSource(%d)", exp, rep, seed)
				s.Keys["RANDSRC"].Set(rand.NewSource(seed))
			}
		}
	}
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	// Thats what this was all about. We call overrideFn below in next.

	path := head.Path
	next := func(exp, rep int) (*api.Simulation, error) {
		s, err := api.New("")
		if err != nil {
			return nil, err
		}

		s.Keys["EXPERIMENT"].Set(int64(exp))
		s.Keys["REPL"].Set(int64(rep))
		s.Dimensions.Configure(head.Dimensions, true)
		overrideFn(s, exp, rep)
		err = s.Parse(path)
		return s, err
	}

	all := make([]*api.Simulation, z)
	var i int
	for j := 0; j < xz; j++ {
		// Replicate the experiment rz times and then move on to the next experiment.
		for k := 0; k < rz; k++ {
			s, err := next(j, k)
			if err != nil {
				return err
			}
			s.Master.SetID(i)
			all[i] = s
			i++
		}
		head.Dimensions.Next()
	}

	ov := api.NewOverseer(all)
	handle(ov)

	start := time.Now()
	ov.NotifyFunc(func() {
		log.Infof("Study completed in %v.", time.Now().Sub(start))
		if postExec != "" {
			log.Infof("Running: %s", postExec)
			cmd := exec.Command("sh", "-c", postExec)
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr
			err := cmd.Run()
			if err != nil {
				log.Errorln("Error running post hook:", err)
			}
		}
		if exit {
			if wait > 0 {
				log.Infof("Waiting %v before exiting...", wait)
				time.Sleep(wait)
			}
			log.Infof("Exiting.")
			os.Exit(0)
		}
	})
	err := ov.RunAsync(parallel)
	if err != nil {
		return err
	}

	go func() {
		// Start the web server:
		log.Info("Listening at ", Conf.Listen, ".")
		chain := alice.New(webutil.CachelessHandler, webutil.LoggingHandler)
		log.Fatal(http.ListenAndServe(Conf.Listen, chain.Then(nil)))
	}()

	return waitInterrupt()
}

func handle(ov *api.Overseer) {
	s := newStudy(ov)
	for _, x := range s.Experiments {
		for _, r := range x.Runs {
			m := r.Sim.Master
			m.SetEndpointPrefix(r.Endpoint)
			http.Handle(r.Endpoint+"/", http.StripPrefix(r.Endpoint, m))
		}
	}

	node, err := pre.New().Parse(ov.Simulations[0].Path)
	if err != nil {
		log.Errorln("Error reading config file:", err.Error())
	}
	http.Handle("/config", webutil.TextHandler(node.String()))

	rh, err := newRscriptHandler("/rscript/", ov)
	if err != nil {
		log.Errorln("Error initializing Rscript handler:", err.Error())
	} else {
		s.Rscript = true
		http.Handle("/rscript/", rh)
	}

	http.Handle("/study", webutil.StructHandler(s))
	http.Handle("/study/dims", webutil.StructHandler(ov.Simulations[0].Dimensions))
	http.Handle("/study/runs", runsHandler(s))
	http.Handle("/endpoints", epHandler(s))
	http.Handle("/", webutil.TemplateHandler(indexTmpl, s))
}

func epHandler(s *study) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		endpoints := []string{"/", "/endpoints", "/config", "/study", "/study/dims", "/study/runs"}
		for _, x := range s.Experiments {
			for _, r := range x.Runs {
				endpoints = append(endpoints, r.Sim.Master.Endpoints()...)
			}
		}

		for _, ep := range endpoints {
			fmt.Fprintln(w, ep)
		}
	})
}

func runsHandler(s *study) http.Handler {
	runs := make([]run, s.NumRuns)
	var k int
	for i, x := range s.Experiments {
		for j, r := range x.Runs {
			runs[k] = run{
				ID:       k,
				Exp:      i,
				Repl:     j,
				Sim:      r.Sim,
				Endpoint: r.Endpoint,
			}
			k++
		}
	}
	return webutil.StructHandler(runs)
}

type run struct {
	ID       int
	Exp      int
	Repl     int
	Sim      *api.Simulation
	Endpoint string
}

func (r run) Header() []string {
	header := r.Sim.Parameters.Header()

	h := make([]string, 0, 5+len(header)*2)
	h = append(h, "id", "exp", "repl", "endpoint", "progress")
	for _, s := range header {
		label := strings.ToUpper(s)
		h = append(h, label, label+".val")
	}
	return h
}

func (r run) Record() []string {
	indices := r.Sim.Parameters.Indices()
	values := r.Sim.Parameters.Record()
	rec := make([]string, 0, 5+len(indices)*2)
	rec = append(rec,
		strconv.FormatInt(int64(r.ID), 10),
		strconv.FormatInt(int64(r.Exp), 10),
		strconv.FormatInt(int64(r.Repl), 10),
		r.Endpoint,
		strconv.FormatFloat(r.Sim.Master.ETA().Float64(), 'f', 1, 64),
	)
	for i, idx := range indices {
		rec = append(rec, strconv.FormatInt(int64(idx), 10), values[i])
	}
	return rec
}

func (r run) MarshalJSON() ([]byte, error) {
	header := r.Header()
	record := r.Record()
	m := len(header) - 1

	var buf bytes.Buffer
	writeEntry := func(i int) {
		buf.WriteRune('"')
		buf.WriteString(header[i])
		buf.WriteRune('"')
		buf.WriteRune(':')
		buf.WriteRune('"')
		buf.WriteString(record[i])
		buf.WriteRune('"')
	}

	buf.WriteRune('{')
	for i := 0; i < m; i++ {
		writeEntry(i)
		buf.WriteRune(',')
	}
	writeEntry(m)
	buf.WriteRune('}')
	return buf.Bytes(), nil
}

type study struct {
	Name    string `json:"name"`
	Desc    string `json:"description"`
	Rscript bool   `json:"rscript"`

	NumDimensions   int `json:"numDimensions"`
	NumExperiments  int `json:"numExperiments"`
	NumReplications int `json:"numReplications"`
	NumRuns         int `json:"numRuns"`
	NumParallel     int `json:"numParallel"`

	SimulationDuration simt.Duration `json:"simulationDuration"`
	ResetTime          simt.Duration `json:"resetTime"`

	DromiVersion string `json:"dromiVersion"`

	Experiments []studyExperiment `json:"experiments"`
	Overseer    *api.Overseer     `json:"-"`
}

func newStudy(ov *api.Overseer) *study {
	all := ov.Simulations
	head, n := all[0], len(all)

	// The following data should stay the same across all runs.
	data := study{
		Name: head.Name,
		Desc: head.Desc,

		NumDimensions:   head.Dimensions.Len(),
		NumExperiments:  head.Dimensions.Total(),
		NumReplications: head.Replications,
		NumRuns:         n,
		NumParallel:     parallel,

		SimulationDuration: head.Duration,
		ResetTime:          head.Keys["RESET_TIME"].Get().(simt.Duration),

		DromiVersion: progInfo.Version,

		Experiments: make([]studyExperiment, head.Dimensions.Total()),
		Overseer:    ov,
	}

	var i int
	for j := 0; j < data.NumExperiments; j++ {
		data.Experiments[j] = studyExperiment{
			Index:  j,
			Params: all[i].Parameters,
			Runs:   make([]studyRun, head.Replications),
		}
		for k := 0; k < head.Replications; k++ {
			data.Experiments[j].Runs[k] = studyRun{
				Index:    k,
				Endpoint: fmt.Sprintf("/run-%d.%d", j, k),
				Sim:      all[i],
			}
			i++
		}
	}

	return &data
}

type studyExperiment struct {
	Index  int        `json:"index"`
	Params api.Point  `json:"parameters"`
	Runs   []studyRun `json:"runs"`
}

type studyRun struct {
	Index    int             `json:"index"`
	Endpoint string          `json:"endpoint"`
	Sim      *api.Simulation `json:"-"`
}

func (s study) Header() []string {
	return []string{
		"name", "description", "rscript",
		"numDimensions", "numExperiments", "numReplications", "numRuns", "numParallel",
		"simulationDuration", "resetTime", "dromiVersion",
	}
}

func (s study) Record() []string {
	return []string{
		s.Name,
		s.Desc,
		fmt.Sprintf("%v", s.Rscript),
		strconv.FormatInt(int64(s.NumDimensions), 10),
		strconv.FormatInt(int64(s.NumExperiments), 10),
		strconv.FormatInt(int64(s.NumReplications), 10),
		strconv.FormatInt(int64(s.NumRuns), 10),
		strconv.FormatInt(int64(s.NumParallel), 10),
		s.SimulationDuration.String(),
		s.ResetTime.String(),
		s.DromiVersion,
	}
}

var indexTmpl = template.Must(dromi.Tmpl.New("index").Parse(`<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Dromi</title>
    <style>
        body {
            font-family: monospace;
        }

        #endpoints {
            margin-left: 4em;
        }

        .experiment span{
            margin-left: 2em;
            white-space: pre;
        }
    </style>
</head>
<body>
<b>Welcome to Dromi!</b>

{{if .Name}} <p>Study: {{.Name}} {{if .Desc}}({{.Desc}}){{end}}</p> {{end}}

<p>
A study with {{.NumExperiments}} experiment(s) and {{.NumReplications}} replication(s) is in progress.<br>
There are a total of {{.NumRuns}} runs;
{{if (eq .NumParallel 0)}}
    all of them run at the same time.
{{else}}
    only {{.NumParallel}} of them run at a time.
{{end}}
</p>

<p style="white-space: pre; margin-left: 0.2em">{{.Overseer.Progress.BarWithSpecialETA 50}}</p>

<div id=endpoints>
<p class=experiment>Overview:<br>
    <span><a href="/config" title="Preprocessed configuration file">/config</a></span><br>
    <span><a href="/endpoints" title="A list of all endpoints (for mirroring)">/endpoints</a></span><br>
    <span><a href="/study" title="Study configuration">/study</a></span><br>
    <span><a href="/study/dims" title="A list of dimension names in the study">/study/dims</a></span><br>
    <span><a href="/study/runs" title="A list of all the runs in the study">/study/runs</a></span><br>
</p>
{{if .Rscript}}<p>Results: <a href="/rscript/">/rscript</a></p>{{end}}
{{range .Experiments}}
    <p class=experiment>
    Experiment {{.Params.String}}<br>
    {{range .Runs}}
        <span><a href="{{.Endpoint}}">{{.Endpoint}}</a>: {{.Sim.Master.ETA.BarWithSpecialETA 30}}</span><br>
    {{end}}
    </p>
{{end}}
</div>

</body>
</html>
`))

// Copyright (c) 2015, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package main

import (
	"bytes"
	"fmt"
	"os"
	"text/template"

	"gopkg.in/twik.v1"

	"github.com/goulash/pre/ast"
	"github.com/goulash/twikutil/key"
	"github.com/spf13/cobra"
	"gitlab.com/dromi/dromi/api"
	"gitlab.com/dromi/dromi/script"

	// Include DromiScript functions not normally included
	_ "gitlab.com/dromi/dromi/presets"
	_ "gitlab.com/dromi/dromi/presets/thesis"
)

func init() {
	RootCmd.AddCommand(scriptCmd)
}

var scriptTmpl = template.Must(template.New("script").Parse(
	`The script command is used to test configuration scripts for correctness.

  DromiScript is a Lisp-like domain specific language (DSL) which is used to
  configure simulation studies. All features of the language are accessed
  through functions, which makes it easy to learn, use, and extend.
  What follows is a short overview.

  DromiScript is an extension of the Twik language: https://github.com/go-twik

  Dromi defines several variables for you, the values of which are determined
  through DromiScript configuration files and command-line options.
  These values can also be overridden, through the "set" function.
    {{range .ReadWrite}}
    {{.}}{{end}}

  Dromi requires that several variables are created (with the "var" function)
  in the configuration script. These define the simulation parameters.
    {{range .ReadOnly}}
    {{.}}{{end}}

  There are also several optional variables you can define:
    {{range .ReadOnlyOptional}}
    {{.}}{{end}}

  DromiScript contains the following predefined functions from Twik:

    true : bool
    false : bool
    nil : nil
    error :: string => error
    == :: {} -> {} => bool
    != :: {} -> {} => bool
    + :: {} -> {} => {}
    - :: {} -> {} => {}
    * :: {} -> {} => {}
    / :: {} -> {} => {}
    or :: bool -> bool => bool
    and :: bool -> bool => bool
    if :: bool -> func -> func => ()
    var :: {} => ()
    do :: ...{} => ()
    for :: ?
    range :: ?

  The following functions are made available to interface with Dromi.
  They are prefixed with "<group>:" to help identify their origin and purpose,
  they are called without this prefix.

    {{range .Library}}{{.}}
    {{end}}
  Example script:

    ; Some simple functions to make changes easier.
    (func def-gen (p) (gen.podder (preset.podder-simple RANDSRC) p))
    (func def-cluster (h) (cl.uniform h (exec.besteffort) CLUST_SIZE))
    (let net.ethernet1G (net.gbps 1.0))
    (let hw.server (hw 12.0 16000 net.ethernet1G 2000 256))

    ; We set all the mandatory variables.
    (let CLUSTER (def-cluster hw.server))
    (let GENERATOR (def-gen (proc.exp RANDSRC 100.0 (dur "1s"))))
    (let SCHEDULER (sf.monolith (sched.first)))
    (let SLAVES (list
        (obs.cluster-stats)
        (obs|plotter (proc.fib (dur "1ms")) (obs.cluster-stats))
        (obs|plotter (proc.fib (dur "1ms")) (obs|refresher (obs.cluster-stats)))
        (sim.resetter (dur "1h"))
    ))
`))

var scriptCmd = &cobra.Command{
	Use:   "script file...",
	Short: "Evaluate the given DromiScript configuration files for correctness",
	Long: func() string {
		data := struct {
			Library          []string
			ReadWrite        []string
			ReadOnly         []string
			ReadOnlyOptional []string
		}{
			Library: script.LibraryUsage(),
		}
		keys := api.NewKeys()

		var max int
		for _, k := range keys.Keys() {
			m := k.Mode()
			name := k.Name()
			if m&key.ReadWrite == key.ReadWrite {
				data.ReadWrite = append(data.ReadWrite, name)
			} else if m == key.Read|key.Required {
				data.ReadOnly = append(data.ReadOnly, name)
			} else if m == key.Read {
				data.ReadOnlyOptional = append(data.ReadOnlyOptional, name)
			} else {
				continue
			}
			if len(name) > max {
				max = len(name)
			}
		}

		fs := fmt.Sprintf("%%%ds : %%s", max)
		format := func(ks *[]string) {
			for i, k := range *ks {
				(*ks)[i] = fmt.Sprintf(fs, k, keys[k].Type())
			}
		}
		format(&data.ReadWrite)
		format(&data.ReadOnly)
		format(&data.ReadOnlyOptional)

		var buf bytes.Buffer
		Must(scriptTmpl.Execute(&buf, data))
		return buf.String()
	}(),
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			cmd.Usage()
			return
		}

		test := func(path string) error {
			s, err := api.New("")
			if err != nil {
				return err
			}

			good := true
			err = s.ParseWithHandler(path, func(err error) error {
				if err != nil {
					good = false
					fmt.Printf("%s: %s\n", path, err)
				}
				return nil
			})
			if err != nil && err != api.ErrFailed {
				return err
			}

			if good {
				fmt.Printf("%s: OK\n", path)
			}
			return nil
		}

		for _, path := range args {
			err := test(path)
			if err != nil {
				switch err.(type) {
				case *twik.Error, *ast.Error:
					// These errors are prefixed with
					fmt.Fprintln(os.Stderr, err)
				default:
					fmt.Fprintln(os.Stderr, "Error:", err)
				}
			}
		}
	},
}

func init() { scriptCmd.AddCommand(scriptListCmd) }

var scriptListCmd = &cobra.Command{
	Use:   "list",
	Short: "List all functions made available through DromiScript",
	Run: func(cmd *cobra.Command, args []string) {
		hs := script.LibraryUsage()
		for _, h := range hs {
			fmt.Println(h)
		}
	},
}

func Must(err error) {
	if err != nil {
		panic(err)
	}
}

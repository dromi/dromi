// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package volt

import "github.com/boltdb/bolt"

type DB struct {
	file string
	back *bolt.DB
}

func Open(file string) (*DB, error) {
	db, err := bolt.Open(file, 0644, nil)
	if err != nil {
		return nil, err
	}
	return &DB{file: file, back: db}, nil
}

func (db *DB) Close() {
	db.back.Close()
}

func (db *DB) View(f func(tx *Tx)) {
	err := db.back.View(func(z *bolt.Tx) error {
		tx := &Tx{z}
		f(tx)
		return nil
	})
	if err != nil {
		panic(err)
	}
}

func (db *DB) ViewErr(f func(tx *Tx) error) error {
	return db.back.View(func(z *bolt.Tx) error {
		tx := &Tx{z}
		return f(tx)
	})
}

func (db *DB) Update(f func(tx *Tx)) {
	err := db.back.Update(func(z *bolt.Tx) error {
		tx := &Tx{z}
		f(tx)
		return nil
	})
	if err != nil {
		panic(err)
	}
}

func (db *DB) UpdateErr(f func(tx *Tx) error) error {
	return db.back.Update(func(z *bolt.Tx) error {
		tx := &Tx{z}
		return f(tx)
	})
}

func (db *DB) Batch(f func(tx *Tx)) {
	err := db.back.Batch(func(z *bolt.Tx) error {
		tx := &Tx{z}
		f(tx)
		return nil
	})
	if err != nil {
		panic(err)
	}
}

func (db *DB) BatchErr(f func(tx *Tx) error) error {
	return db.back.Batch(func(z *bolt.Tx) error {
		tx := &Tx{z}
		return f(tx)
	})
}

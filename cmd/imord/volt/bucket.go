// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package volt

import (
	"strconv"

	"github.com/boltdb/bolt"
)

type Tx struct {
	*bolt.Tx
}

func (t *Tx) Bucket(key string) *Bucket {
	v := t.Tx.Bucket([]byte(key))
	if v == nil {
		return nil
	}
	return &Bucket{v}
}

func (t *Tx) BucketPath(keys ...string) *Bucket {
	if len(keys) == 0 {
		return nil
	}

	b := t.Bucket(keys[0])
	for _, k := range keys[1:] {
		if b == nil {
			break
		}
		b = b.Bucket(k)
	}
	return b
}

func (t *Tx) CreateBucket(key string) (*Bucket, error) {
	v, err := t.Tx.CreateBucket([]byte(key))
	if v == nil {
		return nil, err
	}
	return &Bucket{v}, err
}

func (t *Tx) CreateBucketIfNotExists(key string) (*Bucket, error) {
	b, err := t.Tx.CreateBucketIfNotExists([]byte(key))
	if b == nil {
		return nil, err
	}
	return &Bucket{b}, err
}

func (t *Tx) DeleteBucket(key string) error {
	return t.Tx.DeleteBucket([]byte(key))
}

type Bucket struct {
	buck *bolt.Bucket
}

func (b *Bucket) Bucket(key string) *Bucket {
	v := b.buck.Bucket([]byte(key))
	if v == nil {
		return nil
	}
	return &Bucket{v}
}

func (b *Bucket) BucketPath(keys ...string) *Bucket {
	for _, k := range keys {
		if b == nil {
			break
		}
		b = b.Bucket(k)
	}
	return b
}

func (b *Bucket) CreateBucket(key string) (*Bucket, error) {
	v, err := b.buck.CreateBucket([]byte(key))
	if v == nil {
		return nil, err
	}
	return &Bucket{v}, err
}

func (b *Bucket) CreateBucketIfNotExists(key string) (*Bucket, error) {
	v, err := b.buck.CreateBucketIfNotExists([]byte(key))
	if v == nil {
		return nil, err
	}
	return &Bucket{v}, err
}

func (b *Bucket) Delete(key string) error {
	return b.buck.Delete([]byte(key))
}

func (b *Bucket) DeleteBucket(key string) error {
	return b.buck.DeleteBucket([]byte(key))
}

func (b *Bucket) Buckets() []string {
	keys := make([]string, 0)
	b.ForEach(func(k string, v []byte) {
		if v == nil {
			keys = append(keys, k)
		}
	})
	return keys
}

func (b *Bucket) Keys() []string {
	keys := make([]string, 0)
	b.ForEach(func(k string, v []byte) {
		if v != nil {
			keys = append(keys, k)
		}
	})
	return keys
}

func (b *Bucket) ForEach(fn func(k string, v []byte)) {
	b.buck.ForEach(func(k, v []byte) error {
		fn(string(k), v)
		return nil
	})
}

func (b *Bucket) Has(key string) bool {
	return b.buck.Get([]byte(key)) != nil
}

func (b *Bucket) Put(key string, data []byte) error {
	return b.buck.Put([]byte(key), data)
}

func (b *Bucket) PutBool(key string, data bool) error {
	bs := []byte("false")
	if data {
		bs = []byte("true")
	}
	return b.buck.Put([]byte(key), bs)
}

func (b *Bucket) PutString(key, data string) error {
	return b.buck.Put([]byte(key), []byte(data))
}

func (b *Bucket) PutInt64(key string, data int64) error {
	xs := strconv.FormatInt(data, 10)
	return b.buck.Put([]byte(key), []byte(xs))
}

func (b *Bucket) IncInt64(key string, v int64) error {
	i, _ := b.GetInt64(key)
	return b.PutInt64(key, i+v)
}

func (b *Bucket) Get(key string) ([]byte, bool) {
	v := b.buck.Get([]byte(key))
	if v == nil {
		return nil, false
	}
	return v, true
}

func (b *Bucket) GetBool(key string) (bool, bool) {
	v := b.buck.Get([]byte(key))
	if v == nil {
		return false, false
	}
	return string(v) == "true", true
}

func (b *Bucket) GetString(key string) (string, bool) {
	v := b.buck.Get([]byte(key))
	if v == nil {
		return "", false
	}
	return string(v), true
}

func (b *Bucket) GetInt64(key string) (int64, bool) {
	v := b.buck.Get([]byte(key))
	if v == nil {
		return 0, false
	}
	i, err := strconv.ParseInt(string(v), 10, 64)
	if err != nil {
		panic(err)
	}
	return i, true
}

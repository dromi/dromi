// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package main

import (
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"text/template"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/dromi/dromi"
	"gitlab.com/dromi/dromi/webutil"
)

func init() {
	RootCmd.AddCommand(serveCmd)
	serveCmd.Flags().StringVar(&Conf.Listen, "listen", Conf.Listen, "listen at this port")
}

var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Serve the sessions stored in the file",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) > 1 {
			cmd.Usage()
			return
		}

		var err error
		if len(args) == 1 {
			var session *Session
			if args[0] == "latest" {
				session, err = DB.GetLatestSession()
			} else {
				session, err = DB.GetSessionS(args[0])
			}
			if err != nil {
				log.Error(err.Error())
				return
			}
			err = serveOne(session)
		} else {
			err = serveAll(DB.ListSessions())
		}
		if err != nil {
			log.Errorln(err)
		}
	},
}

func serveOne(s *Session) error {
	http.Handle("/", sessionHandler(s))
	log.Infof("Serving single session at %s...", Conf.Listen)
	go http.ListenAndServe(Conf.Listen, nil)
	signal.Notify(Interrupt, os.Interrupt)
	return waitInterrupt()
}

func serveAll(sessions []*Session) error {
	// Root template:
	sdata := make([]struct {
		ID     int64
		String string
	}, len(sessions))
	mb := 1024 * 1024
	for i, s := range sessions {
		n := s.Len()
		real, disk := s.Size()
		sdata[i].String = fmt.Sprintf(sessionFormat, s.ID(), s.Date().Format(time.Stamp), n, real/mb, disk/mb, s.Label())
		sdata[i].ID = s.ID()
	}
	data := struct {
		Header   string
		HeaderHR string
		Sessions []struct {
			ID     int64
			String string
		}
	}{
		Header:   sessionHeader,
		HeaderHR: sessionHeaderHR,
		Sessions: sdata,
	}
	http.Handle("/", webutil.TemplateHandler(serveTmpl, data))
	for _, s := range sessions {
		ep := fmt.Sprintf("/%d", s.ID())
		http.Handle(ep+"/", http.StripPrefix(ep, sessionHandler(s)))
	}
	log.Infof("Listening at %s...", Conf.Listen)
	go http.ListenAndServe(Conf.Listen, nil)
	signal.Notify(Interrupt, os.Interrupt)
	return waitInterrupt()
}

func sessionHandler(s *Session) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		bs, ok := s.Get(r.URL.Path)
		if !ok {
			http.Error(w, "path does not exist", 400)
			return
		}
		page, err := NewPageFromBytes(bs)
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}
		page.ServeHTTP(w, r)
	})
}

var serveTmpl = template.Must(dromi.Tmpl.New("index").Parse(`<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Imord</title>
    <style>
        body {
            font-family: monospace;
        }

        #sessions {
            margin-left: 4em;
        }

        .session {
            white-space: pre;
        }

        .session.header {
            font-weight: bold;
        }

        .session.br {
            margin-top: -12px;
            margin-bottom: -12px;
        }

        .session:hover {
            background-color: lightgray;
        }
    </style>
</head>
<body>
<b>Welcome to IMORD!</b>

<div id=sessions>
    <p class="session header">        {{.Header}}</p>
    <p class="session br">        {{.HeaderHR}}</p>
{{range .Sessions}}
    <p class=session><a href="/{{.ID}}">session</a> {{.String}}</p>
{{end}}
</div>

</body>
</html>
`))

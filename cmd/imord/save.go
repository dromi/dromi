// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"sync"

	_ "net/http/pprof"

	"github.com/Jeffail/tunny"
	log "github.com/Sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/dromi/dromi/webutil"
)

var (
	parallel int
	label    string
	ignore   []string
	compress bool
)

func init() {
	RootCmd.AddCommand(saveCmd)
	saveCmd.Flags().StringSliceVar(&ignore, "ignore", []string{}, "ignore page types")
	saveCmd.Flags().StringVarP(&label, "label", "L", "", "label for this data set")
	saveCmd.Flags().IntVar(&parallel, "parallel", runtime.NumCPU(), "number of endpoints to fetch at a time")
	saveCmd.Flags().BoolVarP(&compress, "compress", "z", true, "compress data on saving")
}

var saveCmd = &cobra.Command{
	Use:   "save [url]",
	Short: "Save an offline file copy of a Dromi instance",
	Long: `The save command saves all the web-pages of a Dromi instance.

  It does this by loading /endpoints and fetching every single page listed there.
  It also takes into account ?type=json and ?type=csv arguments.

  It is recommended to wait until the Dromi instance in question is completely
  done simulating.`,
	PreRun: func(cmd *cobra.Command, args []string) {
		setPageTypesIgnore(ignore)
		DB.CompressNew = compress

		if Conf.Debug {
			go func() {
				log.Errorln(http.ListenAndServe(":9096", nil))
			}()
		}
	},
	Run: func(cmd *cobra.Command, args []string) {
		var err error
		switch len(args) {
		case 0:
			urlpath := "http://localhost:8080/endpoints"
			log.Infof("Saving from default location %s", urlpath)
			err = Save(urlpath, label, parallel)
		case 1:
			err = Save(args[0], label, parallel)
		default:
			cmd.Usage()
			return
		}

		if err != nil {
			log.Error(err)
		}
	},
}

func Save(urlpath, label string, parallel int) error {
	// Create a pool of workers, so we can control how many
	// endpoints are downloaded at a time.
	if parallel <= 0 {
		parallel = 1
	}
	pool, err := tunny.CreatePoolGeneric(parallel).Open()
	if err != nil {
		return err
	}
	defer pool.Close()

	// First read URL, which should simply contain endpoints.
	uu, err := url.Parse(urlpath)
	if err != nil {
		return err
	}
	shost := fmt.Sprintf("%s://%s", uu.Scheme, uu.Host)

	r, err := http.Get(urlpath)
	if err != nil {
		return err
	}
	eps, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}
	r.Body.Close()

	// Read each endpoint and write it into the database.
	s, err := DB.CreateSession(label)
	if err != nil {
		return err
	}
	endpoints := strings.Split(string(eps), "\n")
	var wg sync.WaitGroup
	var eta webutil.SyncETA
	eta.Add(len(endpoints))
	eta.Every(0.001, func(e webutil.ETA) {
		fmt.Printf("\r%s     ", e.BarWithSpecialETA(40))
	})
	eta.Begin()
	for _, ep := range endpoints {
		if ep == "" {
			eta.Done()
			continue
		}

		wg.Add(1)
		pool.SendWorkAsync(func(endpoint string) func() {
			return func() {
				log.Debugf("Downloading: %s", endpoint)
				p, err := NewPage(shost, endpoint)
				if err != nil {
					log.Errorf("Error downloading endpoint %s: %s", endpoint, err)
					return
				}
				err = s.Put(endpoint, p)
				if err != nil {
					log.Fatalf("Error persisting endpoint: %s", err)
				}
				eta.Done()
				wg.Done()
			}
		}(ep), nil)
	}

	// Wait until we are done with everything.
	signal.Notify(Interrupt, os.Interrupt)
	done := make(chan struct{})
	go func() {
		wg.Wait()
		eta.End()
		fmt.Printf("\r%s     \n", eta.ETA().BarWithSpecialETA(40))
		close(done)
	}()
	select {
	case <-Interrupt:
		log.Fatal("Interrupted.")
		return nil
	case <-done:
		return nil
	}
}

// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package main

import (
	"bytes"
	"errors"
	"io"
	"strconv"
	"time"

	cmp "compress/gzip"

	log "github.com/Sirupsen/logrus"
	"gitlab.com/dromi/dromi/cmd/imord/volt"
)

const (
	TimeFormat = "2006-01-02 15:04:05.999999999 -0700 MST"
)

var ErrDatabaseWrongVersion = errors.New("database needs to be upgraded")

var DatabaseVersion int64 = 2
var upgradesFrom = map[int64]func(*Database) error{
	// Upgrade to version 1 includes:
	//  - new fields "map_length", "disk_bytes", and "bytes" for each session
	1: func(d *Database) error {
		log.Info("Upgrading database to version 2.")

		err := d.db.UpdateErr(func(tx *volt.Tx) error {
			bs := tx.Bucket("/").Buckets()
			for _, id := range bs {
				b := tx.BucketPath("/", id)
				compressed, _ := b.GetBool("compress")
				var mapLength, diskBytes, realBytes int64
				b.Bucket("map").ForEach(func(k string, v []byte) {
					mapLength++
					diskBytes += int64(len(v))

					// Get the uncompressed size
					if compressed {
						var buf bytes.Buffer
						r, err := cmp.NewReader(bytes.NewReader(v))
						if err != nil {
							log.Fatal(err)
						}
						io.Copy(&buf, r)
						r.Close()
						realBytes += int64(buf.Len())
					} else {
						realBytes += diskBytes
					}
				})
				b.PutInt64("map_length", mapLength)
				b.PutInt64("disk_bytes", diskBytes)
				b.PutInt64("real_bytes", realBytes)
			}
			return nil
		})
		if err != nil {
			return err
		}

		idx := d.Index()
		if idx.Next() == 0 {
			// Starting index should be 1 not 0.
			idx.SetNext(1)
		}
		idx.SetVersion(2)
		return nil
	},
}

type Database struct {
	// CompressNew is an option that when true causes new sessions to be stored
	// in a compressed xz format. By default it is false.
	CompressNew bool

	db *volt.DB
}

func OpenDatabase(file string) (*Database, error) {
	db, err := volt.Open(file)
	if err != nil {
		return nil, err
	}

	d := &Database{db: db}
	err = d.Init()
	return d, err
}

func (d *Database) Close() {
	d.db.Close()
}

func (d *Database) Init() error {
	var version int64
	err := d.db.UpdateErr(func(tx *volt.Tx) error {
		_, err := tx.CreateBucketIfNotExists("/")
		if err != nil {
			return err
		}

		b, err := tx.CreateBucketIfNotExists("index")
		if err != nil {
			return err
		}

		version, _ = b.GetInt64("version")
		return nil
	})
	if err != nil {
		return err
	}

	if version == 0 {
		return d.create()
	} else if version < DatabaseVersion {
		return ErrDatabaseWrongVersion
	}
	return nil
}

func (d *Database) Upgrade() error {
	for i := d.Index().Version(); i < DatabaseVersion; i++ {
		f := upgradesFrom[i]
		if f != nil {
			if err := f(d); err != nil {
				return err
			}
		}
	}
	return nil
}

func (d *Database) create() error {
	idx := d.Index()
	idx.SetVersion(DatabaseVersion)
	idx.SetSize(0)
	idx.SetNext(1)
	return nil
}

func (d *Database) Index() *DatabaseIndex {
	return &DatabaseIndex{d.db}
}

type DatabaseIndex struct {
	db *volt.DB
}

func (di *DatabaseIndex) getInt64(key string) int64 {
	var idx int64
	di.db.View(func(tx *volt.Tx) {
		b := tx.Bucket("index")
		idx, _ = b.GetInt64(key)
	})
	return idx
}

func (di *DatabaseIndex) setInt64(key string, i int64) {
	di.db.Update(func(tx *volt.Tx) {
		b := tx.Bucket("index")
		err := b.PutInt64(key, i)
		if err != nil {
			panic(err)
		}
	})
}

func (di *DatabaseIndex) addInt64(key string, v int64) {
	di.db.Update(func(tx *volt.Tx) {
		b := tx.Bucket("index")
		i, _ := b.GetInt64(key)
		err := b.PutInt64(key, i+v)
		if err != nil {
			panic(err)
		}
	})
}

func (di *DatabaseIndex) Version() int64     { return di.getInt64("version") }
func (di *DatabaseIndex) SetVersion(i int64) { di.setInt64("version", i) }
func (di *DatabaseIndex) Next() int64        { return di.getInt64("next") }
func (di *DatabaseIndex) SetNext(i int64)    { di.setInt64("next", i) }
func (di *DatabaseIndex) IncNext(v int64)    { di.addInt64("next", v) }
func (di *DatabaseIndex) Size() int64        { return di.getInt64("size") }
func (di *DatabaseIndex) SetSize(i int64)    { di.setInt64("size", i) }
func (di *DatabaseIndex) IncSize(v int64)    { di.addInt64("size", v) }

func (d *Database) NumSessions() int {
	return int(d.Index().Size())
}

var (
	ErrSessionExists    = errors.New("session already exists")
	ErrSessionNotExists = errors.New("session does not exist")
)

func (d *Database) ImportSession(x *Session) error {
	return d.ImportSessionWithProgress(x, nil)
}

func (d *Database) ImportSessionWithProgress(x *Session, ch chan<- float64) error {
	if d.db == x.db {
		return errors.New("cannot import session from own database")
	}

	// This is all for tracking progress
	i, n := 0, float64(x.Len())
	update := func() {
		i++
		if ch != nil {
			ch <- float64(i) / n
		}
	}

	s, err := d.CreateSession(x.label)
	if err != nil {
		return err
	}
	s.updateString("date", x.Date().Format(TimeFormat))

	if s.compress == x.compress {
		d.db.BatchErr(func(tx *volt.Tx) error {
			b := tx.BucketPath("/", s.ids)
			smap := tx.BucketPath("/", s.ids, "map")
			return x.db.ViewErr(func(vx *volt.Tx) (err error) {
				defer func() {
					e := recover()
					if e != nil {
						err = e.(error)
					}
				}()

				xmap := vx.BucketPath("/", x.ids, "map")
				xmap.ForEach(func(k string, v []byte) {
					err := smap.Put(k, v)
					if err != nil {
						panic(err)
					}

					s.mapLength++
					s.diskBytes += int64(len(v))
					update()
				})

				b.PutInt64("map_length", s.mapLength)
				b.PutInt64("disk_bytes", s.diskBytes)
				if s.mapLength == x.mapLength && s.diskBytes == x.diskBytes {
					// if real_bytes is missing, we know something is inconsistent
					b.PutInt64("real_bytes", x.realBytes)
				}
				return nil
			})
		})
	} else {
		// This is going to be much slower, but it shouldn't happen as often,
		// so we'll do it the simple way.
		for _, k := range x.Keys() {
			bs, ok := x.Get(k)
			if !ok {
				panic("database is corrupt")
			}
			s.PutBytes(k, bs)
			update()
		}
	}

	return nil
}

func (d *Database) CreateSession(label string) (*Session, error) {
	idx := d.Index()
	next := idx.Next()
	idx.IncNext(1)
	idx.IncSize(1)

	s := &Session{
		db:       d.db,
		id:       next,
		ids:      strconv.FormatInt(next, 10),
		label:    label,
		date:     time.Now(),
		compress: d.CompressNew,
	}

	err := d.db.BatchErr(func(tx *volt.Tx) error {
		r := tx.Bucket("/")
		if r.Bucket(s.ids) != nil {
			return ErrSessionExists
		}

		b, err := r.CreateBucket(s.ids)
		if err != nil {
			return err
		}

		b.PutInt64("id", s.id)
		b.PutString("date", s.date.Format(TimeFormat))
		b.PutString("label", s.label)
		b.PutBool("compress", s.compress)
		b.PutInt64("map_length", 0)
		b.PutInt64("disk_bytes", 0)
		b.PutInt64("real_bytes", 0)
		b.CreateBucket("map")
		return nil
	})
	if err != nil {
		// TODO: probably err is always bucket already exists
		return nil, err
	}
	return s, nil
}

func (d *Database) GetLatestSession() (*Session, error) {
	xs := d.ListSessions()
	if len(xs) == 0 {
		return nil, nil
	}

	var max int64
	var idx int
	for i, x := range xs {
		if x.ID() > max {
			max = x.ID()
			idx = i
		}
	}
	return xs[idx], nil
}

func (d *Database) GetSessionS(ids string) (*Session, error) {
	id, err := strconv.ParseInt(ids, 10, 64)
	if err != nil {
		return nil, err
	}
	return d.GetSession(id)
}

func (d *Database) GetSession(id int64) (*Session, error) {
	s := &Session{
		db:  d.db,
		id:  id,
		ids: strconv.FormatInt(id, 10),
	}
	err := d.db.ViewErr(func(tx *volt.Tx) error {
		b := tx.BucketPath("/", s.ids)
		if b == nil {
			return ErrSessionNotExists
		}

		s.label, _ = b.GetString("label")
		s.compress, _ = b.GetBool("compress")
		s.mapLength, _ = b.GetInt64("map_length")
		s.diskBytes, _ = b.GetInt64("disk_bytes")
		s.realBytes, _ = b.GetInt64("real_bytes")
		var err error
		date, _ := b.GetString("date")
		s.date, err = time.Parse(TimeFormat, date)
		return err
	})
	if err != nil {
		return nil, err
	}
	return s, nil
}

func (d *Database) LabelSessionS(ids string, label string) error {
	return d.db.UpdateErr(func(tx *volt.Tx) error {
		r := tx.Bucket("/")
		if r.Bucket(ids) == nil {
			return ErrSessionNotExists
		}
		return r.Bucket(ids).PutString("label", label)
	})
}

func (d *Database) LabelSession(id int64, label string) error {
	return d.LabelSessionS(strconv.FormatInt(id, 10), label)
}

func (d *Database) DeleteSessionS(ids string) error {
	return d.db.UpdateErr(func(tx *volt.Tx) error {
		r := tx.Bucket("/")
		if r.Bucket(ids) == nil {
			return ErrSessionNotExists
		}
		return r.DeleteBucket(ids)
	})
}

func (d *Database) DeleteSession(id int64) error {
	return d.DeleteSessionS(strconv.FormatInt(id, 10))
}

func (d *Database) ListSessions() Sessions {
	var bs []string
	d.db.View(func(tx *volt.Tx) {
		bs = tx.Bucket("/").Buckets()
	})
	xs := make(Sessions, len(bs))
	for i, ids := range bs {
		s, err := d.GetSessionS(ids)
		if err != nil {
			panic(err)
		}
		xs[i] = s
	}
	return xs
}

type Session struct {
	db        *volt.DB
	id        int64
	ids       string
	label     string
	date      time.Time
	compress  bool
	mapLength int64
	diskBytes int64
	realBytes int64
}

func (s Session) ID() int64                  { return s.id }
func (s Session) Date() time.Time            { return s.date }
func (s Session) Label() string              { return s.label }
func (s Session) Len() int                   { return int(s.mapLength) }
func (s Session) Size() (real int, disk int) { return int(s.realBytes), int(s.diskBytes) }

type Marshaler interface {
	MarshalBytes() ([]byte, error)
}

func (s *Session) Put(key string, v Marshaler) error {
	bs, err := v.MarshalBytes()
	if err != nil {
		return err
	}
	return s.PutBytes(key, bs)
}

func (s *Session) PutBytes(key string, bs []byte) error {
	var realSize, diskSize int64

	realSize = int64(len(bs))
	if s.compress {
		var buf bytes.Buffer
		log.Debugf("Compressing with xz: %s", key)
		w, err := cmp.NewWriterLevel(&buf, cmp.BestCompression)
		if err != nil {
			log.Fatal(err)
		}
		io.Copy(w, bytes.NewReader(bs))
		w.Close()
		bs = buf.Bytes()
	}
	diskSize = int64(len(bs))

	return s.db.BatchErr(func(tx *volt.Tx) error {
		b := tx.BucketPath("/", s.ids)
		if b.Bucket("map").Has(key) {
			return errors.New("key already exists")
		}

		b.IncInt64("map_length", 1)
		b.IncInt64("disk_bytes", diskSize)
		b.IncInt64("real_bytes", realSize)
		return b.Bucket("map").Put(key, bs)
	})
}

func (s *Session) Get(key string) (bs []byte, ok bool) {
	s.db.View(func(tx *volt.Tx) {
		bs, ok = tx.BucketPath("/", s.ids, "map").Get(key)
	})

	if !ok {
		return
	}

	if s.compress {
		var buf bytes.Buffer
		log.Debugf("Decompressing with xz: %s", key)
		r, err := cmp.NewReader(bytes.NewReader(bs))
		if err != nil {
			log.Fatal(err)
		}
		io.Copy(&buf, r)
		r.Close()
		bs = buf.Bytes()
	}

	return
}

func (s *Session) Keys() []string {
	var keys []string
	s.db.View(func(tx *volt.Tx) {
		keys = tx.BucketPath("/", s.ids, "map").Keys()
	})
	return keys
}

func (s *Session) updateString(k, v string) error {
	return s.db.UpdateErr(func(tx *volt.Tx) error {
		r := tx.Bucket("/")
		if r.Bucket(s.ids) == nil {
			return ErrSessionNotExists
		}
		return r.Bucket(s.ids).PutString(k, v)
	})
}

type Sessions []*Session

func (s Sessions) Len() int           { return len(s) }
func (s Sessions) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }
func (s Sessions) Less(i, j int) bool { return s[i].id < s[j].id }

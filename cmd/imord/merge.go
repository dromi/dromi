// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package main

import (
	"fmt"

	log "github.com/Sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/dromi/dromi/webutil"
)

var mergeCompress bool

func init() {
	RootCmd.AddCommand(mergeCmd)
	mergeCmd.AddCommand(mergeAllCmd)
	mergeCmd.AddCommand(mergeSessionCmd)
	mergeCmd.PersistentFlags().BoolVarP(&mergeCompress, "compress", "z", true, "compress data on saving")
}

var mergeCmd = &cobra.Command{
	Use:   "merge",
	Short: "Merge in one or more sessions from external databases",
}

var mergeAllCmd = &cobra.Command{
	Use:   "all <database...>",
	Short: "Merge in one or more external databases",
	Run: func(cmd *cobra.Command, args []string) {
		DB.CompressNew = mergeCompress
		if len(args) == 0 {
			cmd.Usage()
			return
		}

		for _, path := range args {
			err := mergeDatabase(path)
			if err != nil {
				log.Error(err)
				return
			}
		}
	},
}

func mergeDatabase(path string) error {
	db, err := OpenDatabase(path)
	if err != nil {
		return err
	}

	sessions := db.ListSessions()
	return importWithProgress(path, sessions)
}

var mergeSessionCmd = &cobra.Command{
	Use:   "session <database> <id...>",
	Short: "Merge in one or more sessions from an external databases",
	Run: func(cmd *cobra.Command, args []string) {
		DB.CompressNew = mergeCompress
		if len(args) < 2 {
			cmd.Usage()
			return
		}

		if err := mergeSession(args[0], args[1:]); err != nil {
			log.Error(err)
		}
	},
}

func mergeSession(path string, ids []string) error {
	db, err := OpenDatabase(path)
	if err != nil {
		return err
	}

	sessions := make(Sessions, 0, len(ids))
	for _, id := range ids {
		s, err := db.GetSessionS(id)
		if err != nil {
			return err
		}
		sessions = append(sessions, s)
	}

	return importWithProgress(path, sessions)
}

func showProgress(path string, ch <-chan float64) (done chan struct{}) {
	done = make(chan struct{})
	go func() {
		var eta webutil.ETA
		eta.Begin()
		for p := range ch {
			eta.Update(p)
			fmt.Printf("\r%s: %s     ", path, eta.BarWithSpecialETA(40))
		}
		eta.End()
		fmt.Printf("\r%s: %s       \n", path, eta.BarWithSpecialETA(40))

		close(done)
	}()
	return
}

func importWithProgress(path string, xs Sessions) error {
	prog := make(chan float64)
	done := showProgress(path, prog)

	n := float64(len(xs))
	for i, s := range xs {
		ch, dc := make(chan float64), make(chan struct{})
		go func(sp float64, in <-chan float64, out chan struct{}) {
			for p := range in {
				prog <- sp * p
			}
			close(out)
		}(float64(i+1)/n, ch, dc)

		ch <- 0.0
		err := DB.ImportSessionWithProgress(s, ch)
		ch <- 1.0

		close(ch)
		<-dc
		if err != nil {
			return err
		}
	}

	close(prog)
	<-done
	return nil
}

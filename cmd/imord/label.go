// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package main

import (
	log "github.com/Sirupsen/logrus"
	"github.com/spf13/cobra"
)

func init() {
	RootCmd.AddCommand(labelCmd)
}

var labelCmd = &cobra.Command{
	Use:   "label <id> <string>",
	Short: "Label the specified session",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 2 {
			cmd.Usage()
			return
		}

		err := DB.LabelSessionS(args[0], args[1])
		if err != nil {
			log.Error(err)
		}
	},
}

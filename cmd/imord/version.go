// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package main

import (
	"os"
	"text/template"
	"time"

	"github.com/spf13/cobra"
)

func init() {
	RootCmd.AddCommand(versionCmd)
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Show version and build information",
	Long:  "Show the official version number of imord, as well as the release date.",
	Run: func(cmd *cobra.Command, args []string) {
		versionTmpl.Execute(os.Stdout, programInfo{
			Name:      "imord",
			Author:    "Ben Morgan",
			Email:     "neembi@gmail.com",
			Version:   "1.0.0",
			Date:      time.Now().Format("2 January 2006"),
			Copyright: time.Now().Format("2006"),
			Homepage:  "https://gitlab.com/dromi/dromi",
			License:   "MIT",

			DatabaseVersion:         DB.Index().Version(),
			RequiredDatabaseVersion: DatabaseVersion,
		})
	},
}

type programInfo struct {
	Name      string
	Author    string
	Email     string
	Version   string
	Date      string
	Homepage  string
	Copyright string
	License   string

	DatabaseVersion         int64
	RequiredDatabaseVersion int64
}

var versionTmpl = template.Must(template.New("version").Parse(
	`{{.Name}} version {{.Version}} ({{.Date}})
Copyright {{.Copyright}}, {{.Author}} <{{.Email}}>

Database version: {{.DatabaseVersion}}
Required database version: {{.RequiredDatabaseVersion}}

You may find {{.Name}} on the Internet at

    {{.Homepage}}

Please report any bugs you may encounter.

The source code of {{.Name}} is licensed under the {{.License}} license.
`))

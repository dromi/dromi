// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"

	log "github.com/Sirupsen/logrus"
)

var pageTypes = []string{"json", "csv"}

func setPageTypesIgnore(xs []string) {
	if len(xs) == 0 {
		return
	}

	m := make(map[string]bool)
	for _, t := range pageTypes {
		m[t] = true
	}
	for _, t := range xs {
		m[t] = false
	}
	pageTypes = make([]string, 0, len(m))
	for k, v := range m {
		if v {
			pageTypes = append(pageTypes, k)
		}
	}
}

type Page struct {
	Endpoint string
	Default  *Response
	Types    map[string]*Response
}

func NewPage(shost, endpoint string) (*Page, error) {
	p := &Page{
		Endpoint: endpoint,
		Types:    make(map[string]*Response),
	}
	r, err := NewResponse(shost + endpoint)
	if err != nil {
		return nil, err
	}
	p.Default = r
	for _, t := range pageTypes {
		r, err = NewResponse(fmt.Sprintf("%s%s?type=%s", shost, endpoint, t))
		if err != nil {
			return p, err
		}
		p.Types[t] = r
	}
	return p, nil
}

func NewPageFromBytes(bs []byte) (*Page, error) {
	p := new(Page)
	err := p.UnmarshalBytes(bs)
	return p, err
}

func (p *Page) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	q := req.URL.Query()
	log.WithFields(log.Fields{"type": q.Get("type")}).Debugf("[page %s] GET %s", p.Endpoint, req.URL.Path)
	if q.Get("type") == "" {
		p.Default.WriteResponse(w)
		return
	}
	r, ok := p.Types[q.Get("type")]
	if !ok {
		http.Error(w, "error: type unknown", 400)
		return
	}
	r.WriteResponse(w)
}

func (p *Page) MarshalBytes() ([]byte, error) {
	return json.Marshal(p)
}

func (p *Page) UnmarshalBytes(bs []byte) error {
	return json.Unmarshal(bs, p)
}

type Response struct {
	Status     string
	StatusCode int

	Header        http.Header
	Body          []byte
	ContentLength int64
}

func NewResponse(u string) (*Response, error) {
	r, err := http.Get(u)
	if err != nil {
		return nil, err
	}
	defer r.Body.Close()

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}

	return &Response{
		Status:        r.Status,
		StatusCode:    r.StatusCode,
		Header:        r.Header,
		Body:          body,
		ContentLength: r.ContentLength,
	}, nil
}

func (r *Response) WriteResponse(w http.ResponseWriter) {
	for k, v := range r.Header {
		w.Header()[k] = v
	}
	rd := bytes.NewReader(r.Body)
	io.Copy(w, rd)
}

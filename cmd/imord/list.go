// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package main

import (
	"fmt"
	"sort"
	"time"

	"github.com/spf13/cobra"
)

func init() {
	RootCmd.AddCommand(listCmd)
}

var listCmd = &cobra.Command{
	Use:   "list",
	Short: "List all available sessions in the file",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) > 0 {
			cmd.Usage()
			return
		}

		sessions := DB.ListSessions()
		sort.Sort(sessions)
		fmt.Print(sessionHeader)
		mb := 1024 * 1024
		for _, s := range sessions {
			n := s.Len()
			real, disk := s.Size()
			fmt.Printf(sessionFormat, s.ID(), s.Date().Format(time.Stamp), n, real/mb, disk/mb, s.Label())
		}
	},
}

var (
	sessionHeader   = "ID \tDATE            \tENDPOINTS \tREAL SIZE \tDISK SIZE \tLABEL\n"
	sessionHeaderHR = "-- \t--------------- \t--------- \t--------- \t--------- \t-----\n"
	sessionFormat   = "% 2d \t%s \t% 9d \t% 6d MB \t% 6d MB \t%s\n"
)

// Copyright (c) 2016, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package main

import (
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/BurntSushi/toml"
	log "github.com/Sirupsen/logrus"
	"github.com/goulash/xdg"
	"github.com/spf13/cobra"
)

type Configuration struct {
	// Listen defines what port we should listen to for online statistics.
	// If left empty, nothing happens.
	Listen string `toml:"listen"`
	Port   int    `toml:"-"`

	// If Debug is true, debugging messages are printed.
	Debug bool `toml:"debug"`

	Database string `toml:"database"`
}

// Default and global configuration
var Conf = &Configuration{
	Listen:   ":8080",
	Port:     8080,
	Debug:    false,
	Database: xdg.UserData("dromi/sessions.db"),
}

var DB *Database

func RootInit() {
	pf := RootCmd.PersistentFlags()
	pf.BoolVar(&Conf.Debug, "debug", Conf.Debug, "print debugging messages")
	pf.StringVarP(&Conf.Database, "database", "D", Conf.Database, "session database")
}

var RootCmd = &cobra.Command{
	Use:   "imord",
	Short: "Save and serve dromi output",
	Long:  `Imord is a helper for dromi, which reads endpoints and saves them to a database.`,
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		if Conf.Debug {
			log.SetLevel(log.DebugLevel)
		}
		if Conf.Listen == "" {
			log.Fatal("Need an address to listen on")
		}
		i := strings.LastIndexByte(Conf.Listen, ':')
		port, err := strconv.Atoi(Conf.Listen[i+1:])
		if err != nil {
			log.Fatal(err.Error())
		}
		Conf.Port = port

		// Open the database already
		xdg.MkdirAll(xdg.UserData("dromi"))
		DB, err = OpenDatabase(Conf.Database)
		if err != nil {
			if err == ErrDatabaseWrongVersion {
				// Either we are upgrading, or we just want to know what the versions are.
				if cmd != upgradeCmd && cmd != versionCmd {
					defer DB.Close()
					log.Fatalf(`Session database needs to be upgraded (%d → %d). Please run "upgrade" command.`,
						DB.Index().Version(), DatabaseVersion)
				}
			} else {
				log.Fatal(err.Error())
			}
		}
	},
	PersistentPostRun: func(cmd *cobra.Command, args []string) {
		DB.Close()
	},
}

func init() {
	RootCmd.AddCommand(upgradeCmd)
}

var upgradeCmd = &cobra.Command{
	Use:   "upgrade",
	Short: "Upgrade database format",
	Run: func(cmd *cobra.Command, args []string) {
		log.Info("Upgrading database...")
		err := DB.Upgrade()
		if err != nil {
			log.Error(err)
		} else {
			log.Info("Upgrade complete.")
		}
	},
}

var Interrupt = make(chan os.Signal, 1)

func waitInterrupt() error {
	<-Interrupt
	fmt.Println()
	return errors.New("Interrupted.")
}

func main() {
	// Read configuration, if it exists.
	path := xdg.FindConfig("dromi/config.toml")
	if path != "" {
		log.Debugf("Reading configuration file %q...", path)
		_, err := toml.DecodeFile(path, Conf)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		log.Debug("No configuration file found.")
	}

	// Arguments from the command line override the configuration file,
	// so we have to add the flags after loading the configuration.
	RootInit()
	RootCmd.Execute()
}
